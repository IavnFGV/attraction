# Attraction #

*Подробная информация в [wiki][link_id_wiki_main]*
## Releases ##

### [v1.3-RELEASE][link_id_release_1_3] ###

* [Вкладка "Документы". Если в цене стоит 0, то при выборе колонки, он сразу выделяется для редактирования - Issue 
13](https://bitbucket.org/IavnFGV/attraction/issues/13/--------------------------)(0.25)
* [Вкладка "Документы". Фильтр по вводу в комбобокс фирм - покупателей - Issue 14](https://bitbucket.org/IavnFGV/attraction/issues/14/----------------------------------------)(2)
* [Счет-фактура. Ошибка при расчете НДС - Issue 15](https://bitbucket.org/IavnFGV/attraction/issues/15/---------------------------------)(1.5)
* [Вкладка "Номера карточек". Новая вкладка - Issue 16](https://bitbucket.org/IavnFGV/attraction/issues/16/---------------------------------)(0.5)
* [Изменение имени создаваемых файлов Акта и Счета-фактуры - Issue 19](https://bitbucket.org/IavnFGV/attraction/issues/19/---------------------------------)(0)
* [Вкладка "Расчет заказа". Пересчет общей стоимости заказа при изменении каждой цены - Issue 20](https://bitbucket.org/IavnFGV/attraction/issues/20/---------------------------------)(1)

** Комментарии по релизу **

+ Изменение 16 требует добавления ресурсов из [resources1.zip](https://bitbucket.org/IavnFGV/attraction/downloads/resources1.zip) :
    + resources\cardNumbers.txt - начинка вкладки - копия cars.txt
    + resources\images\cards.png - файл-картинка для вкладки

### [v1.2 patch 1][link_id_release_1_2_patch_1] ###

* [Критичный баг с перезаписыванием списка услуг по счетам - Issue 18](https://bitbucket.org/IavnFGV/attraction/issues/18/------------------------------------------)(3)

** Комментарии по релизу **

+ Ошибки, которые уже есть в файле - архиве счетов можно исправить только вручную, загружая счет в окно редактирования 
и подставляя правильный набор услуг из третьего источника (например, если сохранились бумажные копии)

### [v1.2-RELEASE][link_id_release_1_2] ###

* [Заменить внешний вид календаря - Issue 7](https://bitbucket.org/IavnFGV/attraction/issues/7/------------------------------)(-)
* [Выбор фирмы - Issue 8](https://bitbucket.org/IavnFGV/attraction/issues/8/-----------)(0.5)
* [Добавить возможность использовать HTML - Issue 9](https://bitbucket.org/IavnFGV/attraction/issues/9/html)(4)
* [Изменить выбор даты на вкладке «Выбор оборудования» - Issue 10](https://bitbucket.org/IavnFGV/attraction/issues/10/------------------------------------------)(5)
* [Вкладка "Расчет заказа" - Issue 12](https://bitbucket.org/IavnFGV/attraction/issues/12/-----------------------)(2)

** Комментарии по релизу **

#### Инструкция ####
1. В рабочем каталоге заменяем папку _dependencies_lib_ на папку из [архива](https://bitbucket.org/IavnFGV/attraction/downloads/dependencies_lib-v1.2-RELEASE.zip) 
1. В папку resources кладем файлик gi.html - теперь это шаблон, который можно править.
1. в папку resources/templates кладем файл order_summary_template.ftl
1. в рабочий каталог копируем с заменой все файлы из [архива релиза](https://bitbucket.org/IavnFGV/attraction/downloads/krutota-attractions-1.2-RELEASE.zip)
    * в одной из тестовых сборок менялась кодировка файлов _resources/cars.txt,equipment,equipment-zakaz_ на 
    стандартную для Java - UTF-8 - нужно проделать это с рабочими файлами

### [v1.1-RELEASE][link_id_release_1_1] ###

* [Отступ под вкладками - Issue 1](https://bitbucket.org/IavnFGV/attraction/issues/1/------------------------------------------)(0.5)
* [Очистка календаря - вкладка "Оборудование" - Issue 2](https://bitbucket.org/IavnFGV/attraction/issues/2/----------------------------)(0.4)
* [Очистка календарей - вкладка "Документы" - Issue 3](https://bitbucket.org/IavnFGV/attraction/issues/3/----------------------------)(0.25)
* [Undo-redo - все приложение - Issue 4](https://bitbucket.org/IavnFGV/attraction/issues/4/----------------------------)(1.5)
* [Ошибка формирования pdf - Issue 5](https://bitbucket.org/IavnFGV/attraction/issues/5/----------------------------)(8)

** Комментарии по релизу **

*Баги/фичи*

+ Из-за особенностей реализации календаря в текущей версии при двойном клике на троеточии будет подставлена текущая дата

* * *

### Для чего это хранилище? ###

* Репозиторий содержит исходный код и все необходимое для работы приложения Attraction
* В баг - трекере ведется сопровождение разработки
* Wiki содержит ответы на некоторые частые вопросы по процессам сопровождения и разработки

[link_id_wiki_main]: https://bitbucket.org/IavnFGV/attraction/wiki/
[link_id_release_1_1]: https://bitbucket.org/IavnFGV/attraction/downloads/krutota-attractions-1.1-RELEASE.jar
[link_id_release_1_2]: https://bitbucket.org/IavnFGV/attraction/downloads/krutota-attractions-1.2-RELEASE.zip
[link_id_release_1_3]: https://bitbucket.org/IavnFGV/attraction/downloads/krutota-attractions-1.3-RELEASE-PROD-7b05ab5.jar
[link_id_release_1_2_patch_1]: https://bitbucket.org/IavnFGV/attraction/downloads/krutota-attractions-1.2%20patch%201-SNAPSHOT-TEST-342341b.jar