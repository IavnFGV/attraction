import gui.Frame;
import tools.Configuration;
import tools.EquipmentList;
import tools.FrameUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.lang.reflect.Method;
import java.util.Properties;

public class Application {


    public Application() {
    }

    public static void main(String[] args) throws InterruptedException {
        setLookAndFeel();
        Configuration.initialize();
        EquipmentList.initialize();
        final Frame frame = new Frame();
        addTestGuard(frame);
        WindowAdapter windowListener = new WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                Configuration.save();

                JTable table = frame.getOrderTab().getTable();
                final File file = new File("config.properties");
                if (!file.exists()) {
                    try {
                        file.createNewFile();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                try {
                    final FileReader fileReader = new FileReader(file);
                    final Properties properties = new Properties();
                    properties.load(fileReader);

                    properties.put("numberw", String.valueOf(table.getColumn("№").getPreferredWidth()));
                    properties.put("namew", String.valueOf(table.getColumn("Название").getPreferredWidth()));
                    properties.put("pricew", String.valueOf(table.getColumn("Цена").getPreferredWidth()));
                    properties.put("summw", String.valueOf(table.getColumn("Сумма").getPreferredWidth()));
                    properties.put("countw", String.valueOf(table.getColumn("Количество").getPreferredWidth()));
                    properties.store(new FileWriter(file), "save props");
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            }
        };
        System.out.println("Frame created");

        Thread.sleep(1000);
        FrameUtil.setCurrentFrame(frame);
        frame.addWindowListener(windowListener);
        frame.setVisible(true);
    }

    public static void setLookAndFeel() {
        String sysLAFClassName = UIManager.getSystemLookAndFeelClassName();

        try {
            UIManager.setLookAndFeel(sysLAFClassName);
            UIManager.put("TabbedPane.contentBorderInsets", new Insets(20, 2, 3, 3));
        } catch (Exception var2) {
            var2.printStackTrace();
        }

    }

    /**
     * adding restrictions for test versions of application - can be ignored
     *
     * @param frame what frame closing to listen
     */
    private static void addTestGuard(Frame frame) {
        final Properties properties = new Properties();
        try {
            InputStream stream =
                    Application.class.getResourceAsStream("mavenbuild.properties");
            properties.load(stream);

            System.out.println("maven.project.version = " + properties.getProperty("maven.project.version"));
            String appVersion = properties.getProperty("maven.project.version");
            if (appVersion.toLowerCase().contains("test")) {
                System.out.println("Test detected. Try to instantiate AppGuard...");
                Class aClass = Class.forName("ApplicationSimpleGuard");
                Method method = aClass.getDeclaredMethod("shutDown");
                System.out.println("AppGuard is here.");
                WindowAdapter windowAdapter = new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        try {
                            method.invoke(aClass);
                        } catch (Throwable e1) {
                            e1.printStackTrace();
                        }
                    }
                };
                frame.addWindowListener(windowAdapter);
            }
        } catch (ClassNotFoundException e) {
            System.out.println("AppGuard is missed. Continue execution");
            return;
        } catch (Throwable e) {
            e.printStackTrace(); // ignore all, if something wrong;
        }
    }
}
