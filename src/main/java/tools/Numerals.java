package tools;

import java.math.BigDecimal;

public class Numerals {
    public Numerals() {
    }

    public static String russian(Number n) {
        return (new Russian()).format(n);
    }

    public static String russianRubles(Number n) {
        BigDecimal bd = Util.toBigDecimal(n);
        return (new Russian()).amount(bd);
    }
}
