package tools;

import java.math.BigDecimal;
import java.math.BigInteger;

public abstract class AbstractNumeral {
    public static final BigInteger MAX_SUPPORTED;

    static {
        MAX_SUPPORTED = (new BigInteger("1000000000000000000000000000000000000")).subtract(BigInteger.ONE);
    }

    public AbstractNumeral() {
    }

    protected void checkSupported(Number number) {
        if(!(number instanceof Integer) && !(number instanceof Long) && !(number instanceof Short) && !(number instanceof Byte)) {
            if(!(number instanceof BigInteger)) {
                throw new IllegalArgumentException("Support only Integer numbers: BigInteger, Integer, Long and Short.Floating-point is not supported");
            }

            BigInteger bi = (BigInteger)number;
            if(bi.abs().compareTo(MAX_SUPPORTED) > 0) {
                throw new IllegalArgumentException("Max supported number:" + MAX_SUPPORTED);
            }
        }

    }

    public abstract String format(Number var1);

    public abstract String amount(BigDecimal var1);

    public String amount(Number amount) {
        return this.amount((BigDecimal)Util.toBigDecimal(amount));
    }
}
