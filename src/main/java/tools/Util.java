package tools;

import java.math.BigDecimal;
import java.math.BigInteger;

class Util {
    Util() {
    }

    static byte[][] groups(String txt, int groupSize) {
        int length = txt.length();
        int groupCount = length / groupSize;
        int remainder = length % groupSize;
        int j = 0;
        int k = 0;
        if(remainder > 0) {
            ++groupCount;
            k = groupSize - remainder;
        }

        byte[][] nn = new byte[groupCount][groupSize];

        for(int i = 0; i < txt.length(); ++i) {
            byte x = (byte)(txt.charAt(i) - 48);
            if(x < 0 || x > 9) {
                throw new IllegalArgumentException("Wrong string:" + txt);
            }

            nn[j][k] = x;
            if(k == groupSize - 1) {
                k = 0;
                ++j;
            } else {
                ++k;
            }
        }

        return nn;
    }

    static int search(String[] array, String token) {
        int x = -1;

        for(int i = 0; i < array.length; ++i) {
            if(array[i].equals(token)) {
                x = i;
                break;
            }
        }

        return x;
    }

    static void toUpperCaseFirstLetter(StringBuilder sb) {
        char first = sb.charAt(0);
        first = Character.toUpperCase(first);
        sb.setCharAt(0, first);
    }

    static BigDecimal toBigDecimal(Number n) {
        if(n == null) {
            throw new IllegalArgumentException("Your number is null");
        } else {
            BigDecimal bd;
            if(n instanceof BigDecimal) {
                bd = (BigDecimal)n;
            } else if(n instanceof BigInteger) {
                bd = new BigDecimal((BigInteger)n);
            } else if(!(n instanceof Short) && !(n instanceof Long) && !(n instanceof Integer)) {
                if(!(n instanceof Float) && !(n instanceof Double)) {
                    throw new IllegalArgumentException("Unsupported type:" + n);
                }

                bd = BigDecimal.valueOf(n.doubleValue());
            } else {
                bd = BigDecimal.valueOf(n.longValue());
            }

            return bd;
        }
    }
}