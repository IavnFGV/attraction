package tools;

import gui.special.JNode;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class EquipmentList {
  private static EquipmentList.Node equipmentRoot = new EquipmentList.Node("root");
  private static EquipmentList.Node orderRoot = new EquipmentList.Node("root");
  private static int enterCount;

  public EquipmentList() {
  }

  public static void setEnterCount(int enterCount) {
    EquipmentList.enterCount = enterCount;
  }

  public static void initialize() {
    try {
      parce(equipmentRoot, "equipment");
      parce(orderRoot, "equipment-zakaz");
    } catch (IOException var1) {
      var1.printStackTrace();
    }

  }

  private static void parce(EquipmentList.Node parent, String fName) throws IOException {
    FileReader in = new FileReader(new File(fName));
    boolean c = false;
    int currentLevel = 0;
    int Level = 0;
    int positionInLine = 0;
    StringBuilder name = new StringBuilder();

    int var10;
    while ((var10 = in.read()) != -1) {
      if (positionInLine == 0) {
        boolean var12 = Character.isWhitespace((char) var10);
        if (!var12) {
          if (var10 == 43) {
            ++currentLevel;
          } else {
            name.append((char) var10);
            ++positionInLine;
          }
        }
      } else if (var10 != 13) {
        if (var10 != 10) {
          name.append((char) var10);
        } else if (currentLevel == 3) {
          parent.setComment(name.toString());
          currentLevel = 0;
          name.setLength(0);
          positionInLine = 0;
        } else if (currentLevel == 4) {
          parent.setPrice(name.toString());
          currentLevel = 0;
          name.setLength(0);
          positionInLine = 0;
        } else {
          int node;
          if (currentLevel > Level) {
            node = parent.children.size() - 1;
            EquipmentList.Node last = (EquipmentList.Node) parent.children.get(node);
            parent = last;
          }

          if (currentLevel < Level) {
            node = Level - currentLevel;
            if (currentLevel == 0) {
              if (fName.equals("equipment")) {
                parent = equipmentRoot;
              } else {
                parent = orderRoot;
              }
            } else {
              do {
                parent = parent.parent;
                --node;
              } while (node > 0);
            }
          }

          EquipmentList.Node var11 = new EquipmentList.Node(name.toString());
          var11.parent = parent;
          var11.choose = currentLevel == 1;
          parent.children.add(var11);
          name.setLength(0);
          Level = currentLevel;
          currentLevel = 0;
          positionInLine = 0;
        }
      }
    }

  }

  public static void createTree(JNode root, ActionListener al, String str, String searchString) {
      EquipmentList.Node r = null;
      if ("equipment".equals(str)) {
        r = equipmentRoot;
      }

      if ("order".equals(str)) {
        r = orderRoot;
      }

      Iterator var5 = r.children.iterator();

      while (var5.hasNext()) {
        EquipmentList.setEnterCount(0);
        EquipmentList.Node child = (EquipmentList.Node) var5.next();
        child.createTree(root, al, searchString);
      }
  }

  public static class Node {
    final String name;
    EquipmentList.Node parent = null;
    List<EquipmentList.Node> children;
    boolean choose = false;
    private String comment;
    private String price;

    public String getPrice() {
      return this.price;
    }

    public void setPrice(String price) {
      this.price = price;
    }

    public String getComment() {
      return this.comment;
    }

    public void setComment(String comment) {
      this.comment = comment;
    }

    public Node(String name) {
      char c = name.charAt(0);
      c = Character.toUpperCase(c);
      this.name = c + name.substring(1, name.length());
      this.children = new ArrayList();
    }

    public void createTree(JNode root, ActionListener al, String searchString) {
      EquipmentList.enterCount++;

      final boolean b = searchString.length() > 3;
      if (b) {
        if (enterCount == 1) {
//          addNode(root, al, searchString);
          Iterator var6 = this.children.iterator();

          while (var6.hasNext()) {
            Node child = (Node) var6.next();
            child.createTree(root, al, searchString);
            enterCount--;
          }
        }
        if (enterCount == 2) {
          if (name.toUpperCase().contains(searchString.toUpperCase())) {
            addNode(root, al, searchString);
          }
//          enterCount--;
        }
        if (enterCount > 2) {
          addNode(root, al, searchString);
        }
      } else {
        addNode(root, al, searchString);
      }
    }

    private void addNode(JNode root, ActionListener al, String searchString) {
      int childCount = this.children.size();
      JNode node;
      if (this.choose) {
        node = new JNode(this.name, al, (String) null, this.comment, this.price);
      } else {
        node = new JNode(this.name);
      }

      root.addNode(node);

      Iterator var6 = this.children.iterator();

      while (var6.hasNext()) {
        Node child = (Node) var6.next();
        child.createTree(node, al, searchString);
        enterCount--;
      }
    }

    public String toString() {
      return this.name;
    }
  }
}
