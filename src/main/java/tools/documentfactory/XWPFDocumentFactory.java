package tools.documentfactory;

import com.itextpdf.text.*;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTblWidth;
import tools.Configuration;
import tools.DocumentFactory;

import javax.swing.*;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;

public class XWPFDocumentFactory implements DocumentFactory {
    public XWPFDocumentFactory() {
    }

    @Override
    public void create(String date, String eventServs, String comment, String contacts, String humans, String equipment, String notes, boolean b, String nal) {
        XWPFDocument document = new XWPFDocument();
        XWPFTable table = document.createTable(1, 2);
        table.getCTTbl().getTblPr().unsetTblBorders();
        table.getCTTbl().getTblPr().getTblW().setType(STTblWidth.PCT);
        table.getCTTbl().getTblPr().getTblW().setW(new BigInteger("5000"));
        CTTc ctTc = table.getRow(0).getCell(0).getCTTc();
        CTP ctP = ctTc.sizeOfPArray() == 0 ? ctTc.addNewP() : ctTc.getPArray(0);
        XWPFParagraph par1 = table.getRow(0).getCell(0).getParagraph(ctP);
        XWPFRun run = par1.createRun();
        run.setFontFamily("Times New Roman");
        run.setFontSize(16);
        run.setText(date);
        ctTc = table.getRow(0).getCell(1).getCTTc();
        ctP = ctTc.sizeOfPArray() == 0 ? ctTc.addNewP() : ctTc.getPArray(0);
        par1 = table.getRow(0).getCell(1).getParagraph(ctP);
        par1.setAlignment(ParagraphAlignment.RIGHT);
        run = par1.createRun();
        run.setFontFamily("Times New Roman");
        run.setFontSize(16);
        run.setText(eventServs);
        addParagraph(document, "notes", notes);
        if (b) {
            addParagraph(document, "", "Оплата по безналу. Передать закрывающие документы заказчику.");
        }

        if (!"".equals(nal.trim()) && !b) {
            addParagraph(document, "", "Взять у заказчика " + nal + " рублей\n");
        }

        addParagraph(document, "Комментарии", comment);
        addParagraph(document, "Контакты", contacts);
        if (humans != null && !humans.trim().equals("")) {
            addList(document, "На мероприятии будут работать", humans);
        }

        addList(document, "Список выбранного оборудования", equipment);
        File outputDirectory = Configuration.getCurrentDirectory();
        String name = date + " - " + eventServs;
        File output = new File(outputDirectory, name + ".doc");
        if (output.exists()) {
            int stream = JOptionPane.showConfirmDialog(null, "Файл с именем " + output.getAbsolutePath() + " уже существует. Заменить его ?", "Сохранение файла", 2);
            if (stream != 0) {
                return;
            }
        } else {
            try {
                output.createNewFile();
            } catch (IOException var22) {
                JOptionPane.showMessageDialog(null, "Произошла ошибка при создании файла " + output.getAbsolutePath(), "Ошибка записи", 0);
            }
        }

        FileOutputStream stream1 = null;

        try {
            stream1 = new FileOutputStream(output);
        } catch (FileNotFoundException var21) {
        }

        try {
            document.write(stream1);
        } catch (NullPointerException | IOException var20) {
            JOptionPane.showMessageDialog(null, "Произошла ошибка при создании файла " + output.getAbsolutePath(), "Ошибка записи", 0);
        }

    }

    private void addParagraph(XWPFDocument document, String title, String text) {
        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun run = null;
        if (!title.equals("")) {
            paragraph.setAlignment(ParagraphAlignment.CENTER);
            run = paragraph.createRun();
            run.setFontFamily("Times New Roman");
            run.setBold(true);
            run.setFontSize(11);
            run.setText(title);
            paragraph = document.createParagraph();
        }

        run = paragraph.createRun();
        run.setFontFamily("Times New Roman");
        run.setFontSize(11);
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < text.length(); ++i) {
            char c = text.charAt(i);
            if (c == 10) {
                run.setText(builder.toString());
                run.addBreak();
                builder.setLength(0);
            } else {
                builder.append(c);
            }
        }

        run.setText(builder.toString());
    }

    private void addList(XWPFDocument document, String title, String list) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun run = paragraph.createRun();
        run.setFontFamily("Times New Roman");
        run.setBold(true);
        run.setFontSize(11);
        run.setText(title);
        paragraph = document.createParagraph();
        run = paragraph.createRun();
        run.setFontFamily("Times New Roman");
        run.setFontSize(11);
        StringBuilder builder = new StringBuilder();
        boolean newLine = false;

        for (int i = 0; i < list.length(); ++i) {
            char c = list.charAt(i);
            if (c == 10) {
                newLine = true;
                run.setText(builder.toString());
                run.addBreak();
                builder.setLength(0);
            } else if (newLine) {
                boolean whitespace = Character.isWhitespace(c);
                if (whitespace) {
                    builder.append(c);
                } else if (c == 45) {
                    builder.append("□");
                    builder.append(' ');
                    newLine = false;
                } else {
                    builder.append(c);
                    newLine = false;
                }
            } else {
                builder.append(c);
            }
        }

        run.setText(builder.toString());
    }

    @Override
    public void print(String date, String eventServs, String comment, String contacts, String humans, String equipment, String notes, boolean b, String nal) {
        BaseFont times = null;

        try {
            times = BaseFont.createFont("c:/windows/fonts/times.ttf", "cp1251", true);
        } catch (IOException | DocumentException var45) {
            System.out.println("1");
            return;
        }

        Font fontTitle = new Font(times);
        fontTitle.setSize(11.0F);
        fontTitle.setStyle(1);
        Font fontTitle2 = new Font(times);
        fontTitle2.setSize(16.0F);
        fontTitle2.setStyle(1);
        Font font = new Font(times);
        font.setSize(11.0F);
        Font notesFont = new Font(times);
        notesFont.setSize(9.0F);
        new File("resources/images/white_square.png");
        Image img = null;

        try {
            img = Image.getInstance("resources/images/white_square.png");
            img.scaleAbsolute(30.0F, 30.0F);
        } catch (BadElementException | IOException var44) {
        }

        Chunk square = new Chunk(img, 0.0F, 0.0F);
        FileOutputStream stream = null;

        try {
            stream = new FileOutputStream(new File("temporary.pdf"));
        } catch (FileNotFoundException var43) {
            try {
                (new File("temporary.pdf")).createNewFile();
            } catch (IOException var42) {
            }
        }

        Document doc = new Document(PageSize.A4, 50.0F, 50.0F, 50.0F, 50.0F);

        try {
            PdfWriter.getInstance(doc, stream);
        } catch (DocumentException var41) {
            System.out.println("2");
            return;
        }

        doc.open();
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100.0F);
        PdfPCell cell3 = new PdfPCell(new Paragraph(date, fontTitle2));
        cell3.setBorder(0);
        Paragraph eventServsValue = new Paragraph(eventServs, fontTitle2);
        eventServsValue.setAlignment(2);
        PdfPCell cell4 = new PdfPCell(eventServsValue);
        cell4.setBorder(0);
        cell4.setHorizontalAlignment(2);
        table.addCell(cell3);
        table.addCell(cell4);

        try {
            doc.add(table);
            doc.add(new Paragraph(""));
        } catch (DocumentException var40) {
        }

        Paragraph notesP = new Paragraph(notes, notesFont);
        notesP.setAlignment(0);

        try {
            doc.add(notesP);
        } catch (DocumentException var39) {
        }

        Paragraph p;
        if (b) {
            p = new Paragraph("Оплата по безналу. Передать закрывающие документы заказчику", notesFont);
            notesP.setAlignment(0);

            try {
                doc.add(p);
            } catch (DocumentException var38) {
            }
        }

        if (!"".equals(nal.trim()) && !b) {
            p = new Paragraph("\nВзять у заказчика " + nal + " рублей", notesFont);
            notesP.setAlignment(0);

            try {
                doc.add(p);
            } catch (DocumentException var37) {
            }
        }

        p = new Paragraph("Комментарии", fontTitle);
        p.setAlignment(1);

        try {
            doc.add(p);
        } catch (DocumentException var36) {
        }

        p = new Paragraph(comment, font);

        try {
            doc.add(p);
        } catch (DocumentException var35) {
        }

        p = new Paragraph("Контакты", fontTitle);
        p.setAlignment(1);

        try {
            doc.add(p);
        } catch (DocumentException var34) {
        }

        p = new Paragraph(contacts, font);

        try {
            doc.add(p);
        } catch (DocumentException var33) {
        }

        p = new Paragraph("На мероприятии будут работать", fontTitle);
        p.setAlignment(1);

        try {
            doc.add(p);
        } catch (DocumentException var32) {
        }

        p = new Paragraph(humans, font);

        try {
            doc.add(p);
        } catch (DocumentException var31) {
        }

        p = new Paragraph("Список выбранного оборудования", fontTitle);
        p.setAlignment(1);

        try {
            doc.add(p);
        } catch (DocumentException var30) {
        }

        p = createEquipmentList(equipment, font, square);

        try {
            doc.add(p);
        } catch (DocumentException var29) {
        }

        doc.close();
        PDDocument documentForPrint = null;

        try {
            documentForPrint = PDDocument.load("temporary.pdf");
        } catch (IOException var28) {
        }

        try {
            documentForPrint.print();
        } catch (PrinterException var27) {
        }

    }

    private Paragraph createEquipmentList(String equipment, Font font, Chunk square) {
        Paragraph paragraph = new Paragraph();
        paragraph.setFont(font);
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < equipment.length(); ++i) {
            char c = equipment.charAt(i);
            if (c == 45) {
                paragraph.add(builder.toString());
                paragraph.add("-");
                builder.setLength(0);
            } else {
                builder.append(c);
            }
        }

        paragraph.add(builder.toString());
        return paragraph;
    }
}
