package tools.documentfactory;


import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.docx4j.convert.out.pdf.viaXSLFO.Conversion;
import org.docx4j.convert.out.pdf.viaXSLFO.PdfSettings;
import org.docx4j.model.structure.PageSizePaper;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.AltChunkType;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tools.DocumentFactory;

import javax.swing.*;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;


/**
 * Created by GFH on 10.12.2015.
 */
public class Doc4jDocumentFactory implements DocumentFactory {
    private static final Logger log = LoggerFactory.getLogger(Doc4jDocumentFactory.class);

    private static String orderSummaryTemplateFileName = "order_summary_template.ftl";
    private static File orderSummaryTemplateDir = new File(System.getProperty("user.dir") + "/resources/templates");

    Configuration configuration;
    Template helloTemplate;
    boolean printingMode = false;

    public Doc4jDocumentFactory() {
        configuration = new Configuration(Configuration.VERSION_2_3_23);
        try {
            configuration.setDirectoryForTemplateLoading(orderSummaryTemplateDir);
            helloTemplate = configuration.getTemplate(orderSummaryTemplateFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveDocFile(WordprocessingMLPackage wordMLPackage, String filename) throws IOException, Docx4JException {
        log.debug("preparing for saving doc file");
        File outputDirectory = tools.Configuration.getCurrentDirectory();
        File output = new File(outputDirectory, filename + ".doc");
        if (output.exists()) {
            log.info("file " + output.getAbsolutePath() + " already exists. Overwrite?");
            int stream = JOptionPane.showConfirmDialog(null, "Файл с именем " + output.getAbsolutePath() + " уже существует. Заменить его ?", "Сохранение файла", 2);
            if (stream != 0) {
                log.info("user declined proposition. Return");
                return;
            }
        } else {
            try {
                log.debug("creating new file: " + output.getAbsolutePath());
                output.createNewFile();
            } catch (IOException e) {
                log.error("Error during file creation " + e.getMessage(), e);
                JOptionPane.showMessageDialog(null, "Произошла ошибка при создании файла " + output.getAbsolutePath(), "Ошибка записи", 0);
                throw e;
            }
        }
        FileOutputStream stream1 = null;
        stream1 = new FileOutputStream(output);
        log.debug("saving wordPackage to doc file");
        try {
            wordMLPackage.save(stream1);
        } catch (Docx4JException e) {
            log.error("Error during wordMLPackage.save " + e.getMessage(), e);
            JOptionPane.showMessageDialog(null, "Произошла ошибка при создании файла " + output.getAbsolutePath(), "Ошибка записи", 0);
            throw e;
        }
    }

    @Override
    public void create(String date, String eventServs, String comment, String contacts, String humans, String equipment, String notes, boolean b, String nal) {
        Map<String, Object> fremarkerMap = getFremarkerMap(date, eventServs, comment, contacts, humans, equipment, notes, b, nal);
        log.debug("Processing template");
        StringWriter writer = new StringWriter();
        try {
            helloTemplate.process(fremarkerMap, writer);
            log.debug(writer.toString());
            log.debug("creating new docx");
            WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.createPackage(PageSizePaper.A4, false);


            MainDocumentPart mdp = wordMLPackage.getMainDocumentPart();

//            mdp.addParagraphOfText("Paragraph 1");

            // Add the XHTML altChunk
            String xhtml = "<html><head><title>Import me</title></head><body><p>Hello World!</p></body></html>";
            mdp.addAltChunk(AltChunkType.Xhtml, writer.getBuffer().toString().getBytes());

//            mdp.addParagraphOfText("Paragraph 3");

            // Round trip
            WordprocessingMLPackage pkgOut = mdp.convertAltChunks();

            // Display result
//            System.out.println(
//                    XmlUtils.marshaltoString(pkgOut.getMainDocumentPart().getJaxbElement(), true, true));


//            log.debug("creating XHTMLImporterImpl");
//            XHTMLImporterImpl XHTMLImporter = new XHTMLImporterImpl(wordMLPackage);
//            log.debug("adding html-->xhtml into docx");
//            wordMLPackage.getMainDocumentPart().getContent().addAll(
//                    XHTMLImporter.convert(writer.getBuffer().toString(), null));
            if (!printingMode) {
                log.info("NOT printing mode : SAVE DOC FILE");
                String name = date + " - " + eventServs;
                saveDocFile(pkgOut, name);
                return;
            }
            log.info("Printing mode. XSLFO Convertion");
            Conversion c = new Conversion(pkgOut);
            log.info("Saving temp .pdf");
            String tempFilename = "temporary1.pdf";
            FileOutputStream stream = null;
            File file = new File(tempFilename);
            if (file.exists()) {
                log.info("Prev temp file found . Delete " + tempFilename);
                if (!file.delete()) {
                    log.error("Cant delete prev temp file");
                    return;
                }
            }
            c.output(new FileOutputStream(tempFilename), new PdfSettings());
            PDDocument documentForPrint = null;
            documentForPrint = PDDocument.load(tempFilename);
            log.info("Sending to print");
            documentForPrint.print();
            log.info("Success");
        } catch (TemplateException | IOException | Docx4JException/*InvalidFormatException*/ | PrinterException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void print(String date, String eventServs, String comment, String contacts, String humans, String equipment, String notes, boolean b, String nal) {
        log.info("switch to printmode");
        printingMode = true;
        create(date, eventServs, comment, contacts, humans, equipment, notes, b, nal);
        printingMode = false;
    }


    private static Map<String, Object> getFremarkerMap(String date, String eventServs, String comment, String contacts, String
            humans, String equipment, String notes, boolean b, String nal) {
        Map<String, Object> map = new HashMap<>();

        Function<String, String> setEncoding = s -> {
            // detectEncoding(s);

            return s;

        };
        map.put("date", setEncoding.apply(date));
        map.put("eventServs", setEncoding.apply(eventServs));
        map.put("notes", setEncoding.apply(notes));
        map.put("beznal", b);
        map.put("nal", setEncoding.apply(nal));
        map.put("humans", setEncoding.apply(humans));
        map.put("comment", setEncoding.apply(comment));
        map.put("contacts", setEncoding.apply(contacts));
        map.put("equipment", setEncoding.apply(equipment));
        return map;
    }
}
