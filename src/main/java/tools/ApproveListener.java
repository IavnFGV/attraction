package tools;

/**
 * @author Dmitry Tumash.
 */
public interface ApproveListener {

  public void update(boolean result);

}
