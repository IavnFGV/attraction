package tools;

public interface DocumentFactory {
    void create(String date, String eventServs, String comment, String contacts, String humans, String equipment,
                String notes, boolean b, String nal);

    void print(String date, String eventServs, String comment, String contacts, String humans, String equipment,
               String notes, boolean b, String nal);
}
