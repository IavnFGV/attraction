package tools;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class RussianMoney {
    public RussianMoney() {
    }

    public static String totalToStrWithNds(BigDecimal d) {
        BigDecimal beznds = d.setScale(2, RoundingMode.HALF_DOWN).divide(new BigDecimal("1.18"), RoundingMode.HALF_DOWN);
        BigDecimal nds = d.subtract(beznds);
        int rub = nds.setScale(0, RoundingMode.DOWN).intValue();
        int kop = nds.subtract(new BigDecimal(rub)).setScale(2, RoundingMode.HALF_DOWN).unscaledValue().intValue();
        return Numerals.russianRubles(d) + " в т.ч. НДС 18% - " + rub + " руб. " + (kop < 10?"0" + kop:"" + kop) + " коп.";
    }

    public static void main(String[] args) {
        System.out.println(totalToStrWithNds(new BigDecimal(118)));
    }
}
