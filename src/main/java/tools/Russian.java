package tools;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

public class Russian extends AbstractNumeral {
    private static final String[] EDINICHI = new String[]{"ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять"};
    private static final String[] DESYAT = new String[]{"десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемьнадцать", "девятнадцать"};
    private static final String[] DESYATKI = new String[]{"", "десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
    private static final String[] SOTNI = new String[]{"", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};
    private static final String[] LIONS = new String[]{"", "тысяча", "миллион", "миллиард", "триллион", "квадриллион", "квинтиллион", "секстиллион", "септиллион", "октиллион", "нониллион", "дециллион"};

    public Russian() {
    }

    public String format(Number number) {
        this.checkSupported(number);
        return this.formatImpl(number.toString());
    }

    private String formatImpl(String text) {
        if("0".equals(text)) {
            return EDINICHI[0];
        } else {
            StringBuilder sb = new StringBuilder();
            if(text.startsWith("-")) {
                sb.append("минус ");
                text = text.substring(1);
            }

            byte[][] n = Util.groups(text, 3);

            for(int i = 0; i < n.length; ++i) {
                int k = n.length - i - 1;
                byte h = n[i][0];
                byte t = n[i][1];
                byte u = n[i][2];
                if(h != 0 || t != 0 || u != 0) {
                    String ed;
                    if(h > 0) {
                        ed = SOTNI[h];
                        sb.append(ed);
                        sb.append(" ");
                    }

                    if(t == 0) {
                        if(u > 0) {
                            ed = EDINICHI[u];
                            if(k == 1) {
                                switch(u) {
                                    case 1:
                                        ed = "одна";
                                        break;
                                    case 2:
                                        ed = "две";
                                }
                            }

                            sb.append(ed);
                            sb.append(" ");
                        }
                    } else if(t == 1) {
                        sb.append(DESYAT[u]);
                        sb.append(" ");
                    } else if(t > 1) {
                        sb.append(DESYATKI[t]);
                        if(u > 0) {
                            sb.append(" ");
                            ed = EDINICHI[u];
                            if(k == 1) {
                                switch(u) {
                                    case 1:
                                        ed = "одна";
                                        break;
                                    case 2:
                                        ed = "две";
                                }
                            }

                            sb.append(ed);
                        }

                        sb.append(" ");
                    }

                    if(k > 0 && h + t + u > 0) {
                        if(k == 1) {
                            sb.append(tisyachi(h, t, u));
                        } else {
                            sb.append(lions(h, t, u, k));
                        }

                        sb.append(" ");
                    }
                }
            }

            return sb.toString().trim();
        }
    }

    private static char lastNonWhitespace(CharSequence sb) {
        for(int i = sb.length() - 1; i >= 0; --i) {
            char ch = sb.charAt(i);
            if(!Character.isWhitespace(ch)) {
                return ch;
            }
        }

        return '\u0000';
    }

    static String lions(int h, int t, int u, int k) {
        StringBuilder sb = new StringBuilder();
        sb.append(LIONS[k]);
        if(t != 0 && t <= 1) {
            sb.append("ов");
        } else {
            switch(u) {
                case 1:
                    break;
                case 2:
                case 3:
                case 4:
                    sb.append("а");
                    break;
                default:
                    sb.append("ов");
            }
        }

        return sb.toString();
    }

    static String tisyachi(int h, int t, int u) {
        String result = "тысяч";
        if(t == 0 || t > 1) {
            switch(u) {
                case 1:
                    result = "тысяча";
                    break;
                case 2:
                case 3:
                case 4:
                    result = "тысячи";
            }
        }

        return result;
    }

    public String amount(BigDecimal bi) {
        String txt = bi.toPlainString();
        int point = txt.indexOf(46);
        StringBuilder sb = new StringBuilder();
        String rubli = txt;
        if(point > 0) {
            rubli = txt.substring(0, point);
        }

        String celaya = this.formatImpl(rubli);
        sb.append(celaya);
        sb.append(" ");
        String currency = rubley(bi);
        sb.append(currency);
        sb.append(" ");
        int k = roundKopeyki(bi);

        assert k >= 0 && k < 100;

        if(k < 10) {
            sb.append("0");
            sb.append(k);
        } else {
            sb.append(k);
        }

        sb.append(" ");
        sb.append(kopeyki(k));
        Util.toUpperCaseFirstLetter(sb);
        return sb.toString();
    }

    private static int roundKopeyki(BigDecimal b) {
        b = b.abs();
        int k = b.multiply(BigDecimal.valueOf(100L)).remainder(BigDecimal.valueOf(100L)).setScale(0, RoundingMode.HALF_UP).intValue();
        return k;
    }

    private static String kopeyki(int k) {
        String result = "копеек";
        if(k > 10 && k < 20) {
            result = "копеек";
        } else {
            int last = k % 10;
            switch(last) {
                case 1:
                    result = "копейка";
                    break;
                case 2:
                case 3:
                case 4:
                    result = "копейки";
                    break;
                default:
                    result = "копеек";
            }
        }

        return result;
    }

    private static String rubley(BigDecimal amount) {
        BigInteger r = amount.setScale(0, RoundingMode.DOWN).toBigInteger();
        String result = "рублей";
        r = r.remainder(BigInteger.valueOf(100L));
        if(r.compareTo(BigInteger.TEN) > 0 && r.compareTo(BigInteger.valueOf(20L)) < 0) {
            result = "рублей";
        } else {
            int last = r.remainder(BigInteger.TEN).intValue();
            switch(last) {
                case 1:
                    result = "рубль";
                    break;
                case 2:
                case 3:
                case 4:
                    result = "рубля";
                    break;
                default:
                    result = "рублей";
            }
        }

        return result;
    }
}
