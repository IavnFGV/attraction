package tools;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Configuration {
    private static File currentDirectory;
    private static File defaultDirectory;
    private static Properties properties;

    public Configuration() {
    }

    public static void initialize() {
        String title = "Нарушена целостность програмы";
        String messageFileNotFound = "Не найден конфигурационный файл";
        String messageBadFile = "Сбой при чтении конфигурационного файла";
        defaultDirectory = new File("documents");
        if(!defaultDirectory.exists()) {
            defaultDirectory.mkdirs();
        }

        currentDirectory = defaultDirectory;
        properties = new Properties();
        File configurationFile = new File("configuration");
        if(!configurationFile.exists()) {
            showMessageAndExit(title, messageFileNotFound);
        }

        Document doc = null;

        try {
            SAXBuilder root = new SAXBuilder();
            doc = root.build(configurationFile);
        } catch (IOException | JDOMException var10) {
            showMessageAndExit(title, messageBadFile);
        }

        Element root1 = doc.getRootElement();
        Element directories = root1.getChild("directories");
        if(directories == null) {
            showMessageAndExit(title, messageBadFile);
        }

        Element documents = directories.getChild("documents");
        String value;
        if(documents != null) {
            value = documents.getAttributeValue("path");
            if(value != null) {
                File file = new File(value);
                if(file.exists() && file.isDirectory()) {
                    if((!file.exists() || file.isDirectory()) && file.exists() && file.isDirectory()) {
                        currentDirectory = file;
                    }
                } else if(file.mkdirs()) {
                    currentDirectory = file;
                }
            }
        }

        value = root1.getChild("gui").getChild("equipment").getChild("sidebar").getAttributeValue("width");
        properties.setProperty("equipment.sidebar.attributes.width", value);
    }

    private static void showMessageAndExit(String title, String message) {
        JOptionPane.showMessageDialog((Component)null, message, title, 0);
        System.exit(1);
    }

    public static void setCurrentDirectory(File file, boolean create) {
        currentDirectory = file;
    }

    public static File getCurrentDirectory() {
        return currentDirectory;
    }

    public static File getDefaultDirectory() {
        return defaultDirectory;
    }

    public static String getGUIProperty(String key) {
        return properties.getProperty(key);
    }

    public static Object setGUIProperty(String key, String value) {
        return properties.setProperty(key, value);
    }

    public static void save() {
        Format format = Format.getCompactFormat();
        format.setEncoding("cp1251");
        XMLOutputter out = new XMLOutputter(format);
        Document doc = new Document();
        Element root = new Element("configuration");
        doc.setRootElement(root);
        Element directories = new Element("directories");
        root.addContent(directories);
        Element documents = new Element("documents");
        Attribute documentsPath = new Attribute("path", currentDirectory.getAbsolutePath());
        documents.setAttribute(documentsPath);
        directories.addContent(documents);
        Element gui = new Element("gui");
        root.addContent(gui);
        Element equipment = new Element("equipment");
        gui.addContent(equipment);
        Element sidebar = new Element("sidebar");
        equipment.addContent(sidebar);
        Attribute attSidebarWidth = new Attribute("width", getGUIProperty("equipment.sidebar.attributes.width"));
        sidebar.setAttribute(attSidebarWidth);

        try {
            FileOutputStream stream = new FileOutputStream(new File("configuration"));
            out.output(doc, stream);
        } catch (FileNotFoundException var12) {
            ;
        } catch (IOException var13) {
            ;
        }

    }
}