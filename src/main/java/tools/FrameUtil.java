package tools;

import gui.Frame;

/**
 * @author Dmitry Tumash.
 */
public class FrameUtil {
  private static Frame currentFrame;

  public static Frame getCurrentFrame() {
    return currentFrame;
  }

  public static void setCurrentFrame(Frame currentFrame) {
    FrameUtil.currentFrame = currentFrame;
  }
}
