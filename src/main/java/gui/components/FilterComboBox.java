package gui.components;

/**
 * Created by GFH on 02.04.2016.
 */

import document.MyComboboxModel;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;

public class FilterComboBox extends JComboBox {

    public FilterComboBox(MyComboboxModel aModel) {
        super(aModel);
        this.setEditable(true);
        final JTextField textfield = (JTextField) this.getEditor().getEditorComponent();
        textfield.addKeyListener(new KeyAdapter() {
            Set<Integer> auxKeys = new HashSet<Integer>(asList(KeyEvent.VK_DOWN, KeyEvent.VK_UP, KeyEvent.VK_ENTER));

            public void keyPressed(KeyEvent ke) {
                if (auxKeys.contains(ke.getKeyCode())) {
                    return;
                }
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        String curText = textfield.getText();
                        comboFilter(curText);
                        textfield.setText(curText);
                    }
                });
            }

        });
    }

    public void comboFilter(String enteredText) {

        this.getModel().setFilterString(enteredText);
        if (getModel().getList().size() > 0) {
            SwingUtilities.invokeLater(() -> {
                this.hidePopup();
                this.showPopup();
            });
        } else {
            SwingUtilities.invokeLater(this::hidePopup);
        }
    }

    @Override
    public MyComboboxModel getModel() {
        return (MyComboboxModel) super.getModel();
    }
}