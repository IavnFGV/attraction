package gui.components;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * @author Dmitry Tumash
 */

public class HintTextField extends JTextField implements FocusListener {
    private final String hint;
    private boolean showingHint;
    private Color foregroundColor;
    private Color hintForegroundColor;

    public HintTextField(String hint) {
        super(hint);
        this.foregroundColor = Color.BLACK;
        this.hintForegroundColor = Color.LIGHT_GRAY;
        this.setForeground(this.hintForegroundColor);
        this.hint = hint;
        this.showingHint = true;
        super.addFocusListener(this);
    }

    public void focusGained(FocusEvent e) {
        if(this.getText().isEmpty()) {
            super.setText("");
            this.showingHint = false;
        }

        this.setForeground(this.foregroundColor);
    }

    public void focusLost(FocusEvent e) {
        if(this.getText().isEmpty()) {
            this.setForeground(this.hintForegroundColor);
            super.setText(this.hint);
            this.showingHint = true;
        } else {
            this.setForeground(this.foregroundColor);
        }

    }

    public String getText() {
        return this.showingHint?"":super.getText();
    }
}
