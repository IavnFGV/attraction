package gui;

import gui.special.JNode;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class EditNodeDialog extends JDialog {
    private final JPanel contentPanel = new JPanel();
    private JTextArea textArea;
    private StringBuilder before;
    private StringBuilder after;
    private JNode jNode;
    private BufferedReader in;
    private BufferedWriter out;
    String fName;

    public static void main(String[] args) {
        try {
            EditNodeDialog e = new EditNodeDialog();
            e.setDefaultCloseOperation(2);
            e.setVisible(true);
        } catch (Exception var2) {
            var2.printStackTrace();
        }

    }

    public void editNode(JNode jNode, String fileName) {
        this.jNode = jNode;
        this.fName = fileName;
        String nodeName = jNode.name;
        this.in = null;

        try {
            this.in = new BufferedReader(new FileReader(fileName));
        } catch (FileNotFoundException var6) {
            var6.printStackTrace();
            return;
        }

        this.before = new StringBuilder();
        this.after = new StringBuilder();
        StringBuilder editingText = new StringBuilder();

        try {
            String e;
            label50:
            while((e = this.in.readLine()) != null) {
                if(e.trim().contains(nodeName)) {
                    editingText.append(e + "\n");

                    while((e = this.in.readLine()) != null) {
                        if(e.trim().startsWith("+") && !e.trim().substring(1).startsWith("+")) {
                            this.after.append("\r\n" + e + "\r\n");
                            break label50;
                        }

                        editingText.append(e + "\n");
                    }
                } else {
                    this.before.append(e + "\r\n");
                }
            }

            while((e = this.in.readLine()) != null) {
                this.after.append(e + "\r\n");
            }

            this.textArea.setText(editingText.toString());
            this.in.close();
            this.setVisible(true);
        } catch (IOException var7) {
            var7.printStackTrace();
        }

    }

    public EditNodeDialog() {
        this.setBounds(100, 100, 450, 300);
        this.setModal(true);
        this.getContentPane().setLayout(new BorderLayout());
        this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.getContentPane().add(this.contentPanel, "Center");
        GridBagLayout gbl_contentPanel = new GridBagLayout();
        gbl_contentPanel.columnWidths = new int[]{89, 256, 0};
        gbl_contentPanel.rowHeights = new int[]{2, 0};
        gbl_contentPanel.columnWeights = new double[]{0.0D, 0.0D, 4.9E-324D};
        gbl_contentPanel.rowWeights = new double[]{0.0D, 4.9E-324D};
        this.contentPanel.setLayout(gbl_contentPanel);
        JScrollPane buttonPane = new JScrollPane();
        GridBagConstraints cancelButton = new GridBagConstraints();
        cancelButton.fill = 1;
        cancelButton.gridwidth = 3;
        cancelButton.gridheight = 2;
        cancelButton.anchor = 18;
        cancelButton.gridx = 0;
        cancelButton.gridy = 0;
        this.contentPanel.add(buttonPane, cancelButton);
        this.textArea = new JTextArea();
        this.textArea.setLineWrap(true);
        this.textArea.setFont(new Font("Monospaced", 0, 14));
        this.textArea.setPreferredSize(new Dimension(10, 0));
        this.textArea.setColumns(50);
        buttonPane.setViewportView(this.textArea);
        JPanel buttonPane1 = new JPanel();
        buttonPane1.setLayout(new FlowLayout(2));
        this.getContentPane().add(buttonPane1, "South");
        JButton cancelButton1 = new JButton("OK");
        cancelButton1.setActionCommand("OK");
        buttonPane1.add(cancelButton1);
        this.getRootPane().setDefaultButton(cancelButton1);
        cancelButton1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String[] strs = EditNodeDialog.this.textArea.getText().split("\n");
                EditNodeDialog.this.jNode.removeNodes();
                String[] var6 = strs;
                int var5 = strs.length;

                for(int str = 0; str < var5; ++str) {
                    String e1 = var6[str];
                    if(e1.startsWith("+") && !e1.startsWith("++")) {
                        EditNodeDialog.this.jNode.setName(e1.trim().substring(1));
                    }

                    if(e1.startsWith("++") && !e1.startsWith("+++")) {
                        String name = e1.trim().substring(2);
                        JNode n = new JNode(name);
                        EditNodeDialog.this.jNode.addNode(n);
                    }

                    if(e1.startsWith("+++") && !e1.startsWith("++++")) {
                        EditNodeDialog.this.jNode.setComment(e1.trim().substring(3));
                    }

                    if(e1.startsWith("++++")) {
                        EditNodeDialog.this.jNode.setPrice(e1.trim().substring(4));
                    }
                }

                try {
                    BufferedWriter var10 = new BufferedWriter(new FileWriter(EditNodeDialog.this.fName));
                    var10.write(EditNodeDialog.this.before.toString());
                    String[] var13 = strs;
                    int var12 = strs.length;

                    for(var5 = 0; var5 < var12; ++var5) {
                        String var11 = var13[var5];
                        var10.write(var11.trim() + "\r\n");
                    }

                    var10.write(EditNodeDialog.this.after.toString());
                    var10.flush();
                    var10.close();
                    EditNodeDialog.this.setVisible(false);
                } catch (IOException var9) {
                    var9.printStackTrace();
                    JOptionPane.showMessageDialog(EditNodeDialog.this, "Ошибка при сохранении изменений");
                    EditNodeDialog.this.setVisible(false);
                }

            }
        });
        cancelButton1 = new JButton("Cancel");
        cancelButton1.setActionCommand("Cancel");
        buttonPane1.add(cancelButton1);
        cancelButton1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                EditNodeDialog.this.setVisible(false);
            }
        });
    }
}