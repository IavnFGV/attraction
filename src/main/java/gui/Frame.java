package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;

public class Frame extends JFrame {

  private final Canvas canvas;

  public Frame() {
        super("Аттракционы");
        this.setDefaultCloseOperation(3);
    canvas = new Canvas();
        this.setContentPane(canvas);
        this.setSize(1200, 600);
        this.loadSize();
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                canvas.windowWasClosed();
                Frame.this.saveSize();
            }
        });
    }

    private void saveSize() {
        Rectangle b = this.getBounds();

        try {
            FileOutputStream e = new FileOutputStream("resources/bounds");
            ObjectOutputStream out = new ObjectOutputStream(e);
            out.writeObject(b);
            out.flush();
            out.close();
        } catch (FileNotFoundException var4) {
            var4.printStackTrace();
        } catch (IOException var5) {
            var5.printStackTrace();
        }

    }

    private void loadSize() {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("resources/bounds"));
            Rectangle e = (Rectangle)in.readObject();
            in.close();
            this.setBounds(e);
            this.revalidate();
        } catch (Exception var3) {
            var3.printStackTrace();
        }

    }

  public OrderTab getOrderTab() {
    return canvas.getOrderTab();
  }
}
