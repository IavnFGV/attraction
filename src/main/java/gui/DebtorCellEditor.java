package gui;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class DebtorCellEditor extends AbstractCellEditor implements TableCellEditor, TableCellRenderer {
    JTextArea component = new JTextArea();

    public DebtorCellEditor() {
    }

    public Component getTableCellEditorComponent(final JTable table, Object value, boolean isSelected, final int rowIndex, int vColIndex) {
        this.component.setLineWrap(true);
        this.component.setText((String)value);
        this.component.setFont(table.getFont());
        this.component.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                table.setRowHeight(rowIndex, DebtorCellEditor.this.component.getPreferredSize().height);
            }

            public void keyReleased(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
            }
        });
        return this.component;
    }

    public Object getCellEditorValue() {
        return this.component.getText();
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        return this.getTableCellEditorComponent(table, value, isSelected, row, column);
    }
}