package gui;

import gui.components.HintTextField;
import gui.special.Form;
import gui.special.JNode;
import gui.special.Sidebar;
import tools.Configuration;
import tools.EquipmentList;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Equipment extends JComponent {
  private Sidebar sidebar;
  Form form;
  JPanel bottomPanel;
  EditNodeDialog end = new EditNodeDialog();
  Equipment.EquipmentAdapter ownAdapter;
  List<Equipment.EquipmentListener> listeners;
  List<JNode> selectedEquipment;
  List<Cleanable> cleanableList;
  private JNode root;
  JButton collapseAllBtn = new JButton("Свернуть все");
  JToggleButton editToggleBtn = new JToggleButton("Редактирование");
  private final JPanel leftPanel;
  private final JSplitPane jSplitPane;
  private ActionListener al;
  private final JTextField searchTextField;

  private static final Object lock = new Object();

  public Equipment() {
    this.setLayout(new BorderLayout());

    jSplitPane = new JSplitPane();
    leftPanel = new JPanel(new BorderLayout());
    jSplitPane.setLeftComponent(leftPanel);
    final JPanel leftUpperPanel = new JPanel(new GridLayout(3, 1));
    final JPanel managementTreePanel = new JPanel(new GridLayout(1, 2));
    managementTreePanel.add(collapseAllBtn);
    managementTreePanel.add(editToggleBtn);
    leftUpperPanel.add(managementTreePanel);
    final JLabel jLabel = new JLabel("Выбор оборудования");
    final Font font = jLabel.getFont();
    final Font font1 = new Font(font.getName(), font.getStyle(), 15);
    jLabel.setFont(font1);
    final JPanel jPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    jPanel.add(jLabel);
    leftUpperPanel.add(jPanel);

    final JPanel searchPanel = new JPanel(new BorderLayout());
    searchPanel.setBorder(new EmptyBorder(5, 3, 5, 3));
    searchTextField = new HintTextField("Поиск");

    searchTextField.getDocument().addDocumentListener(new DocumentListener() {
      @Override
      public void insertUpdate(DocumentEvent e) {
        if (searchTextField.getText().equals("Поиск")) {
          return;
        }
        System.out.println("insert: " + searchTextField.getText());
        updateTree();
      }

      @Override
      public void removeUpdate(DocumentEvent e) {
        System.out.println("remove: " + searchTextField.getText());
        updateTree();
      }

      @Override
      public void changedUpdate(DocumentEvent e) {
        System.out.println("change: " + searchTextField.getText());
      }
    });


    searchPanel.add(searchTextField, BorderLayout.CENTER);
    final JPanel comp = new JPanel(new GridLayout(1, 1));
    final JButton clearSearchString = new JButton("Очистить");
    clearSearchString.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        searchTextField.setText("");
      }
    });
    comp.add(clearSearchString);
    searchPanel.add(comp, BorderLayout.EAST);
    leftUpperPanel.add(searchPanel);

    leftPanel.add(leftUpperPanel, BorderLayout.NORTH);

    add(jSplitPane, BorderLayout.CENTER);



    this.listeners = new ArrayList();
    this.selectedEquipment = new ArrayList();
    this.cleanableList = new ArrayList();

//    this.addComponentListener(adapter);
//    this.collapseAllBtn.setBounds(10, 0, 120, 25);
//    this.add(this.collapseAllBtn);
    this.collapseAllBtn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        List lst = Equipment.this.root.getChildren();
        Iterator var4 = lst.iterator();

        while (var4.hasNext()) {
          JNode n = (JNode) var4.next();
          n.setHidden(true);
          Iterator var6 = n.getChildren().iterator();

          while (var6.hasNext()) {
            JNode nn = (JNode) var6.next();
            nn.setHidden(true);
          }
        }

      }
    });
//    this.editToggleBtn.setBounds(150, 0, 120, 25);
//    this.add(this.editToggleBtn);

    /*final JLabel label = new JLabel("Выбор оборудования");
    label.setBounds(100, 15, 200, 30);
    add(label);*/


    this.editToggleBtn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        List lst = Equipment.this.root.getChildren();
        Iterator var4 = lst.iterator();

        while (var4.hasNext()) {
          JNode n = (JNode) var4.next();
          n.hideEditBtn(Equipment.this.editToggleBtn.isSelected());
          Iterator var6 = n.getChildren().iterator();

          while (var6.hasNext()) {
            JNode nn = (JNode) var6.next();
            nn.hideEditBtn(Equipment.this.editToggleBtn.isSelected());
          }
        }

      }
    });
    this.form = new Form();
    this.cleanableList.add(this.form);
    jSplitPane.setRightComponent(form);
    this.bottomPanel = new JPanel();
//    this.add(this.bottomPanel);
    fillBottomPanel();
    this.sidebar = new Sidebar();
    sidebar.setBounds(100, 30, 200, 30);
//    this.add(this.sidebar);
    fillSidebar();
    KeyAdapter keys = new KeyAdapter() {
      public void keyPressed(KeyEvent e) {
        System.out.println("pressed");
        boolean controlDown = e.isControlDown();
        int code = e.getKeyCode();
        switch (e.getKeyCode()) {
          case 32:
            Equipment.this.ownAdapter.clear();
            break;
          case 68:
            Equipment.this.ownAdapter.directory();
            break;
          case 80:
            Equipment.this.ownAdapter.print();
            break;
          case 83:
            Equipment.this.ownAdapter.save();
        }

      }
    };
    this.addKeyListener(keys);
    this.setFocusable(true);
    this.ownAdapter = new Equipment.EquipmentAdapter() {
      public void print() {
        Iterator var2 = Equipment.this.listeners.iterator();

        while (var2.hasNext()) {
          Equipment.EquipmentListener el = (Equipment.EquipmentListener) var2.next();
          el.print();
        }

      }

      public void save() {
        Iterator var2 = Equipment.this.listeners.iterator();

        while (var2.hasNext()) {
          Equipment.EquipmentListener el = (Equipment.EquipmentListener) var2.next();
          el.save();
        }

      }

      public void clear() {
        Equipment.this.selectedEquipment.clear();
        Iterator n = Equipment.this.cleanableList.iterator();
        searchTextField.setText("");
        while (n.hasNext()) {
          Cleanable lst = (Cleanable) n.next();
          lst.clear();
        }

        n = Equipment.this.listeners.iterator();

        while (n.hasNext()) {
          Equipment.EquipmentListener lst1 = (Equipment.EquipmentListener) n.next();
          lst1.clear();
        }

        List lst2 = Equipment.this.root.getChildren();
        Iterator var3 = lst2.iterator();

        while (var3.hasNext()) {
          JNode n1 = (JNode) var3.next();
          n1.setSellected(false);
          n1.setHidden(true);
          Iterator var5 = n1.getChildren().iterator();

          while (var5.hasNext()) {
            JNode nn = (JNode) var5.next();
            nn.setSellected(false);
            nn.setCount(1);
          }
        }

      }

      public void directory() {
        System.out.println("dfsdfsdfsd");
        File currentDirectory = Configuration.getCurrentDirectory();
        JFileChooser chooser = new JFileChooser(currentDirectory);
        chooser.setFileSelectionMode(1);
        chooser.showDialog(Equipment.this, "выбрать");
        File selected = chooser.getSelectedFile();
        Configuration.setCurrentDirectory(selected, false);
      }
    };
  }

  private void updateTree() {
    if (searchTextField.getText().length() > 3) {
      //start filtering
      //          root.setHidden(false);

      new Thread(new Runnable() {
        @Override
        public void run() {
          synchronized (lock) {

            root.removeNodes();
            root.updateTree();
            EquipmentList.createTree(root, al, "equipment", searchTextField.getText());

            root.repaint();
          }

        }
      }).start();

    } else {
      new Thread(new Runnable() {
        @Override
        public void run() {
          synchronized (lock) {
            root.removeNodes();
            root.updateTree();
            EquipmentList.createTree(root, al, "equipment", searchTextField.getText());

            root.repaint();
          }

        }
      }).start();

    }
  }

  void fillBottomPanel() {
    ActionListener al = new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        String command = event.getActionCommand();
        switch (command.hashCode()) {
          case -962584979:
            if (command.equals("directory")) {
              Equipment.this.ownAdapter.directory();
            }
            break;
          case 3522941:
            if (command.equals("save")) {
              Equipment.this.ownAdapter.save();
            }
            break;
          case 94746189:
            if (command.equals("clear")) {
              Equipment.this.ownAdapter.clear();
            }
            break;
          case 106934957:
            if (command.equals("print")) {
              Equipment.this.ownAdapter.print();
              Equipment.this.ownAdapter.save();
            }
        }

      }
    };
    JButton buttonDirectory = new JButton("Папка сохранения");
    buttonDirectory.setActionCommand("directory");
    buttonDirectory.addActionListener(al);
    this.bottomPanel.add(buttonDirectory);
    JButton buttonClear = new JButton("Очистить экран");
    buttonClear.addActionListener(al);
    buttonClear.setActionCommand("clear");
    this.bottomPanel.add(buttonClear);
    JButton buttonPrint = new JButton("Распечатать");
    buttonPrint.setActionCommand("print");
    buttonPrint.addActionListener(al);
    this.bottomPanel.add(buttonPrint);
    JButton buttonSave = new JButton("Сохранить");
    buttonSave.setActionCommand("save");
    buttonSave.addActionListener(al);
    this.bottomPanel.add(buttonSave);
    final JButton openDirectory = new JButton("Открыть папку");
    openDirectory.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if(Desktop.isDesktopSupported()) {
          try {
            Desktop.getDesktop().open(Configuration.getCurrentDirectory());
          } catch (IOException var3) {
            var3.printStackTrace();
          }
        }

      }
    });
    bottomPanel.add(openDirectory);
    add(bottomPanel, BorderLayout.SOUTH);
  }

  void fillSidebar() {
    String property = Configuration.getGUIProperty("equipment.sidebar.attributes.width");
    System.out.println(property);
    int width = 400;

    try {
      width = Integer.parseInt(property);
    } catch (NumberFormatException | NullPointerException var5) {
      ;
    }

    this.sidebar.setSize(width, 100);
//    this.sidebar.addComponentListener(this.getComponentListeners()[0]);
    this.root = new JNode("");
    this.cleanableList.add(this.root);
    this.root.setBounds(0, 0, 300, 25, true);
    JScrollPane wrap = new JScrollPane(this.root);
    wrap.setVerticalScrollBarPolicy(22);
    wrap.setHorizontalScrollBarPolicy(31);
    wrap.getViewport().setBackground(Color.white);
    wrap.getVerticalScrollBar().setUnitIncrement(20);
    this.sidebar.getContentPane().add(wrap);
    leftPanel.add(sidebar.getContentPane(), BorderLayout.CENTER);
//    this.sidebar.setLocation(0, 40);
    al = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        JNode node;
        if (e.getActionCommand().equals("editBtn")) {
          node = (JNode) e.getSource();
          Equipment.this.end.editNode(node, "equipment");
        }

        node = (JNode) e.getSource();
        boolean selected = node.selected();
        System.out.println(selected);
        boolean changed = false;
        if (selected) {
          if (!Equipment.this.selectedEquipment.contains(node)) {
            Equipment.this.selectedEquipment.add(node);
          }
        } else {
          Equipment.this.selectedEquipment.remove(node);
        }

        Equipment.this.form.setSelectedEquipmentList(Equipment.this.selectedEquipment);
      }
    };
    EquipmentList.createTree(this.root, al, "equipment", "");
    this.root.setHidden(false);
  }

  public void addEquipmentListener(Equipment.EquipmentListener el) {
    this.listeners.add(el);
  }

  public static class EquipmentAdapter implements Equipment.EquipmentListener {
    public EquipmentAdapter() {
    }

    public void print() {
    }

    public void save() {
    }

    public void directory() {
    }

    public void clear() {
    }
  }

  public interface EquipmentListener {
    void print();

    void save();

    void directory();

    void clear();
  }
}
