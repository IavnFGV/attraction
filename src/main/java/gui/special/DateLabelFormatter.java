package gui.special;


import javax.swing.JFormattedTextField.AbstractFormatter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author Dmitry Tumash
 */
public class DateLabelFormatter extends AbstractFormatter {
    public final static String datePattern = "dd.MM.yyyy EEEE";
    private SimpleDateFormat dateFormatter;

    public DateLabelFormatter() {
        this.dateFormatter = new SimpleDateFormat(datePattern);
    }

    public Object stringToValue(String text) throws ParseException {
        return this.dateFormatter.parseObject(text);
    }

    public String valueToString(Object value) throws ParseException {
        if(value != null) {
            Calendar cal = (Calendar)value;
            return this.dateFormatter.format(cal.getTime());
        } else {
            return "";
        }
    }
}
