package gui.special;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Sidebar extends JComponent {
    private Insets insets = new Insets(10, 10, 10, 10);
    private JPanel contentPane;

    public Sidebar() {
        this.setLayout((LayoutManager)null);
        this.contentPane = new JPanel();
        this.contentPane.setLocation(this.insets.left, this.insets.top);
        this.contentPane.setLayout(new BorderLayout());
        this.add(this.contentPane);
        ComponentAdapter listener = new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                Sidebar.this.update();
            }
        };
        this.addComponentListener(listener);
        MouseAdapter mouse = new MouseAdapter() {
            Rectangle huinyshka = new Rectangle();
            boolean resize = false;

            public void mouseMoved(MouseEvent e) {
            }

            public void mouseReleased(MouseEvent e) {
                this.resize = false;
            }

            public void mouseDragged(MouseEvent e) {
                if(this.resize) {
                    int x = e.getPoint().x;
                    if(x <= 10) {
                        return;
                    }

                    int height = Sidebar.this.getHeight();
                    Sidebar.this.setSize(x, height);
                } else if(this.mouseINResizeLine(e.getPoint())) {
                    this.resize = true;
                }

            }

            boolean mouseINResizeLine(Point mouse) {
                this.huinyshka.height = Sidebar.this.getHeight();
                this.huinyshka.width = Sidebar.this.insets.right;
                this.huinyshka.y = 0;
                this.huinyshka.x = Sidebar.this.getWidth() - Sidebar.this.insets.right;
                return this.huinyshka.contains(mouse);
            }

            void setPrefinedCursor(Point mouse) {
                boolean contains = this.mouseINResizeLine(mouse);
                if(contains) {
                    ;
                }

            }
        };
        this.addMouseListener(mouse);
        this.addMouseMotionListener(mouse);
    }

    private void update() {
        Dimension size = this.getSize();
        size.width -= this.insets.left + this.insets.right;
        size.height -= this.insets.top + this.insets.bottom;
        this.contentPane.setSize(size);
        this.repaint();
    }

    public Container getContentPane() {
        return this.contentPane;
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D gc = (Graphics2D)g;
        gc.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int height = this.getHeight();
        byte box = 3;
        byte padding = 3;
        int offsetX = this.getWidth() - this.insets.right + (this.insets.right - box) / 2;
        int offsetY = (this.getHeight() - (box * 3 + padding * 2)) / 2;
        gc.setPaint(Color.gray);
        gc.fillRect(offsetX, offsetY, box, box);
        offsetY += padding + box;
        gc.fillRect(offsetX, offsetY, box, box);
        offsetY += padding + box;
        gc.fillRect(offsetX, offsetY, box, box);
        int var10000 = offsetY + padding + box;
    }
}