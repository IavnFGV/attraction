package gui.special;

import gui.Cleanable;
import gui.SomeListener;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.GeneralPath;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class JNode extends JComponent implements Cleanable {
  public String name;
  private boolean hidden = true;
  private boolean fixed = false;
  private boolean selected = false;
  private final JComponent header;
  private final JComponent content;
  private JButton editBtn = new JButton();
  private JNode parent;
  JSpinner countS;
  private int count = 1;
  private String comment;
  private double price;
  private Double manualPrice;
  private static final byte LEFT_MARGIN = 20;
  private static final byte HEIGHT = 25;
  private JLabel label;

  private SomeListener someListener;

  public void setManualPrice(double manualPrice) {
    this.manualPrice = manualPrice;
  }

  public Double getManualPrice() {
    return manualPrice;
  }

  public void setSomeListener(SomeListener someListener) {
    this.someListener = someListener;
  }

  public double getPrice() {
    return this.price;
  }

  public void setPrice(String price) {
    double d = 0.0D;

    try {
      d = Double.parseDouble(price);
    } catch (Exception var5) {
      ;
    }

    this.price = d;
  }

  public int getIntPrice() {
    return Double.valueOf(price).intValue();
  }

  public void setName(String name) {
    this.name = name;
    this.label.setText(name);
  }

  public String getComment() {
    return this.comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public int getCount() {
    return this.count;
  }

  public void setCount(int count) {
    if (this.countS != null) {
      this.countS.setValue(Integer.valueOf(count));
    }

    this.count = count;
  }

  public void hideEditBtn(boolean hide) {
    this.editBtn.setVisible(hide);
  }

  public JNode(String name) {
    this.name = name;
    this.header = new JLabel(name);
    ((JLabel) this.header).setHorizontalAlignment(2);
    ((JLabel) this.header).setBackground(Color.orange);
    this.header.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        JNode.this.setHidden(!JNode.this.hidden);
      }
    });
    this.add(this.header);
    this.content = new JPanel();
    this.content.setLayout((LayoutManager) null);
    this.content.setOpaque(false);
    this.add(this.content);
    this.addMouseListener();
  }

  public void setSellected(boolean b) {
    this.selected = false;
    if (this.countS != null && !b) {
      this.countS.setVisible(false);
    }

  }

  public JNode(String name, final ActionListener al, String actionCommand, String comment, String price) {
    this.name = name;
    this.comment = comment;
    this.setPrice(price);
    this.header = new JPanel();
    this.header.setLayout(new BorderLayout());
    this.header.setOpaque(false);
    this.add(this.header);
    this.countS = new JSpinner(new SpinnerNumberModel(1, 1, 99999, 1));
    this.countS.setVisible(false);
    this.countS.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent arg0) {


        JNode.this.count = ((Integer) JNode.this.countS.getValue()).intValue();
        ActionEvent ae = new ActionEvent(JNode.this, 0, "");
        if (someListener != null) {
          someListener.stateChanged();
        }
        al.actionPerformed(ae);
      }
    });
    this.editBtn.setIcon(new ImageIcon("resources/images/edit.png"));
    this.editBtn.setVisible(false);
    this.editBtn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        ActionEvent event = new ActionEvent(JNode.this, 0, "editBtn");
        al.actionPerformed(event);
      }
    });
    this.editBtn.setMargin(new Insets(0, 0, 0, 0));
    this.editBtn.setMaximumSize(new Dimension(23, 23));
    this.editBtn.setPreferredSize(new Dimension(23, 23));
    JPanel p = new JPanel();
    p.setLayout(new BorderLayout());
    p.add(this.countS, "West");
    p.add(this.editBtn, "East");
    this.header.add(p, "East");
    final JCheckBox checkBox = new JCheckBox();
    checkBox.setOpaque(false);
    this.header.add(checkBox, "West");
    ActionListener l = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        e.setSource(JNode.this);
        JNode.this.selected = checkBox.isSelected();
        JNode.this.countS.setVisible(JNode.this.selected);
        al.actionPerformed(e);
      }
    };
    checkBox.setActionCommand(actionCommand);
    checkBox.addActionListener(l);
    this.label = new JLabel(name);
    this.label.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        JNode.this.setHidden(!JNode.this.hidden);
      }
    });
    this.header.add(this.label);
    this.content = new JPanel();
    this.content.setLayout((LayoutManager) null);
    this.content.setOpaque(false);
    this.add(this.content);
    this.addMouseListener();
  }

  private void addMouseListener() {
    MouseAdapter mouse = new MouseAdapter() {
      Rectangle huinyshka = new Rectangle(20, 20);

      public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        if (JNode.this.parent != null) {
          new Rectangle(JNode.this.header.getSize());
          if (this.huinyshka.contains(e.getPoint())) {
            JNode.this.setHidden(!JNode.this.hidden);
          }

        }
      }
    };
    this.addMouseListener(mouse);
  }

  public void setBounds(int x, int y, int width, int height) {
    height = this.preferredHeight();
    if (this.parent == null) {
      this.header.setLocation(0, 0);
      this.header.setSize(width, 25);
      this.content.setLocation(5, 25);
      this.content.setSize(width - 5, height - 25);
    } else {
      this.header.setLocation(20, 0);
      this.header.setSize(width - 20, 25);
      this.content.setLocation(25, 25);
      this.content.setSize(width - 20 - 5, height - 25);
    }

    super.setBounds(x, y, width, height);
    super.setPreferredSize(new Dimension(width, height));
  }

  public void setBounds(int x, int y, int width, int height, boolean update) {
    if (!update) {
      super.setPreferredSize(new Dimension(width, height));
      super.setBounds(x, y, width, height);
    } else {
      this.setBounds(x, y, width, height);
    }

  }

  public void setBounds(Rectangle r) {
    this.setBounds(r.x, r.y, r.width, r.height);
  }

  public void setSize(Dimension d) {
    this.setBounds(this.getLocation().x, this.getLocation().y, d.width, d.height);
  }

  public void setSize(int width, int height) {
    this.setBounds(this.getLocation().x, this.getLocation().y, width, height);
  }

  public void setSize(int width, int height, boolean update) {
    if (!update) {
      super.setSize(width, height);
      super.setPreferredSize(new Dimension(width, height));
    } else {
      this.setSize(width, height);
    }

  }

  private int preferredHeight() {
    if (this.hidden) {
      return 25;
    } else {
      int sum = 25;
      Component[] var5;
      int var4 = (var5 = this.content.getComponents()).length;

      for (int var3 = 0; var3 < var4; ++var3) {
        Component comp = var5[var3];
        sum += ((JNode) comp).preferredHeight();
      }

      return sum;
    }
  }

  public int updateTree() {
    int height = this.preferredHeight();
    int width = this.getWidth();
    this.setSize(width, height, false);
    int y = 0;
    Component[] var7;
    int var6 = (var7 = this.content.getComponents()).length;

    for (int var5 = 0; var5 < var6; ++var5) {
      Component comp = var7[var5];
      comp.setLocation(comp.getLocation().x, y);
      y += comp.getHeight();
    }

    if (this.parent != null) {
      this.parent.updateTree();
    }

    return 0;
  }

  public void setHidden(boolean hidden) {
    this.hidden = hidden;
    this.content.setVisible(!hidden);
    this.updateTree();
    this.repaint();
  }

  public void addNode(JNode node) {
    node.parent = this;
    int width = this.content.getWidth();
    int y = this.content.getHeight();
    node.setBounds(0, y, width, 25);
    this.content.add(node);
    this.content.setSize(width, y + 25);
    this.updateTree();
  }

  public void delNode(JNode node) {
  }

  public void removeNodes() {
    this.content.removeAll();
  }

  public List<JNode> getChildren() {
    ArrayList children = new ArrayList();
    Component[] var5;
    int var4 = (var5 = this.content.getComponents()).length;

    for (int var3 = 0; var3 < var4; ++var3) {
      Component comp = var5[var3];
      children.add((JNode) comp);
    }

    return children;
  }

  public boolean selected() {
    return this.selected;
  }

  public void clear() {
    clear(this);
  }

  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    if (!this.fixed) {
      int childCount = this.content.getComponentCount();
      if (childCount > 0) {
        if (this.parent != null) {
          GeneralPath triangle = new GeneralPath();
          byte base = 10;
          int middle = base / 2;
          double gc;
          double y;
          if (this.hidden) {
            gc = (double) ((20 - middle) / 2);
            y = (double) ((25 - base) / 2);
            triangle.moveTo(gc, y);
            triangle.lineTo(gc + (double) middle, y + (double) middle);
            triangle.lineTo(gc, y + (double) base);
            triangle.closePath();
          } else {
            gc = (double) ((20 - base) / 2);
            y = (double) ((25 - middle) / 2);
            triangle.moveTo(gc, y);
            triangle.lineTo(gc + (double) middle, y + (double) middle);
            triangle.lineTo(gc + (double) base, y);
            triangle.closePath();
          }

          Graphics2D gc1 = (Graphics2D) g;
          gc1.setPaint(Color.white);
          gc1.fillRect(0, 0, this.getWidth(), this.getHeight());
          gc1.setPaint(Color.gray);
          gc1.fill(triangle);
        }
      }
    }
  }

  public static void clear(JNode node) {
    if (node.header instanceof JPanel) {
      Component[] child = node.header.getComponents();
      Component[] var5 = child;
      int var4 = child.length;

      for (int var3 = 0; var3 < var4; ++var3) {
        Component c = var5[var3];
        if (c instanceof JCheckBox) {
          ((JCheckBox) c).setSelected(false);
        }

        if (c instanceof JSpinner) {
          c.setVisible(false);
        }
      }
    }

    Iterator var7 = node.getChildren().iterator();

    while (var7.hasNext()) {
      JNode var6 = (JNode) var7.next();
      clear(var6);
    }

  }
}
