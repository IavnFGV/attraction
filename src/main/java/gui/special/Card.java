package gui.special;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.ImageObserver;

/**
 * @author Dmitry Tumash
 */
public class Card extends JComponent {
    String name;
    String phone;
    Image image;
    JCheckBox box;

    public Card(String name, String phone, Image image) {
        this.name = name;
        this.phone = phone;
        this.image = image.getScaledInstance(150, 225, 4);
        this.setLayout((LayoutManager)null);
        final JPanel header = new JPanel();
        header.setLayout(new BorderLayout());
        this.add(header);
        JTextArea nameLabel = new JTextArea(name);
        nameLabel.setSelectionColor(Color.orange);
        nameLabel.setFont(new Font("Verdana", 1, 12));
        header.add(nameLabel);
        this.box = new JCheckBox();
        this.box.setActionCommand(name + " " + phone);
        header.add(this.box, "East");
        final JTextArea phoneArea = new JTextArea(phone);
        phoneArea.setSelectionColor(Color.orange);
        phoneArea.setFont(new Font("Verdana", 0, 12));
        this.add(phoneArea);
        ComponentAdapter listener = new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                byte fieldHeight = 20;
                int width = Card.this.getWidth();
                int height = Card.this.getHeight();
                header.setSize(width, fieldHeight);
                header.setLocation(0, height - fieldHeight * 2);
                phoneArea.setSize(width, fieldHeight);
                phoneArea.setLocation(0, height - fieldHeight);
            }
        };
        this.addComponentListener(listener);
    }

    public void addActionListener(ActionListener al) {
        this.box.addActionListener(al);
    }

    void delActionListener(ActionListener al) {
        this.box.removeActionListener(al);
    }

    public void clear() {
        this.box.setSelected(false);
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(this.image, 0, 0, (ImageObserver)null);
    }
}

