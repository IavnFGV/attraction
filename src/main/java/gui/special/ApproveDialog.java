/*
 * Created by JFormDesigner on Mon Jun 22 23:35:49 MSK 2015
 */

package gui.special;

import tools.ApproveListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author dmitry tumash
 */
public class ApproveDialog extends JDialog {

  private ApproveListener myListener;

  public ApproveDialog(Frame owner, ApproveListener listener) {
    super(owner);
    myListener = listener;
    initComponents();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    // Generated using JFormDesigner Evaluation license - dmitry tumash
    dialogPane = new JPanel();
    contentPanel = new JPanel();
    panel1 = new JPanel();
    label1 = new JLabel();
    buttonBar = new JPanel();
    okButton = new JButton();
    okButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        myListener.update(true);
        setVisible(false);
      }
    });

    cancelButton = new JButton();
    cancelButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        myListener.update(false);
        setVisible(false);
      }
    });

    //======== this ========
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== dialogPane ========
    {
      dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));

      // JFormDesigner evaluation mark
      dialogPane.setLayout(new BorderLayout());

      //======== contentPanel ========
      {
        contentPanel.setLayout(new GridLayout());

        //======== panel1 ========
        {
          panel1.setLayout(new FlowLayout());

          //---- label1 ----
          label1.setText("\u0412\u044b \u0443\u0432\u0435\u0440\u0435\u043d\u044b \u0447\u0442\u043e \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0430 \u043c\u0435\u043d\u044c\u0448\u0435 1000 \u0440\u0443\u0431\u043b\u0435\u0439?");
          panel1.add(label1);
        }
        contentPanel.add(panel1);
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);

      //======== buttonBar ========
      {
        buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
        buttonBar.setLayout(new GridBagLayout());
        ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 85, 80};
        ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0};

        //---- okButton ----
        okButton.setText("\u0414\u0430");
        buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- cancelButton ----
        cancelButton.setText("\u041d\u0435\u0442");
        buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  // Generated using JFormDesigner Evaluation license - dmitry tumash
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JPanel panel1;
  private JLabel label1;
  private JPanel buttonBar;
  private JButton okButton;
  private JButton cancelButton;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
