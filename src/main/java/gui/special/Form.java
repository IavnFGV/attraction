package gui.special;

import es.esy.iavnfgv.fx.component.datepicker.DatePickerWithWeek;
import gui.Cleanable;
import gui.components.HintTextField;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Form extends JComponent implements Cleanable {
    public final JTextArea comment = new JTextArea();
    public final JTextArea contacts = new JTextArea();
    public final JTextArea list = new JTextArea();
    public final JTextField myEventAdminName = new HintTextField("Мероприятие обслуживает");
    private final JPanel panel4 = new JPanel();
    public DatePickerWithWeek datePicker;
    private boolean shortEquipmentList = false;
    private List<JNode> selectedEquipment = new ArrayList();
    private JCheckBox beznalCB = new JCheckBox("Оплата по безналу");
    private JLabel nalLbl = new JLabel("Наличными");
    private JTextField nalTF = new JTextField(15);
    private JFXPanel calendarPanel = new JFXPanel();

    public Form() {

        JPanel panel3 = new JPanel();

        JPanel panel5 = new JPanel();
        JPanel panel7 = new JPanel();
        JPanel panel6 = new JPanel();

        this.setLayout(new GridLayout(2, 0));

        final JPanel upperPanel = new JPanel();
        upperPanel.setLayout(new BorderLayout());


        // JFormDesigner evaluation mark
        panel3.setLayout(new BorderLayout());

        //======== panel4 ========
//        initPanel4();
//        panel3.add(panel4);
        panel3.add(calendarPanel, BorderLayout.WEST);
        calendarPanel.setBackground(Color.gray);
//        calendarPanel.setPreferredSize(new Dimension(100,40));
//        panel3.setPreferredSize(new Dimension(100,40));
        Platform.runLater(() -> initFX(calendarPanel));


        //======== panel5 ========
        {
            panel5.setLayout(new GridLayout());

            //======== panel7 ========
            {
                panel7.setLayout(new GridLayout());
            }
            panel5.add(panel7);
        }
        panel3.add(panel5);

        //======== panel6 ========
        {
            panel6.setLayout(new GridLayout());
//      myEventAdminName.setMinimumSize(new Dimension(300, 15));
//      myEventAdminName.setMaximumSize(new Dimension(300, 15));
//      myEventAdminName.setPreferredSize(new Dimension(300, 15));
//      myEventAdminName.setFont(new Font("Verdana", 0, 14));
            panel6.add(myEventAdminName);
            panel6.setMinimumSize(new Dimension(300, 26));
            panel6.setMaximumSize(new Dimension(300, 26));
            panel6.setPreferredSize(new Dimension(300, 26));

        }

        JPanel panelForMyEventAdminName = new JPanel(new BorderLayout());
        panelForMyEventAdminName.add(panel6, BorderLayout.NORTH);
        panel3.add(panelForMyEventAdminName, BorderLayout.EAST);

        upperPanel.add(panel3, BorderLayout.NORTH);

        final JPanel upperCenterPanel = new JPanel();
        upperCenterPanel.setLayout(new GridLayout(2, 1));

        final JPanel commentsPanel = new JPanel();
        commentsPanel.setLayout(new BorderLayout());
        addLabel("Комментарии к заказу", commentsPanel);
        comment.setLineWrap(true);
        comment.setWrapStyleWord(true);
        Font font = new Font("Verdana", 0, 14);
        comment.setFont(font);
        comment.setSelectionColor(Color.orange);
        final JScrollPane jScrollPane = new JScrollPane(comment);
        Border border = BorderFactory.createLineBorder(Color.lightGray);
        jScrollPane.setBorder(border);
        jScrollPane.setEnabled(false);
        jScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        commentsPanel.add(jScrollPane, BorderLayout.CENTER);

        upperCenterPanel.add(commentsPanel);

        final JPanel contactsPanel = new JPanel();
        contactsPanel.setLayout(new BorderLayout());
        addLabel("Контакты", contactsPanel);
        contacts.setLineWrap(true);
        contacts.setWrapStyleWord(true);
        Font font1 = new Font("Verdana", 0, 14);
        contacts.setFont(font1);
        contacts.setSelectionColor(Color.orange);
        final JScrollPane jScrollPane1 = new JScrollPane(contacts);
        Border border1 = BorderFactory.createLineBorder(Color.lightGray);
        jScrollPane1.setBorder(border1);
        jScrollPane1.setEnabled(false);
        jScrollPane1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        contactsPanel.add(jScrollPane1, BorderLayout.CENTER);

        upperCenterPanel.add(contactsPanel);

        upperPanel.add(upperCenterPanel);
        add(upperPanel);


        JPanel panelEquipment = new JPanel();
        panelEquipment.add(this.nalLbl);
        panelEquipment.add(this.nalTF);
        panelEquipment.add(this.beznalCB);
        this.beznalCB.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent arg0) {
                Form.this.updSelectedEquipmentList();
            }
        });
        JLabel labelEquipment = new JLabel("Список оборудования");
        labelEquipment.setFont(new Font("Verdana", 0, 14));
        panelEquipment.add(labelEquipment);
        final JButton buttonShortEquipment = new JButton("Кратко");
        buttonShortEquipment.setEnabled(!this.shortEquipmentList);
        panelEquipment.add(buttonShortEquipment);
        final JButton buttonLongEquipment = new JButton("Подробно");
        panelEquipment.add(buttonLongEquipment);
        buttonLongEquipment.setEnabled(this.shortEquipmentList);
        panelEquipment.setMaximumSize(new Dimension(10000, 35));
        upperPanel.add(panelEquipment, BorderLayout.SOUTH);


        list.setLineWrap(true);
        list.setWrapStyleWord(true);

        list.setLineWrap(true);
        list.setWrapStyleWord(true);
        Font font2 = new Font("Verdana", 0, 14);
        list.setFont(font2);
        list.setSelectionColor(Color.orange);
        final JScrollPane jScrollPane2 = new JScrollPane(list);
        Border border2 = BorderFactory.createLineBorder(Color.lightGray);
        jScrollPane2.setBorder(border2);
        jScrollPane2.setEnabled(false);
        jScrollPane2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        add(jScrollPane2);

//    this.addField(this.list, new Dimension(10000, 1000), (Dimension) null);
        ActionListener al = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String command = e.getActionCommand();
                switch (command.hashCode()) {
                    case 3327612:
                        if (command.equals("long")) {
                            this.update(false);
                        }
                        break;
                    case 109413500:
                        if (command.equals("short")) {
                            this.update(true);
                        }
                }

            }

            void update(boolean shortMode) {
                Form.this.shortEquipmentList = shortMode;
                buttonLongEquipment.setEnabled(Form.this.shortEquipmentList);
                buttonShortEquipment.setEnabled(!Form.this.shortEquipmentList);
                Form.this.updSelectedEquipmentList();
            }
        };
        buttonLongEquipment.setActionCommand("long");
        buttonLongEquipment.addActionListener(al);
        buttonShortEquipment.setActionCommand("short");
        buttonShortEquipment.addActionListener(al);
    }

    private void initFX(JFXPanel fxPanel) {
        // This method is invoked on the JavaFX thread
        Scene scene = createScene();
        fxPanel.setScene(scene);
    }

    private void addLabel(String title, JComponent form) {
        Font font = new Font("Verdana", 0, 14);
        JLabel label = new JLabel(title);
        label.setOpaque(true);
        label.setBackground(new Color(240, 240, 240));
        label.setFont(font);
        label.setAlignmentX(0.5F);
        label.setHorizontalAlignment(0);
//    label.setMaximumSize(new Dimension(10000, 25));
        form.add(label, BorderLayout.NORTH);
    }

    public void updSelectedEquipmentList() {
        StringBuilder builder = new StringBuilder();
        int counter = 1;

        for (Iterator var4 = this.selectedEquipment.iterator(); var4.hasNext(); ++counter) {
            JNode node = (JNode) var4.next();
            builder.append(counter);
            builder.append(". ");
            builder.append(node.name);
            if (node.getCount() > 1 && node.getCount() < 5) {
                builder.append(" (");
                builder.append(node.getCount());
                builder.append(" КОМПЛЕКТА)");
            } else if (node.getCount() > 4) {
                builder.append(" (");
                builder.append(node.getCount());
                builder.append(" КОМПЛЕКТОВ)");
            }

            builder.append('\n');
            if (!this.shortEquipmentList) {
                Iterator var6 = node.getChildren().iterator();

                while (var6.hasNext()) {
                    JNode child = (JNode) var6.next();
                    builder.append("      -  ");
                    builder.append(child.name);
                    builder.append('\n');
                }
            }

            if (node.getComment() != null) {
                builder.append("      ");
                builder.append(node.getComment());
                builder.append('\n');
            }
        }

        this.list.setText(builder.toString());
    }

    private Scene createScene() {
        Group root = new Group();
        Scene scene = new Scene(root);

        datePicker = new DatePickerWithWeek();
        datePicker.setDatePattern(DateLabelFormatter.datePattern);
        root.getChildren().add(datePicker);
        datePicker.setPrefSize(250, 50);
        return (scene);
    }

    public boolean isBeznalChecked() {
        return this.beznalCB.isSelected();
    }

  /*private void addField(JTextComponent c, final Dimension max, final Dimension preferredSize) {
    Font font = new Font("Verdana", 0, 14);
    c.setFont(font);
    c.setSelectionColor(Color.orange);
    Border border = BorderFactory.createLineBorder(Color.lightGray);
    final JScrollPane wrap = new JScrollPane(c);
    wrap.setBorder(border);
    wrap.setEnabled(false);
    wrap.setVerticalScrollBarPolicy(20);
    if (max != null) {
      wrap.setMaximumSize(max);
      if (preferredSize != null) {
        c.addComponentListener(new ComponentAdapter() {
          public void componentResized(ComponentEvent e) {
            Dimension newDimension = e.getComponent().getSize();
            if (newDimension.getHeight() < preferredSize.getHeight() && newDimension.getHeight() > max.getHeight()) {
              wrap.setMaximumSize(new Dimension(max.width, newDimension.height));
              wrap.setSize(new Dimension(max.width, newDimension.height));
              wrap.setPreferredSize(new Dimension(max.width, newDimension.height));
            }

            wrap.repaint();
          }
        });
      }

      wrap.setMaximumSize(max);
    }

    this.add(wrap);
  }*/

    public String getNal() {
        return this.nalTF.getText();
    }

    public void setSelectedEquipmentList(List<JNode> selected) {
        this.selectedEquipment = selected;
        this.updSelectedEquipmentList();
    }

    public void clear() {
        this.myEventAdminName.setText((String) null);
//        initPanel4();
        Platform.runLater(() -> datePicker.getDatePicker().setCalendar(null));
        this.comment.setText((String) null);
        this.contacts.setText((String) null);
        this.list.setText((String) null);
        this.nalTF.setText("");
        this.beznalCB.setSelected(false);
        if (this.selectedEquipment != null) {
            this.selectedEquipment.clear();
        }

    }
}