package gui;

import gui.calendar.JDateChooser;
import gui.components.HintTextField;
import gui.special.ApproveDialog;
import gui.special.JNode;
import gui.special.Sidebar;
import net.sourceforge.jdatepicker.impl.UtilCalendarModel;
import tools.ApproveListener;
import tools.EquipmentList;
import tools.FrameUtil;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class OrderTab extends JComponent {

    private static final Object lock = new Object();
    final JCheckBox particularyCB = new JCheckBox("Подробно");
    private final JTable table;
    private final JTextArea resultArea;
    private final JPanel leftPanel;
    private final JTextField searchTextField;
    List<JNode> selectedEquipment;
    EditNodeDialog end = new EditNodeDialog();
    List<Cleanable> cleanableList;
    JButton collapseAllBtn = new JButton("Свернуть все");
    JToggleButton editToggleBtn = new JToggleButton("Редактирование");
    JCheckBox deliveryIncudedCB = new JCheckBox("Доставка включена в стоимость");
    private Sidebar sidebar;
    private JNode root;
    private JDateChooser datePicker;
    //  private JTextField startTF = new JTextField(10);
//  private JTextField endTF = new JTextField(10);
    private JTextField discountTF = new JTextField(10);
    private JTextField myDliveryPriceTF = new JTextField(10);
    private JTextField percentTF = new JTextField(10) {
        {
            this.setText("10");
        }
    };
    private Font taFont = new Font("SansSerif", 0, 14);
    private JButton copyBtn = new JButton("Копировать");
    private JButton clearBtn = new JButton("Очистить");
    private JTextArea reportTA = new JTextArea(10, 50);
    private ActionListener al;

    public OrderTab() {
        setLayout(new GridLayout());

        final JSplitPane jSplitPane = new JSplitPane();
        add(jSplitPane);
        leftPanel = new JPanel(new BorderLayout());
        jSplitPane.setLeftComponent(leftPanel);
        final JPanel leftUpperPanel = new JPanel(new GridLayout(3, 1));
        final JPanel managementTreePanel = new JPanel(new GridLayout(1, 2));
        managementTreePanel.add(collapseAllBtn);
        managementTreePanel.add(editToggleBtn);
        leftUpperPanel.add(managementTreePanel);
        final JLabel jLabel = new JLabel("Расчет заказа");
        final Font font = jLabel.getFont();
        final Font font1 = new Font(font.getName(), font.getStyle(), 15);
        jLabel.setFont(font1);
        final JPanel jPanel111 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        jPanel111.add(jLabel);
        leftUpperPanel.add(jPanel111);

        final JPanel searchPanel = new JPanel(new BorderLayout());
        searchPanel.setBorder(new EmptyBorder(5, 3, 5, 3));
        searchTextField = new HintTextField("Поиск");

        searchTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                if (searchTextField.getText().equals("Поиск")) {
                    return;
                }
                System.out.println("insert: " + searchTextField.getText());
                updateTree();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                System.out.println("remove: " + searchTextField.getText());
                updateTree();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                System.out.println("change: " + searchTextField.getText());
            }
        });


        searchPanel.add(searchTextField, BorderLayout.CENTER);
        final JPanel comp11 = new JPanel(new GridLayout(1, 1));
        final JButton clearSearchString = new JButton("Очистить");
        clearSearchString.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                searchTextField.setText("");
            }
        });
        comp11.add(clearSearchString);
        searchPanel.add(comp11, BorderLayout.EAST);
        leftUpperPanel.add(searchPanel);
        leftPanel.add(leftUpperPanel, BorderLayout.NORTH);

        this.selectedEquipment = new ArrayList();
        this.cleanableList = new ArrayList();
        this.reportTA.setFont(this.taFont);
        this.reportTA.setLineWrap(true);
        Locale.setDefault(new Locale("ru", "RU"));
        Calendar cal = Calendar.getInstance(new Locale("ru", "RU"));
        UtilCalendarModel ucm = new UtilCalendarModel(cal);
        final Locale aLocale = new Locale("ru", "RU");
        final JDateChooser jDateChooser = new JDateChooser(gui.special.DateLabelFormatter.datePattern, false);
        jDateChooser.setLocale(aLocale);
        datePicker = jDateChooser;
//    this.datePicker.getJFormattedTextField().setBackground(Color.white);
        this.sidebar = new Sidebar();
//    this.collapseAllBtn.setBounds(10, 0, 120, 25);
//    this.add(this.collapseAllBtn);
        this.collapseAllBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                List lst = OrderTab.this.root.getChildren();
                Iterator var4 = lst.iterator();

                while (var4.hasNext()) {
                    JNode n = (JNode) var4.next();
                    n.setHidden(true);
                    Iterator var6 = n.getChildren().iterator();

                    while (var6.hasNext()) {
                        JNode nn = (JNode) var6.next();
                        nn.setHidden(true);
                    }
                }

            }
        });
        this.editToggleBtn.setBounds(150, 0, 120, 25);
//    this.add(this.editToggleBtn);
        this.editToggleBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                List lst = OrderTab.this.root.getChildren();
                Iterator var4 = lst.iterator();

                while (var4.hasNext()) {
                    JNode n = (JNode) var4.next();
                    n.hideEditBtn(OrderTab.this.editToggleBtn.isSelected());
                    Iterator var6 = n.getChildren().iterator();

                    while (var6.hasNext()) {
                        JNode nn = (JNode) var6.next();
                        nn.hideEditBtn(OrderTab.this.editToggleBtn.isSelected());
                    }
                }

            }
        });
        this.fillSidebar();
        leftPanel.add(this.sidebar.getContentPane(), BorderLayout.CENTER);
        this.setFocusable(true);
        JLabel discountL = new JLabel("Скидка:");
        JLabel deliveryPriceL = new JLabel("Стоимость доставки:");
        JLabel percentL = new JLabel("% по безналу:");
        JPanel enterPanel = new JPanel();
        enterPanel.setLayout(new GridLayout(2, 3));
        JPanel jPanel;

        jPanel = new JPanel(new GridLayout(1, 2));
        jPanel.setBorder(new EmptyBorder(0, 40, 0, 0));
        final JPanel jPanel3 = new JPanel(new GridLayout(1, 1));
        jPanel3.add(discountL);
        jPanel.add(jPanel3);
        final JPanel jPanel2 = new JPanel(new GridLayout());
        jPanel2.setBorder(new EmptyBorder(5, 0, 5, 0));
        jPanel2.add(discountTF);
        jPanel.add(jPanel2);
        enterPanel.add(jPanel);


        jPanel = new JPanel(new GridLayout(1, 2));
        jPanel.setBorder(new EmptyBorder(0, 40, 0, 0));
        final JPanel jPanel31 = new JPanel(new GridLayout(1, 1));
        jPanel31.add(percentL);
        jPanel.add(jPanel31);
        final JPanel jPanel21 = new JPanel(new GridLayout());
        jPanel21.setBorder(new EmptyBorder(5, 0, 5, 0));
        jPanel21.add(percentTF);
        jPanel.add(jPanel21);
        enterPanel.add(jPanel);

        jPanel = new JPanel(new GridLayout(1, 2));
        jPanel.setBorder(new EmptyBorder(0, 40, 0, 0));
        final JPanel jPanel312 = new JPanel(new GridLayout(1, 1));
        jPanel312.add(deliveryPriceL);
        jPanel.add(jPanel312);
        final JPanel jPanel212 = new JPanel(new GridLayout());
        jPanel212.setBorder(new EmptyBorder(5, 0, 5, 0));
        jPanel212.add(myDliveryPriceTF);
        jPanel.add(jPanel212);
        enterPanel.add(jPanel);


        final JPanel comp = new JPanel(new FlowLayout(FlowLayout.LEFT));
        comp.setBorder(new EmptyBorder(0, 33, 0, 0));
        comp.add(this.particularyCB);
        enterPanel.add(comp);
        enterPanel.add(new JPanel());
        enterPanel.add(new JPanel());


        JPanel eastPanel = new JPanel();
        eastPanel.setLayout(new BorderLayout());
        eastPanel.add(enterPanel, "North");
        JScrollPane sp = new JScrollPane();
        table = new JTable();

        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setRowHeight(32);

        final File file = new File("config.properties");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }


        final OrderTabTableModel dataModel = new OrderTabTableModel(selectedEquipment, this);
        table.setModel(dataModel);
        ((OrderTabTableModel) table.getModel()).setCheckBox(particularyCB);


        try {
            final FileReader fileReader = new FileReader(file);
            final Properties properties = new Properties();
            properties.load(fileReader);

            final String numberw = properties.getProperty("numberw");
            if (numberw != null) {
                table.getColumn("№").setPreferredWidth(Integer.valueOf(numberw));
                table.getColumn("Название").setPreferredWidth(Integer.valueOf(properties.getProperty("namew")));
                table.getColumn("Цена").setPreferredWidth(Integer.valueOf(properties.getProperty("pricew")));
                table.getColumn("Количество").setPreferredWidth(Integer.valueOf(properties.getProperty("countw")));
                table.getColumn("Сумма").setPreferredWidth(Integer.valueOf(properties.getProperty("summw")));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        OrderTabCountEditor orderTabCountEditor = new OrderTabCountEditor(selectedEquipment);
        OrderTabPriceEditor orderTabPriceEditor = new OrderTabPriceEditor(selectedEquipment, this);

        table.getColumnModel().getColumn(3).setCellEditor(orderTabCountEditor);
        table.getColumnModel().getColumn(2).setCellEditor(orderTabPriceEditor);
        final TableCellRenderer simpleRenderer = new TableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                final JPanel jPanel1 = new JPanel(new GridLayout());

                final JPanel jPanel4 = new JPanel(new FlowLayout());
                final JLabel jLabel1 = new JLabel(String.valueOf(value));
                jLabel1.setFont(new Font("SansSerif", 0, 14));
                jPanel4.add(jLabel1);
                jPanel1.add(jPanel4);
                return jPanel4;
            }
        };
        table.getColumn("№").setCellRenderer(simpleRenderer);
        table.getColumn("Название").setCellRenderer(simpleRenderer);


        table.getColumn("Название").setCellRenderer(new TableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                final JPanel jPanel1 = new JPanel(new GridLayout());

                final JPanel jPanel4 = new JPanel(new GridLayout());
                final JTextArea jTextArea = new JTextArea(10, 1);
                jTextArea.setText(String.valueOf(value));
                jTextArea.setFont(new Font("SansSerif", 0, 14));
                jTextArea.setBackground(new Color(240, 240, 240));
                jTextArea.setEditable(false);
                jTextArea.setLineWrap(true);

                jPanel4.add(jTextArea);
                jPanel1.add(jPanel4);
                if (particularyCB.isSelected()) {
                    table.setRowHeight(row, 6 * 32);
                } else {
                    table.setRowHeight(32);
                }
                return jPanel4;
            }
        });
        table.getColumn("Сумма").setCellRenderer(simpleRenderer);
        table.getColumn("Цена").setCellRenderer(new OrderTabPriceEditor(selectedEquipment, this));
        table.getColumn("Количество").setCellRenderer(new OrderTabCountEditor(selectedEquipment)/*simpleRenderer*/);


        sp.setViewportView(table);
        final JPanel jPanel4 = new JPanel(new BorderLayout());
        jPanel4.add(sp, BorderLayout.CENTER);

        final JPanel comp1 = new JPanel(new GridLayout(1, 1));
        this.resultArea = new JTextArea(5, 1);
        resultArea.setLineWrap(true);
        resultArea.setFont(this.taFont);
        comp1.add(this.resultArea);
        jPanel4.add(comp1, BorderLayout.SOUTH);

        eastPanel.add(jPanel4, BorderLayout.CENTER);


        JPanel southP = new JPanel();
        southP.add(this.copyBtn, "South");
        southP.add(this.clearBtn, "South");
        eastPanel.add(southP, "South");
        jSplitPane.setRightComponent(eastPanel);
        this.initListeners();
    }

    public JTable getTable() {
        return table;
    }

    private void updateTree() {
        if (searchTextField.getText().length() > 3) {
            //start filtering
            //          root.setHidden(false);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    synchronized (lock) {

                        root.removeNodes();
                        root.updateTree();
                        EquipmentList.createTree(root, al, "order", searchTextField.getText());

                        root.repaint();
                    }

                }
            }).start();

        } else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    synchronized (lock) {
                        root.removeNodes();
                        root.updateTree();
                        EquipmentList.createTree(root, al, "order", searchTextField.getText());

                        root.repaint();
                    }

                }
            }).start();

        }
    }

    private void initListeners() {
        this.clearBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                searchTextField.setText("");
                OrderTab.this.selectedEquipment.clear();
                ((OrderTabTableModel) table.getModel()).fireTableDataChanged();
                JNode.clear(OrderTab.this.root);
                List lst = OrderTab.this.root.getChildren();
                Iterator var4 = lst.iterator();

                while (var4.hasNext()) {
                    JNode n = (JNode) var4.next();
                    n.setSellected(false);
                    n.setHidden(true);
                    Iterator var6 = n.getChildren().iterator();

                    while (var6.hasNext()) {
                        JNode nn = (JNode) var6.next();
                        nn.setSellected(false);
                        nn.setCount(1);
                    }
                }

                OrderTab.this.discountTF.setText("");
                OrderTab.this.myDliveryPriceTF.setText("");
                OrderTab.this.datePicker.setDate(new Date());
//        OrderTab.this.endTF.setText("");
//        OrderTab.this.startTF.setText("");
                OrderTab.this.myDliveryPriceTF.setText("");
                OrderTab.this.updateReport();
            }
        });
        this.discountTF.getDocument().addDocumentListener(new DocumentListener() {
            public void removeUpdate(DocumentEvent arg0) {
                OrderTab.this.updateReport();
            }

            public void insertUpdate(DocumentEvent arg0) {
                OrderTab.this.updateReport();
            }

            public void changedUpdate(DocumentEvent arg0) {
                OrderTab.this.updateReport();
            }
        });
        this.deliveryIncudedCB.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent arg0) {
                OrderTab.this.updateReport();
            }
        });
        this.myDliveryPriceTF.getDocument().addDocumentListener(new DocumentListener() {
            public void removeUpdate(DocumentEvent arg0) {
                OrderTab.this.updateReport();
            }

            public void insertUpdate(DocumentEvent arg0) {
                OrderTab.this.updateReport();
            }

            public void changedUpdate(DocumentEvent arg0) {
                OrderTab.this.updateReport();
            }
        });

        this.particularyCB.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent arg0) {
                ((OrderTabTableModel) table.getModel()).fireTableDataChanged();
                OrderTab.this.updateReport();
            }
        });
        this.copyBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                StringSelection ss = new StringSelection(OrderTab.this.reportTA.getText());
                Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, (ClipboardOwner) null);
            }
        });

        myDliveryPriceTF.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                System.out.println("Gained");
            }

            @Override
            public void focusLost(FocusEvent e) {
                final String text = myDliveryPriceTF.getText();
                if (!text.isEmpty()) {
                    final Double aDouble = Double.valueOf(text);
                    if (aDouble < 1000) {
                        final ApproveDialog approveDialog = new ApproveDialog(FrameUtil.getCurrentFrame(), new ApproveListener() {
                            @Override
                            public void update(boolean result) {
                                if (!result) {
                                    myDliveryPriceTF.setText("0");
                                }
                            }
                        });
                        approveDialog.setVisible(true);
                        approveDialog.setModal(true);
                    }
                }
            }
        });

        this.percentTF.getDocument().addDocumentListener(new DocumentListener() {
            public void removeUpdate(DocumentEvent arg0) {
                OrderTab.this.updateReport();
            }

            public void insertUpdate(DocumentEvent arg0) {
                OrderTab.this.updateReport();
            }

            public void changedUpdate(DocumentEvent arg0) {
                OrderTab.this.updateReport();
            }
        });
    }

    private int getPercent() {
        int result = 10;

        try {
            result = Integer.parseInt(this.percentTF.getText());
        } catch (Exception var3) {
            ;
        }

        return result;
    }

    void fillSidebar() {
//    short width = 400;
//    this.sidebar.setPreferredSize(new Dimension(width, 500));
//    this.sidebar.setMinimumSize(new Dimension(width, 500));
//    this.sidebar.setMaximumSize(new Dimension(1000, 1000));
        this.root = new JNode("");
        this.root.setBounds(0, 0, 300, 25, true);
        JScrollPane wrap = new JScrollPane(this.root);
        wrap.setVerticalScrollBarPolicy(22);
        wrap.setHorizontalScrollBarPolicy(31);
        wrap.getViewport().setBackground(Color.white);
        wrap.getVerticalScrollBar().setUnitIncrement(20);
        this.sidebar.getContentPane().add(wrap);
        al = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JNode node;
                if (e.getActionCommand().equals("editBtn")) {
                    node = (JNode) e.getSource();
                    OrderTab.this.end.editNode(node, "equipment-zakaz");

                    node.setSomeListener(new SomeListener() {
                        @Override
                        public void stateChanged() {
                            ((OrderTabTableModel) table.getModel()).fireTableDataChanged();
                        }
                    });

                }

                node = (JNode) e.getSource();
                node.setSomeListener(new SomeListener() {
                    @Override
                    public void stateChanged() {
                        ((OrderTabTableModel) table.getModel()).fireTableDataChanged();
                    }
                });
                boolean selected = node.selected();
                System.out.println(selected);
                boolean changed = false;
                if (selected) {
                    if (!OrderTab.this.selectedEquipment.contains(node)) {
                        OrderTab.this.selectedEquipment.add(node);
                        ((OrderTabTableModel) table.getModel()).fireTableDataChanged();
                    }
                } else {
                    OrderTab.this.selectedEquipment.remove(node);
                    ((OrderTabTableModel) table.getModel()).fireTableDataChanged();
                }

                OrderTab.this.updateReport();
            }
        };
        EquipmentList.createTree(this.root, al, "order", "");
        this.root.setHidden(false);
    }

    private double getDicsount() {
        String dStr = this.discountTF.getText();
        double result = 0.0D;

        try {
            result = Double.parseDouble(dStr);
        } catch (NumberFormatException var5) {
            return 0.0D;
        }

        return result <= 99.0D ? result : 0.0D;
    }

    private double getdeliveryCost() {
        String dStr = this.myDliveryPriceTF.getText();
        double result = 0.0D;

        try {
            result = Double.parseDouble(dStr);
            return result;
        } catch (NumberFormatException var5) {
            this.myDliveryPriceTF.setText("");
            return 0.0D;
        }
    }

    private boolean isDeliveryCostIncluded() {
        return this.deliveryIncudedCB.isSelected();
    }

    public void updateReport() {
        this.reportTA.setText("");
        StringBuilder sb = new StringBuilder();

        JNode beznal_dost;
        Iterator var5;
        for (int total = 0; total < this.selectedEquipment.size(); ++total) {
            JNode curNode = this.selectedEquipment.get(total);
            sb.append(total + 1 + ". ");
            sb.append(curNode.name);
            sb.append(" - ").append(curNode.getCount()).append(" шт");
            final double price = curNode.getManualPrice() == null ? curNode.getPrice() * (double) curNode.getCount() : curNode.getManualPrice();
            if (price != 0.0D) {
                sb.append(" - ");
                sb.append((int) price);
                if (this.getDicsount() != 0.0D) {
                    if (this.getDicsount() > 0.0D) {
                        sb.append(" (с учетом скидки в ");
                    } else {
                        sb.append(" (с учетом наценки в ");
                    }
                    sb.append((int) this.getDicsount() < 0 ? (int) this.getDicsount() * -1 : (int) this.getDicsount());
                    sb.append("% - ");
                    sb.append((int) (price * (1.0D - this.getDicsount() / 100.0D)));
                    sb.append(")");
                }
            }

            sb.append("\n");
            if (this.particularyCB.isSelected() && curNode.getChildren().size() > 0) {
                var5 = curNode.getChildren().iterator();

                while (var5.hasNext()) {
                    beznal_dost = (JNode) var5.next();
                    sb.append("  - ");
                    sb.append(beznal_dost.name);
                    sb.append("\n");
                }
            }
        }

        double var6 = 0.0D;
        var5 = this.selectedEquipment.iterator();

        while (var5.hasNext()) {
            beznal_dost = (JNode) var5.next();
            final double v = beznal_dost.getManualPrice() == null ? beznal_dost.getPrice() * (double) beznal_dost.getCount() : beznal_dost.getManualPrice();
            if (v != 0.0D) {
                var6 += v;
            }
        }

        calculateSummary(sb, var6);

        this.reportTA.setText(sb.toString());

        final StringBuilder sb1 = new StringBuilder();
        calculateSummary(sb1, var6);

        resultArea.setText(sb1.toString());

    }

    private void calculateSummary(StringBuilder sb, double var6) {
        if (this.selectedEquipment.size() > 0) {
            sb.append("Итого при оплате наличными: ");
            sb.append(Double.valueOf(var6).intValue());
            if (this.getDicsount() != 0.0D) {
                if (this.getDicsount() > 0.0D) {
                    sb.append(" (с учетом скидки в ");
                } else {
                    sb.append(" (с учетом наценки в ");
                }
                sb.append(this.getDicsount() < 0 ? (int) this.getDicsount() * -1 : (int) this.getDicsount());
                sb.append("% - ");
                sb.append(String.format("%.2f", (var6 * (1.0D - this.getDicsount() / 100.0D))));
                sb.append(")");
            }

            sb.append("\n");
            sb.append("При оплате по безналу +  " + this.getPercent() + "%, т.е. - ");
            double var7 = (var6 * (1.0D - this.getDicsount() / 100.0D) * (1.0D + (double) this.getPercent() / 100.0D));
            sb.append(Double.valueOf(var7).intValue());
            sb.append(", в том числе НДС 18% - ");
            final Double aDouble = Double.valueOf((double) (var7 * 18) / 118.0D);
            sb.append(aDouble.intValue());
            sb.append("\n");
        }

        if (this.getdeliveryCost() != 0.0D && this.selectedEquipment.size() > 0) {
            sb.append("Доставка - ");
            sb.append((int) this.getdeliveryCost());
            sb.append("\n");
            sb.append("При оплате наличными с учетом доставки : ");
            final double v = var6 * (1.0D - this.getDicsount() / 100.0D) + this.getdeliveryCost();
            sb.append(Double.valueOf(v).intValue());
            sb.append("\n");
            sb.append("При оплате по безналу с учетом доставки : ");
            double var8 = var6 * (1.0D - this.getDicsount() / 100.0D) * (1.0D + (double) this.getPercent() / 100.0D) + this.getdeliveryCost() * 1.1D;
            final Double aDouble = Double.valueOf(var8);
            sb.append(aDouble.intValue());
            sb.append(", в том числе НДС 18% - ");
            final Double aDouble1 = Double.valueOf(var8 * 18.0D / 118.0D);
            sb.append(aDouble1.intValue());
            sb.append("\n");
        } else if (this.selectedEquipment.size() > 0) {
            sb.append("Доставка включена в стоимость\n");
        }
    }


}
