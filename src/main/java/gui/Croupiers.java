package gui;

import gui.special.Card;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Croupiers extends JComponent {
    final JPanel contentPane = new JPanel();
    List<ActionListener> listeners = new ArrayList();

    public Croupiers() {
        final JScrollPane wrap = new JScrollPane(this.contentPane);
        JScrollBar vertical = wrap.getVerticalScrollBar();
        vertical.setUnitIncrement(20);
        wrap.setVerticalScrollBarPolicy(20);
        this.setLayout(new BorderLayout());
        this.add(wrap);
        ComponentAdapter listener = new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                int width = wrap.getWidth();
                int count = Croupiers.this.contentPane.getComponentCount();
                int hgap = ((FlowLayout)Croupiers.this.contentPane.getLayout()).getHgap() + 150;
                int vgap = ((FlowLayout)Croupiers.this.contentPane.getLayout()).getVgap() + 275;
                int inHorz = width / hgap;
                int height = count * vgap;
                if(inHorz > 0) {
                    height = count / inHorz * vgap;
                    if(count % inHorz > 0) {
                        height += vgap;
                    }
                }

                Dimension preferredSize = new Dimension(width, height);
                Croupiers.this.contentPane.setPreferredSize(preferredSize);
            }
        };
        wrap.addComponentListener(listener);
        FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(".jpg");
            }
        };
        File resourcesDirectory = new File("resources/images/croupiers");
        File[] images = resourcesDirectory.listFiles(filter);
        FlowLayout layout = new FlowLayout(1, 40, 100);
        this.contentPane.setLayout(layout);
        StringBuilder nameBuilder = new StringBuilder();
        StringBuilder phoneBuilder = new StringBuilder();
        ActionListener al = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Iterator var3 = Croupiers.this.listeners.iterator();

                while(var3.hasNext()) {
                    ActionListener al = (ActionListener)var3.next();
                    al.actionPerformed(e);
                }

            }
        };
        File[] var14 = images;
        int var13 = images.length;

        for(int var12 = 0; var12 < var13; ++var12) {
            File img = var14[var12];

            try {
                BufferedImage image = ImageIO.read(img);
                this.parce(img.getName(), nameBuilder, phoneBuilder);
                Card card = new Card(nameBuilder.toString(), phoneBuilder.toString(), image);
                card.setPreferredSize(new Dimension(150, 275));
                card.addActionListener(al);
                this.contentPane.add(card);
            } catch (IOException var17) {
                ;
            }
        }

    }

    public void clear() {
        Component[] var4;
        int var3 = (var4 = this.contentPane.getComponents()).length;

        for(int var2 = 0; var2 < var3; ++var2) {
            Component comp = var4[var2];
            ((Card)comp).clear();
        }

    }

    void parce(String string, StringBuilder name, StringBuilder phone) {
        name.setLength(0);
        phone.setLength(0);
        boolean c = false;
        int i = 0;

        for(boolean readName = true; i < string.length(); ++i) {
            char var7 = string.charAt(i);
            if(var7 == 45) {
                readName = false;
            } else if(readName) {
                name.append((char)var7);
            } else {
                if(var7 == 46) {
                    break;
                }

                phone.append((char)var7);
            }
        }

    }

    public void addActionListener(ActionListener al) {
        this.listeners.add(al);
    }
}

