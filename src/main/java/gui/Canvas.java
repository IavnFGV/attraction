package gui;

/**
 * @author Dmitry Tumash
 */

import bugalteriya.Bugalteriya;
import document.DocumentPanel;
import gui.Equipment.EquipmentAdapter;
import gui.special.Form;
import tools.DocumentFactory;
import tools.documentfactory.Doc4jDocumentFactory;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class Canvas extends JTabbedPane {
    final Equipment equipmentTab = new Equipment();
    final Animators animatorsTab = new Animators();
    final Croupiers croupiersTab = new Croupiers();
    final CardNumbersTab cardNumbersTab = new CardNumbersTab();
    final CarTab carTab = new CarTab();
    final NewDebtorTab debtTab = new NewDebtorTab();
    final OrderTab orderTab = new OrderTab();
    DocumentPanel documentPanel = new DocumentPanel();
    Bugalteriya bug;
    private Map<JTextComponent, UndoManager> managerMap = new HashMap<>();

    public Canvas() {
        this.bug = new Bugalteriya(this.documentPanel.getDocStateTM());
        this.addTab("Выбор оборудования", this.equipmentTab);
        this.addTab("Аниматоры", this.animatorsTab);
        this.addTab("Крупье", this.croupiersTab);
        this.addTab("Машины", this.carTab);
        this.addTab("Номера карточек", this.cardNumbersTab);
        this.addTab("Долги", this.debtTab);
        this.addTab("Расчет заказа", this.orderTab);
        this.addTab("Документы", this.documentPanel);
        this.addTab("Бухгалтерия", this.bug);
//    this.addTab("Выбор оборудования", new Equipment1());
        final HashSet humansList = new HashSet();
        ActionListener al = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof JCheckBox) {
                    boolean selected = ((JCheckBox) source).isSelected();
                    if (selected) {
                        humansList.add(e.getActionCommand());
                    } else {
                        humansList.remove(e.getActionCommand());
                    }
                }

            }
        };
        this.animatorsTab.addActionListener(al);
        this.croupiersTab.addActionListener(al);
        EquipmentAdapter ea = new EquipmentAdapter() {
            public void print() {
                super.print();
                Form form = Canvas.this.equipmentTab.form;
                String eventServs = form.myEventAdminName.getText();

//                final Date date1 = form.datePicker.getDate();
                final Date date1 =  form.datePicker.getDatePicker().getCalendar().getTime();
                final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateLabelFormatter.datePattern, new Locale("ru", "RU"));
                simpleDateFormat.format(date1);
                String date = simpleDateFormat.format(date1);

                String comments = form.comment.getText();
                String contacts = form.contacts.getText();
                StringBuilder humans = new StringBuilder();
                int counter = 1;

                String equipment;
                for (Iterator notes = humansList.iterator(); notes.hasNext(); ++counter) {
                    equipment = (String) notes.next();
                    humans.append(counter);
                    humans.append('.');
                    humans.append(' ');
                    humans.append(equipment);
                    humans.append('\n');
                }

                equipment = form.list.getText();
                StringBuilder var16 = new StringBuilder();
                String fName = "resources/gi.html";
                File gif = new File(fName);
                if (gif.exists()) {
                    try {
                        BufferedReader in = new BufferedReader(new FileReader(fName));
                        String str = null;

                        while ((str = in.readLine()) != null) {
                            var16.append(str + "\n");
                        }
                    } catch (FileNotFoundException var14) {
                    } catch (IOException var15) {
                    }
                }
                DocumentFactory documentFactory = new Doc4jDocumentFactory();// XWPFDocumentFactory();
                documentFactory.print(date, eventServs, comments, contacts, humans.toString(), equipment, var16
                        .toString(), form.isBeznalChecked(), form.getNal());
            }

            public void save() {
                super.save();
                Form form = Canvas.this.equipmentTab.form;
                String eventServs = form.myEventAdminName.getText();

//                final Date date1 = form.datePicker.;
                final Date date1 = form.datePicker.getDatePicker().getCalendar().getTime();
                final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateLabelFormatter.datePattern, new Locale("ru", "RU"));
                simpleDateFormat.format(date1);
                String date = simpleDateFormat.format(date1);

                String comments = form.comment.getText();
                String contacts = form.contacts.getText();
                StringBuilder humans = new StringBuilder();
                int counter = 1;

                String equipment;
                for (Iterator notes = humansList.iterator(); notes.hasNext(); ++counter) {
                    equipment = (String) notes.next();
                    humans.append(counter);
                    humans.append('.');
                    humans.append(' ');
                    humans.append(equipment);
                    humans.append('\n');
                }

                equipment = form.list.getText();
                StringBuilder var16 = new StringBuilder();
                String fName = "resources/gi.html";
                File gif = new File(fName);
                if (gif.exists()) {
                    try {
                        BufferedReader in = new BufferedReader(new FileReader(fName));
                        String str = null;

                        while ((str = in.readLine()) != null) {
                            var16.append(str + "\n");
                        }
                    } catch (FileNotFoundException var14) {
                    } catch (IOException var15) {
                    }
                }
                DocumentFactory documentFactory = new Doc4jDocumentFactory();// XWPFDocumentFactory();
                documentFactory.create(date, eventServs, comments, contacts, humans.toString(), equipment, var16
                        .toString(), form.isBeznalChecked(), form.getNal());
            }

            public void clear() {
                super.clear();
                Canvas.this.animatorsTab.clear();
                Canvas.this.croupiersTab.clear();
                humansList.clear();
            }
        };
        this.equipmentTab.addEquipmentListener(ea);
        File directory = new File("resources/images");
        File fileEquipmentPNG = new File(directory, "equipment.png");
        this.setIcon(fileEquipmentPNG, 0);
        File fileAnimatorPNG = new File(directory, "animator.png");
        this.setIcon(fileAnimatorPNG, 1);
        File fileCroupierPNG = new File(directory, "croupier.png");
        this.setIcon(fileCroupierPNG, 2);
        File carsPNG = new File(directory, "cars.png");
        this.setIcon(carsPNG, 3);
        File cardsPNG = new File(directory, "cards.png");
        this.setIcon(cardsPNG, 4);
        File debtPNG = new File(directory, "debt.png");
        this.setIcon(debtPNG, 5);

        addUndoRedoForTextFields();
    }

    boolean setIcon(File file, int index) {
        try {
            Image e = ImageIO.read(file).getScaledInstance(16, 16, 4);
            ImageIcon icon = new ImageIcon(e);
            this.setIconAt(index, icon);
            return true;
        } catch (IOException var5) {
            return false;
        }
    }

    private void addUndoRedoForTextFields() {
        synchronized (getTreeLock()) { // maybe it is unnecessary but lets do as doc says for getComponents();
            java.util.List<JTextComponent> components = harvestMatches(this, JTextComponent.class);
            components.stream()
            .filter(jTextComponent -> !(jTextComponent instanceof JFormattedTextField ))
                    .forEach(this::bindUndoToTextComponent);
        }
    }

    public <T extends Component> java.util.List<T> harvestMatches(Container root, Class<T> clazz) {
        java.util.List<Container> containers = new LinkedList<>();
        java.util.List<T> harvested = new ArrayList<>();

        containers.add(root);
        while (!containers.isEmpty()) {
            Container container = containers.remove(0);
            for (Component component : container.getComponents()) {
                if (clazz.isAssignableFrom(component.getClass())) {
                    harvested.add((T) component);
                } else if (component instanceof Container) {
                    containers.add((Container) component);
                }
            }
        }

        return Collections.unmodifiableList(harvested);
    }

    public OrderTab getOrderTab() {
        return orderTab;
    }

    private void bindUndoToTextComponent(JTextComponent jTextComponent) {
        UndoManager undo = new UndoManager();
        Document doc = jTextComponent.getDocument();
        managerMap.put(jTextComponent, undo);
        // Listen for undo and redo events
        doc.addUndoableEditListener(new UndoableEditListener() {
            public void undoableEditHappened(UndoableEditEvent evt) {
                undo.addEdit(evt.getEdit());

            }
        });

        // Create an undo action and add it to the text component
        jTextComponent.getActionMap().put("Undo",
                new AbstractAction("Undo") {
                    public void actionPerformed(ActionEvent evt) {
                        try {
                            if (undo.canUndo()) {
                                undo.undo();
                            }
                        } catch (CannotUndoException e) {
                        }
                    }
                });

        // Bind the undo action to ctl-Z
        jTextComponent.getInputMap().put(KeyStroke.getKeyStroke("control Z"), "Undo");

        // Create a redo action and add it to the text component
        jTextComponent.getActionMap().put("Redo",
                new AbstractAction("Redo") {
                    public void actionPerformed(ActionEvent evt) {
                        try {
                            if (undo.canRedo()) {
                                undo.redo();
                            }
                        } catch (CannotRedoException e) {
                        }
                    }
                });

        // Bind the redo action to ctl-Y
        jTextComponent.getInputMap().put(KeyStroke.getKeyStroke("control Y"), "Redo");
    }

    public void windowWasClosed() {
        this.debtTab.save();
        this.bug.save();
    }
}

