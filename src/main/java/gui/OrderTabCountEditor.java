package gui;

import gui.special.JNode;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

/**
 * @author Dmitry Tumash.
 *
 * in order to implement task #12
 * add List<JNode> selectedEquipment because we don`t know node during constructor call
 */
public class OrderTabCountEditor extends AbstractCellEditor implements TableCellEditor, TableCellRenderer {

    private JPanel content;
    private JTextField countTextField;
    private List<JNode> selectedEquipment;
    private JNode node;

    public OrderTabCountEditor(List<JNode> selectedEquipment) {
        this.selectedEquipment = selectedEquipment;
        content = new JPanel(new BorderLayout());
        final JPanel jPanel = new JPanel(new GridLayout());
        final JButton increaseCountButton = new JButton("+");
        increaseCountButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Integer integer = Integer.valueOf(countTextField.getText());
                integer++;
                countTextField.setText(String.valueOf(integer));
                node.setCount(integer);
            }
        });
        jPanel.add(increaseCountButton);
        content.add(jPanel, BorderLayout.EAST);

        final JPanel jPanel1 = new JPanel(new GridLayout());
        countTextField = new JTextField();
        countTextField.setFont(new Font("SansSerif", 0, 14));


        jPanel1.add(countTextField);
        content.add(jPanel1, BorderLayout.CENTER);

        final JPanel jPanel2 = new JPanel(new GridLayout());
        JButton decreaseCountButton = new JButton("-");
        decreaseCountButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Integer integer = Integer.valueOf(countTextField.getText());
                integer--;
                node.setCount(integer);
                countTextField.setText(String.valueOf(integer));
            }
        });
        countTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                int i = 0;
                try {
                    i = Integer.parseInt(countTextField.getText());
                } catch (Throwable t) {
                    t.printStackTrace();
                    return;
                }
                node.setCount(i);
            }

            @Override
            public void keyReleased(KeyEvent e) {
                int i = 0;
                try {
                    i = Integer.parseInt(countTextField.getText());
                } catch (Throwable t) {
                    t.printStackTrace();
                    return;
                }
                node.setCount(i);
            }
        });
        jPanel2.add(decreaseCountButton);
        content.add(jPanel2, BorderLayout.WEST);
    }

    @Override
    public Object getCellEditorValue() {
        return countTextField.getText();
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        return getTableCellEditorComponent(table, value, isSelected, row, column);
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        countTextField.setText(String.valueOf(value));
        //we cant get node in constructor so have to set it during rendering
        node = selectedEquipment.get(row);
         return content;
    }
}
