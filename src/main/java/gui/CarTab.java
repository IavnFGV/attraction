package gui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class CarTab extends JComponent {
    private static final String FILENAME = "resources/cars.txt";
    private JTextArea textArea = new JTextArea();

    public CarTab() {
        Font font = new Font("Verdana", 0, 14);
        this.textArea.setFont(font);
        this.textArea.setEditable(false);
        this.textArea.setLineWrap(true);
        this.textArea.setWrapStyleWord(true);
        this.getContentFromFile();
        this.setLayout(new BorderLayout());
        this.setBorder(new EmptyBorder(10, 10, 10, 10));
        JScrollPane scroll = new JScrollPane(this.textArea, 22, 31);
        this.add(scroll);
    }

    private void getContentFromFile() {
        StringBuilder contents = new StringBuilder();

        try {
            BufferedReader ex = new BufferedReader(new FileReader(new File("resources/cars.txt")));

            try {
                String line = null;

                while((line = ex.readLine()) != null) {
                    contents.append(line);
                    contents.append(System.getProperty("line.separator"));
                }
            } finally {
                ex.close();
            }
        } catch (IOException var8) {
            var8.printStackTrace();
        }

        this.textArea.setText(contents.toString());
    }
}