/*
 * Created by JFormDesigner on Fri Jun 19 22:06:30 MSK 2015
 */

package gui;

import javax.swing.*;
import java.awt.*;

/**
 * @author unknown
 */
public class Equipment1 extends JComponent {
  public Equipment1() {
    initComponents();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    // Generated using JFormDesigner Evaluation license - dmitry tumash
    panel3 = new JPanel();
    panel4 = new JPanel();
    textField2 = new JTextField();
    panel5 = new JPanel();
    panel7 = new JPanel();
    panel6 = new JPanel();
    textField4 = new JTextField();
    splitPane1 = new JSplitPane();

    //======== this ========
    setLayout(new BorderLayout());

    //======== panel3 ========
    {

      // JFormDesigner evaluation mark
      panel3.setBorder(new javax.swing.border.CompoundBorder(
        new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
          "JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
          javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
          java.awt.Color.red), panel3.getBorder())); panel3.addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

      panel3.setLayout(new GridLayout(1, 3));

      //======== panel4 ========
      {
        panel4.setLayout(new BorderLayout());
        panel4.add(textField2, BorderLayout.WEST);
      }
      panel3.add(panel4);

      //======== panel5 ========
      {
        panel5.setLayout(new GridLayout());

        //======== panel7 ========
        {
          panel7.setLayout(new GridLayout());
        }
        panel5.add(panel7);
      }
      panel3.add(panel5);

      //======== panel6 ========
      {
        panel6.setLayout(new GridLayout());
        panel6.add(textField4);
      }
      panel3.add(panel6);
    }
    add(panel3, BorderLayout.NORTH);
    add(splitPane1, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  // Generated using JFormDesigner Evaluation license - dmitry tumash
  private JPanel panel3;
  private JPanel panel4;
  private JTextField textField2;
  private JPanel panel5;
  private JPanel panel7;
  private JPanel panel6;
  private JTextField textField4;
  private JSplitPane splitPane1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
