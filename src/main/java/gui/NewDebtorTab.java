package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NewDebtorTab extends JPanel {
    private JTable table;
    private DebtorTableModel dtm = new DebtorTableModel();

    public NewDebtorTab() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[3];
        gridBagLayout.rowHeights = new int[2];
        gridBagLayout.columnWeights = new double[]{0.0D, 1.0D, 4.9E-324D};
        gridBagLayout.rowWeights = new double[]{1.0D, 4.9E-324D};
        this.setLayout(gridBagLayout);
        JButton btnNewButton = new JButton("Добавить долг");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                NewDebtorTab.this.dtm.addDebtor();
            }
        });
        GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
        gbc_btnNewButton.anchor = 11;
        gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
        gbc_btnNewButton.gridx = 0;
        gbc_btnNewButton.gridy = 0;
        this.add(btnNewButton, gbc_btnNewButton);
        JScrollPane scrollPane = new JScrollPane();
        GridBagConstraints gbc_scrollPane = new GridBagConstraints();
        gbc_scrollPane.gridwidth = 2;
        gbc_scrollPane.gridheight = 2;
        gbc_scrollPane.fill = 1;
        gbc_scrollPane.gridx = 1;
        gbc_scrollPane.gridy = 0;
        this.add(scrollPane, gbc_scrollPane);
        this.table = new JTable();
        this.table.setModel(this.dtm);
        DefaultCellEditor dce = new DefaultCellEditor(new JTextField());
        dce.setClickCountToStart(1);
        this.table.getColumnModel().getColumn(0).setCellEditor(dce);
        this.table.getColumnModel().getColumn(1).setCellEditor(dce);
        this.table.getColumnModel().getColumn(2).setCellEditor(new DebtorCellEditor());
        this.table.getColumnModel().getColumn(2).setCellRenderer(new DebtorCellEditor());
        this.table.getColumnModel().getColumn(3).setCellEditor(new DebtorCellEditor());
        this.table.getColumnModel().getColumn(3).setCellRenderer(new DebtorCellEditor());
        this.table.setRowSelectionAllowed(false);
        this.table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        this.table.setAutoResizeMode(4);
        this.table.getColumnModel().getColumn(0).setPreferredWidth(100);
        this.table.getColumnModel().getColumn(1).setPreferredWidth(100);
        this.table.getColumnModel().getColumn(2).setPreferredWidth(300);
        this.table.getColumnModel().getColumn(3).setPreferredWidth(300);
        this.table.getColumnModel().getColumn(4).setPreferredWidth(70);
        scrollPane.setViewportView(this.table);
    }

    public void save() {
        this.dtm.saveDebtors();
    }
}
