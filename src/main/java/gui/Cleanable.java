package gui;

public interface Cleanable {
    void clear();
}
