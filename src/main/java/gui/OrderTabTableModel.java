package gui;

import gui.special.JNode;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.Iterator;
import java.util.List;

/**
 * @author Dmitry Tumash.
 */
public class OrderTabTableModel extends AbstractTableModel {

    JCheckBox checkBox;
    private List<JNode> list;
    private OrderTab tab;

    public OrderTabTableModel(List<JNode> list, OrderTab tab) {
        this.list = list;
        this.tab = tab;
    }

    public JCheckBox getCheckBox() {
        return checkBox;
    }

    public void setCheckBox(JCheckBox checkBox) {
        this.checkBox = checkBox;
    }

    public int getRowCount() {
        return list.size();
    }

    public int getColumnCount() {
        return 5;
    }

    public Object getValueAt(int arg0, int arg1) {
        final JNode jNode = list.get(arg0);
        switch (arg1) {
            case 0:
                return arg0 + 1;
            case 1:
                if (checkBox.isSelected()) {
                    String sb = jNode.name + '\n';
                    Iterator var5 = jNode.getChildren().iterator();
                    while (var5.hasNext()) {
                        JNode beznal_dost = (JNode) var5.next();
                        sb = sb + "  - ";
                        sb = sb + beznal_dost.name;
                        sb = sb + "\n";
                    }
                    return sb;
                } else {
                    return jNode.name;
                }
            case 2:
                return jNode.getIntPrice();
            case 3:
                return jNode.getCount();
            case 4:
                return jNode.getIntPrice() * jNode.getCount();
            default:
                return "oOO";
        }
    }

    public String getColumnName(int c) {
        switch (c) {
            case 0:
                return "№";
            case 1:
                return "Название";
            case 2:
                return "Цена";
            case 3:
                return "Количество";
            case 4:
                return "Сумма";
            default:
                return "oOO";
        }
    }

    public boolean isCellEditable(int r, int c) {
        switch (c) {
            case 0:
                return false;
            case 1:
                return false;
            case 4:
                return false;
            default:
                return true;
        }
    }

    public void setValueAt(Object val, int r, int c) {
        //when rendering  table after adding and deleting items we can get r>size
        if (r >= list.size()) return;
        final JNode jNode = list.get(r);

        switch (c) {
            case 2:
                jNode.setPrice(String.valueOf(val));
                fireTableDataChanged();
                tab.updateReport();
                return;
            case 3:
                jNode.setCount(Integer.valueOf(String.valueOf(val)));
                fireTableDataChanged();
                tab.updateReport();
                return;
            default:
        }
    }

}
