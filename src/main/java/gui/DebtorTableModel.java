package gui;

import javax.swing.table.AbstractTableModel;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class DebtorTableModel extends AbstractTableModel {
    private static final String DEBTORFILE = "resources/debtors";
    private List<DebtorTableModel.Debtor> list = new ArrayList();

    public DebtorTableModel() {
        this.loadDebtors();
    }

    public void loadDebtors() {
        try {
            ObjectInputStream e = new ObjectInputStream(new FileInputStream("resources/debtors"));
            this.list = (List)e.readObject();
            e.close();
        } catch (IOException | ClassNotFoundException var2) {
            var2.printStackTrace();
        }

    }

    public void saveDebtors() {
        ListIterator iter = this.list.listIterator();

        while(iter.hasNext()) {
            if(((DebtorTableModel.Debtor)iter.next()).delete) {
                iter.remove();
            }
        }

        try {
            ObjectOutputStream e = new ObjectOutputStream(new FileOutputStream("resources/debtors"));
            e.writeObject(this.list);
            e.flush();
            e.close();
        } catch (IOException var3) {
            var3.printStackTrace();
        }

    }

    public int getRowCount() {
        return this.list.size();
    }

    public int getColumnCount() {
        return 5;
    }

    public void addDebtor() {
        this.list.add(new DebtorTableModel.Debtor());
        this.fireTableDataChanged();
    }

    public boolean isCellEditable(int row, int col) {
        return true;
    }

    public void setValueAt(Object val, int row, int col) {
        switch(col) {
            case 0:
                ((DebtorTableModel.Debtor)this.list.get(row)).name = (String)val;
                break;
            case 1:
                ((DebtorTableModel.Debtor)this.list.get(row)).sum = (String)val;
                break;
            case 2:
                ((DebtorTableModel.Debtor)this.list.get(row)).forWhat = (String)val;
                break;
            case 3:
                ((DebtorTableModel.Debtor)this.list.get(row)).notes = (String)val;
                break;
            case 4:
                ((DebtorTableModel.Debtor)this.list.get(row)).delete = ((Boolean)val).booleanValue();
        }

    }

    public String getColumnName(int col) {
        switch(col) {
            case 0:
                return "Должник";
            case 1:
                return "Сумма долга";
            case 2:
                return "За что должен";
            case 3:
                return "Примечания";
            case 4:
                return "Удалить?";
            default:
                return null;
        }
    }

    public Class<?> getColumnClass(int col) {
        switch(col) {
            case 4:
                return Boolean.class;
            default:
                return String.class;
        }
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        DebtorTableModel.Debtor d = (DebtorTableModel.Debtor)this.list.get(rowIndex);
        switch(columnIndex) {
            case 0:
                return d.name;
            case 1:
                return d.sum;
            case 2:
                return d.forWhat;
            case 3:
                return d.notes;
            case 4:
                return Boolean.valueOf(d.delete);
            default:
                return null;
        }
    }

    static class Debtor implements Serializable {
        String name = "";
        String sum = "";
        String forWhat = "";
        String notes = "";
        boolean delete;

        Debtor() {
        }
    }
}