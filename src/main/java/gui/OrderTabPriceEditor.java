package gui;

import gui.special.JNode;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

/**
 * @author Dmitry Tumash.
 *
 * in order to implement task #12
 * add List<JNode> selectedEquipment because we don`t know node during constructor call
 */
public class OrderTabPriceEditor extends AbstractCellEditor implements TableCellEditor, TableCellRenderer {

    private final JPanel content;
    private final JTextField price;
    private List<JNode> selectedEquipment;
    private JNode node;

    public OrderTabPriceEditor(List<JNode> selectedEquipment, OrderTab orderTab) {
        this.selectedEquipment = selectedEquipment;
        content = new JPanel(new GridLayout());
        price = new JTextField();
        price.setFont(new Font("SansSerif", 0, 14));
        content.add(price);

        price.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                double v = 0;
                try {
                    v = Double.parseDouble(price.getText());
                } catch (Throwable t) {
                    t.printStackTrace();
                    return;
                }
                node.setPrice(price.getText());
                orderTab.updateReport();
            }

            @Override
            public void keyReleased(KeyEvent e) {
                double v = 0;
                try {
                    v = Double.parseDouble(price.getText());
                } catch (Throwable t) {
                    t.printStackTrace();
                    return;
                }
                node.setPrice(price.getText());
                orderTab.updateReport();
            }
        });
    }

    @Override
    public Object getCellEditorValue() {
        return price.getText();
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        return getTableCellEditorComponent(table, value, isSelected, row, column);
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        price.setText(String.valueOf(value));
        //we cant get node in constructor so have to set it during rendering
        node = selectedEquipment.get(row);
        return content;
    }
}
