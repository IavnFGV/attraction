package document;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FirmMiniDialog extends JDialog {
    private final JPanel contentPanel = new JPanel();
    private MyComboboxModel mcb;
    private JList list = new JList();
    private FirmEditDialog firmDialog = new FirmEditDialog();

    public FirmMiniDialog() {
        this.setUndecorated(true);
        this.setBounds(300, 300, 200, 300);
        this.setModal(true);
        this.getContentPane().setLayout(new BorderLayout());
        this.contentPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
        this.contentPanel.setAlignmentY(0.0F);
        this.contentPanel.setAlignmentX(0.0F);
        this.getContentPane().add(this.contentPanel, "Center");
        GridBagLayout gbl_contentPanel = new GridBagLayout();
        gbl_contentPanel.columnWidths = new int[]{100, 1, 0};
        gbl_contentPanel.rowHeights = new int[]{1, 0, 0};
        gbl_contentPanel.columnWeights = new double[]{0.0D, 0.0D, 4.9E-324D};
        gbl_contentPanel.rowWeights = new double[]{1.0D, 1.0D, 4.9E-324D};
        this.contentPanel.setLayout(gbl_contentPanel);
        JScrollPane buttonPane = new JScrollPane();
        buttonPane.setPreferredSize(new Dimension(0, 0));
        GridBagConstraints fl_buttonPane = new GridBagConstraints();
        fl_buttonPane.gridheight = 2;
        fl_buttonPane.gridwidth = 2;
        fl_buttonPane.weighty = 1.0D;
        fl_buttonPane.weightx = 1.0D;
        fl_buttonPane.fill = 1;
        fl_buttonPane.gridx = 0;
        fl_buttonPane.gridy = 0;
        this.contentPanel.add(buttonPane, fl_buttonPane);
        buttonPane.setViewportView(this.list);
        this.list.setLayoutOrientation(2);
        this.list.setSelectionMode(0);
        this.list.setLayoutOrientation(0);
        JPanel buttonPane1 = new JPanel();
        buttonPane1.setBorder(new LineBorder(new Color(0, 0, 0)));
        FlowLayout fl_buttonPane1 = new FlowLayout(0);
        fl_buttonPane1.setVgap(0);
        fl_buttonPane1.setHgap(0);
        buttonPane1.setLayout(fl_buttonPane1);
        this.getContentPane().add(buttonPane1, "South");
        JButton deleteBtn = new JButton("");
        deleteBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Object e2 = FirmMiniDialog.this.list.getSelectedValue();
                    if (e2 != null) {
                        FirmMiniDialog.this.mcb.setSelectedItem(e2);
                    }

                    FirmMiniDialog.this.setVisible(false);
                } catch (RuntimeException var3) {
                    FirmMiniDialog.this.setVisible(false);
                    var3.printStackTrace();
                }

            }
        });
        deleteBtn.setIcon(new ImageIcon("resources/images/ok.png"));
        deleteBtn.setPreferredSize(new Dimension(20, 20));
        deleteBtn.setMinimumSize(new Dimension(20, 20));
        deleteBtn.setMaximumSize(new Dimension(20, 20));
        buttonPane1.add(deleteBtn);
        deleteBtn = new JButton("");
        deleteBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Firm f = FirmMiniDialog.this.firmDialog.createFirm(FirmMiniDialog.this.mcb);
                if (f != null) {
                    if (!FirmMiniDialog.this.firmDialog.isAlreadyExists()) {
                        FirmMiniDialog.this.mcb.addElement(f);
                        int ind = FirmMiniDialog.this.mcb.getSellectedIndex();
                        if (ind != -1) {
                            FirmMiniDialog.this.list.setSelectedIndex(ind);
                        }
                        FirmMiniDialog.this.mcb.updateCB();
                    } else {
                        FirmMiniDialog.this.mcb.setSelectedItem(f);
                        int ind = FirmMiniDialog.this.mcb.getSellectedIndex();
                        if (ind != -1) {
                            FirmMiniDialog.this.list.setSelectedIndex(ind);
                        }
                        FirmMiniDialog.this.setVisible(false);
                    }
                }
            }
        });
        deleteBtn.setIcon(new ImageIcon("resources/images/add.png"));
        deleteBtn.setMaximumSize(new Dimension(20, 20));
        deleteBtn.setMinimumSize(new Dimension(20, 20));
        deleteBtn.setPreferredSize(new Dimension(20, 20));
        buttonPane1.add(deleteBtn);
        deleteBtn = new JButton("");
        deleteBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Object e2 = FirmMiniDialog.this.list.getSelectedValue();
                    if (e2 != null) {
                        FirmMiniDialog.this.firmDialog.editFirm((Firm) e2, FirmMiniDialog.this.mcb);
                        FirmMiniDialog.this.mcb.updateCB();
                        FirmMiniDialog.this.mcb.setSelectedItem(e2);
                        FirmMiniDialog.this.setVisible(false);
                    }
                } catch (RuntimeException var3) {
                    var3.printStackTrace();
                }

            }
        });
        deleteBtn.setIcon(new ImageIcon("resources/images/edit.png"));
        deleteBtn.setPreferredSize(new Dimension(20, 20));
        deleteBtn.setMinimumSize(new Dimension(20, 20));
        deleteBtn.setMaximumSize(new Dimension(20, 20));
        buttonPane1.add(deleteBtn);
        deleteBtn = new JButton("");
        deleteBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Object e2 = FirmMiniDialog.this.list.getSelectedValue();
                    if (e2 != null) {
                        FirmMiniDialog.this.mcb.remove(e2);
                    }
                } catch (RuntimeException var3) {
                    var3.printStackTrace();
                }

            }
        });
        deleteBtn.setIcon(new ImageIcon("resources/images/del.png"));
        deleteBtn.setPreferredSize(new Dimension(20, 20));
        buttonPane1.add(deleteBtn);
    }

    public static void main(String[] args) {
        try {
            FirmMiniDialog e = new FirmMiniDialog();
            e.setDefaultCloseOperation(2);
            e.setVisible(true);
        } catch (Exception var2) {
            var2.printStackTrace();
        }

    }

    public void showDialog(MyComboboxModel mcb) {
        this.mcb = mcb;
        this.list.setModel(mcb);
        this.setVisible(true);
    }
}
