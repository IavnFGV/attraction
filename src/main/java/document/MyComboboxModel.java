package document;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MyComboboxModel extends AbstractListModel<Firm> implements ComboBoxModel<Firm> {
    private String fName;
    private List<Firm> list = new ArrayList();
    private Firm selected = null;
    private List<String> numbers = new ArrayList();
    private boolean sort;
    private String filterString;
    private List<Firm> filteredList;

    public MyComboboxModel(String fName, boolean sort) {
        this.fName = fName;
        this.sort = sort;

        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(fName));
            Object e = in.readObject();
            if (e instanceof List) {
                this.list = (List) e;
            }

            in.close();
            this.generateNumList();
        } catch (ClassNotFoundException | IOException var5) {
            var5.printStackTrace();
        }

    }

    private void generateNumList() {
        ArrayList result = new ArrayList();

        for (int i = 0; i < this.list.size(); ++i) {
            result.add(((Firm) this.list.get(i)).inn);
        }

        this.numbers = result;
    }

    public List<String> getNumbers() {
        return this.numbers;
    }

    public void setNumbers(List<String> numbers) {
        this.numbers = numbers;
    }

    public void remove(Object o) {
        this.list.remove(o);
        this.setSelectedItem((Object) null);
        this.updateCB();
    }

    public void updateCB() {
        if (this.sort) {
            this.sort();
        }

        this.fireContentsChanged(this, 0, this.getSize() - 1);
        this.saveList();
    }

    private void softUpdateCB() {
        if (this.sort) {
            this.sort();
        }

        this.fireContentsChanged(this, 0, this.getSize() - 1);

    }

    public int getSize() {
        return this.getList().size();
    }

    public Firm getElementAt(int arg0) {
        return arg0 >= this.getList().size() ? null : (Firm) this.getList().get(arg0);
    }

    public List<Firm> getList() {
        if ((filterString != null) && !(filterString.isEmpty())) {
            return this.filteredList;
        }
        return list;
    }

    private void sort() {
        Collections.sort(this.list, new Comparator<Firm>() {
            public int compare(Firm o1, Firm o2) {
                String s1 = o1.type + " " + o1.name;
                String s2 = o2.type + " " + o2.name;
                return s1.compareTo(s2);
            }
        });
    }

    private void saveList() {
        this.generateNumList();

        try {
            ObjectOutputStream e = new ObjectOutputStream(new FileOutputStream(this.fName));
            e.writeObject(this.list);
            e.flush();
            e.close();
        } catch (FileNotFoundException var2) {
            var2.printStackTrace();
        } catch (IOException var3) {
            var3.printStackTrace();
        }

    }

    public void addElement(Firm firm) {
        this.list.add(firm);
        this.setFilterString(null);
        if (this.sort) {
            this.sort();
        }
        this.setSelectedItem(firm);
        this.saveList();
    }

    public String getFilterString() {
        return filterString;
    }

    public void setFilterString(String filterString) {
        String oldFilter = this.filterString;
        this.filterString = filterString;
        if ((this.filterString != null) && (!this.filterString.equals(oldFilter))) {
            this.filteredList = list.stream().filter(firm -> firm.toString().toLowerCase().contains
                    (filterString.toLowerCase())).collect(Collectors.toList());
        }
        softUpdateCB();
    }

    public int getSellectedIndex() {
        return this.list.indexOf(this.selected);
    }

    public Object getSelectedItem() {
        return this.selected;
    }

    public void setSelectedItem(Object arg0) {
        this.selected = (Firm) arg0;
        this.fireContentsChanged(this, 0, this.getSize() - 1);
    }


}