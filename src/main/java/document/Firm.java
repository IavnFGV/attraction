package document;


import java.io.Serializable;

/**
 * @author Dmitry Tumash
 */
public class Firm implements Serializable {
    String name = "";
    String type = "";
    String address = "";
    String inn = "";
    String kpp = "";
    String rasch_schet = "";
    String korr_schet = "";
    String bank = "";
    String bik = "";
    String organ_upravl = "";
    String v_litze = "";
    String na_osnovanii = "";
    String litzo = "";

    public Firm() {
    }

    public String toString() {
        return this.type + " " + this.name;
    }
}
