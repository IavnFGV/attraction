package document;


import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Dmitry Tumash
 */
public class ArchiveDialog extends JDialog {
  private final JPanel contentPanel = new JPanel();
  private JTable table;
  private DocumentState curDS = null;

  public static void main(String[] args) {
    try {
      ArchiveDialog e = new ArchiveDialog();
      e.setDefaultCloseOperation(2);
      e.setVisible(true);
    } catch (Exception var2) {
      var2.printStackTrace();
    }

  }

  public void setModel(TableModel tm) {
    /*final DocStateTableModel docStateTableModel = (DocStateTableModel) tm;

    final java.util.List<DocumentState> list = docStateTableModel.getList();
    Collections.sort(list, new Comparator<DocumentState>() {
      @Override
      public int compare(DocumentState o1, DocumentState o2) {
        return o1.getFromDate().compareTo(o2.getFromDate());
      }
    });

    docStateTableModel.setList(list);*/

    this.table.setModel(tm);
    this.table.getColumnModel().getColumn(0).setMaxWidth(25);
    this.table.getColumnModel().getColumn(1).setMaxWidth(85);
    this.table.getColumnModel().getColumn(3).setMaxWidth(85);
    this.table.getColumnModel().getColumn(4).setMaxWidth(85);
    this.table.getColumnModel().getColumn(5).setMaxWidth(125);
  }

  public DocumentState getDocState() {
    this.setVisible(true);
    return this.curDS;
  }

  public ArchiveDialog() {
    this.setModal(true);
    this.setBounds(100, 100, 564, 553);
    this.getContentPane().setLayout(new BorderLayout());
    this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.getContentPane().add(this.contentPanel, "Center");
    GridBagLayout gbl_contentPanel = new GridBagLayout();
    gbl_contentPanel.columnWidths = new int[]{48, 452, 0};
    gbl_contentPanel.rowHeights = new int[]{402, 0};
    gbl_contentPanel.columnWeights = new double[]{0.0D, 1.0D, 4.9E-324D};
    gbl_contentPanel.rowWeights = new double[]{1.0D, 4.9E-324D};
    this.contentPanel.setLayout(gbl_contentPanel);
    JScrollPane buttonPane = new JScrollPane();
    GridBagConstraints cancelButton = new GridBagConstraints();
    cancelButton.gridwidth = 2;
    cancelButton.fill = 1;
    cancelButton.anchor = 18;
    cancelButton.gridx = 0;
    cancelButton.gridy = 0;
    this.contentPanel.add(buttonPane, cancelButton);
    this.table = new JTable();
    this.table.setMaximumSize(new Dimension(32222, 32222));
    buttonPane.setViewportView(this.table);
    JPanel buttonPane1 = new JPanel();
    buttonPane1.setLayout(new FlowLayout(2));
    this.getContentPane().add(buttonPane1, "South");
    JButton cancelButton1 = new JButton("OK");
    cancelButton1.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        int row = ArchiveDialog.this.table.getSelectedRow();
        if (row != -1) {
          row = ArchiveDialog.this.table.convertRowIndexToModel(row);
          ArchiveDialog.this.curDS = ((DocStateTableModel) ArchiveDialog.this.table.getModel()).getDocState(row);
          ArchiveDialog.this.setVisible(false);
        }

      }
    });
    cancelButton1.setActionCommand("OK");
    buttonPane1.add(cancelButton1);
    this.getRootPane().setDefaultButton(cancelButton1);
    cancelButton1 = new JButton("Отмена");
    cancelButton1.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        ArchiveDialog.this.curDS = null;
        ArchiveDialog.this.setVisible(false);
      }
    });
    JButton button = new JButton("Удалить");
    button.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        int row = ArchiveDialog.this.table.getSelectedRow();
        if (row != -1) {
          row = ArchiveDialog.this.table.convertRowIndexToModel(row);
          DocStateTableModel tm = (DocStateTableModel) ArchiveDialog.this.table.getModel();
          tm.remove(row);
          ArchiveDialog.this.curDS = null;
        }

      }
    });
    buttonPane1.add(button);
    cancelButton1.setActionCommand("Cancel");
    buttonPane1.add(cancelButton1);
  }
}
