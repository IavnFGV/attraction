package document;

/**
 * @author Dmitry Tumash
 */

import javax.swing.table.AbstractTableModel;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ServiceTableModel extends AbstractTableModel implements Serializable {
    private List<ServiceTableModel.R> list = new ArrayList();

    public ServiceTableModel() {
    }

    public void clearList() {
        this.list = new ArrayList();
        this.fireTableDataChanged();
    }

    public int getRowCount() {
        return this.list.size();
    }

    public int getColumnCount() {
        return 4;
    }

    public Object getValueAt(int r, int c) {
        ServiceTableModel.R rr = (ServiceTableModel.R) this.list.get(r);
        switch (c) {
            case 0:
                return rr.name;
            case 1:
                return rr.count;
            case 2:
                return rr.price;
            case 3:
                return rr.price.multiply(rr.count);
            default:
                return null;
        }
    }

    public String getColumnName(int c) {
        switch (c) {
            case 0:
                return "Наименование";
            case 1:
                return "Количество";
            case 2:
                return "Цена";
            case 3:
                return "Сумма";
            default:
                return null;
        }
    }

    public Class<?> getColumnClass(int c) {
        switch (c) {
            case 0:
                return String.class;
            case 1:
                return BigDecimal.class;
            case 2:
                return BigDecimal.class;
            case 3:
                return BigDecimal.class;
            default:
                return null;
        }
    }

    public boolean isCellEditable(int r, int c) {
        return c != 3;
    }

    public void setValueAt(Object val, int r, int c) {
        ServiceTableModel.R rr = (ServiceTableModel.R) this.list.get(r);
        switch (c) {
            case 0:
                System.out.println(val);
                rr.name = (ServiceName) val;
                break;
            case 1:
                rr.count = (BigDecimal) val;
                break;
            case 2:
                rr.price = (BigDecimal) val;
        }

        this.fireTableDataChanged();
    }

    public void addR() {
        list.add(new ServiceTableModel.R());
        this.fireTableDataChanged();
    }

    public void addR(ServiceName serviceName) {
        final R e = new R();
        e.name = serviceName;
        list.add(e);
        this.fireTableDataChanged();
    }

    public void remove(int i) {
        this.list.remove(i);
        this.fireTableDataChanged();
    }

    public BigDecimal getTotal() {
        BigDecimal total = BigDecimal.ZERO;

        ServiceTableModel.R el;
        for (Iterator var3 = this.list.iterator(); var3.hasNext(); total = total.add(el.price.multiply(el.count))) {
            el = (ServiceTableModel.R) var3.next();
        }

        return total;
    }

    public List<ServiceTableModel.R> getList() {
        return this.list;
    }

    public void setList(List<ServiceTableModel.R> rr) {
        this.list = rr;
        this.fireTableDataChanged();
    }

    public static class R implements Serializable {

        private static final long serialVersionUID = 4200398492072786110L;
        ServiceName name = new ServiceName();
        BigDecimal count;
        BigDecimal price;
        BigDecimal sum;

        public R() {
            this.count = BigDecimal.ONE;
            this.price = BigDecimal.ZERO;
        }

        public R(R r) {
            this.name.serviceName = r.name.serviceName;
            this.name.serviceNameRodPad = r.name.serviceNameRodPad;
            this.count = r.count;
            this.price = r.price;
            if (r.sum != null) {
                this.sum = r.sum;
            }
        }

    }
}
