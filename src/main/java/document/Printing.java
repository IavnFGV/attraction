package document;

import org.docx4j.XmlUtils;
import org.docx4j.convert.out.pdf.viaXSLFO.Conversion;
import org.docx4j.convert.out.pdf.viaXSLFO.PdfSettings;
import org.docx4j.jaxb.Context;
import org.docx4j.model.datastorage.migration.VariablePrepare;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.io.SaveToZipFile;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.*;
import org.docx4j.wml.R.Tab;
import tools.RussianMoney;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

/**
 * @author Dmitry Tumash
 */
public class Printing {
    private static final String PDF_TEMPLATE = "resources/templates/pdf_template.docx";
    private static final String PDF_DOGOVOR_TEMPLATE = "resources/templates/pdf_dogovor_template.docx";
    private static final String SCHET_TEMPLATE = "resources/templates/schet_template.docx";
  private static final String DOGOVOR_TEMPLATE = "resources/templates/dogovor_template.docx";
    static SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
  private static BigDecimal NDS = new BigDecimal("1.18");
    private static BigDecimal NDS018 = new BigDecimal("0.18");
    private static HashMap<String, String> var_mapping = new LinkedHashMap();

    public Printing() {
    }

    private static void prepareMapping(DocumentState ds) {
        var_mapping.clear();
        var_mapping.put("sellertypef", ds.seller.type);
        var_mapping.put("sellernamef", ds.seller.name);
        var_mapping.put("selleraddress", ds.seller.address);
        var_mapping.put("sellerinn", ds.seller.inn);
        var_mapping.put("sellerkpp", ds.seller.kpp);
        var_mapping.put("sellerrschet", ds.seller.rasch_schet);
        var_mapping.put("sellerkschet", ds.seller.korr_schet);
        var_mapping.put("sellerbank", ds.seller.bank);
        var_mapping.put("sellerbik", ds.seller.bik);
        var_mapping.put("sellerorganupravl", ds.seller.organ_upravl);
        var_mapping.put("sellervlitze", ds.seller.v_litze);
        var_mapping.put("sellernaosnovanii", ds.seller.na_osnovanii);
        var_mapping.put("sellerlitzo", ds.seller.litzo);
        var_mapping.put("typef", ds.buyer.type);
        var_mapping.put("namef", ds.buyer.name);
        var_mapping.put("address", ds.buyer.address);
        var_mapping.put("inn", ds.buyer.inn);
        var_mapping.put("kpp", ds.buyer.kpp);
        var_mapping.put("rschet", ds.buyer.rasch_schet);
        var_mapping.put("kschet", ds.buyer.korr_schet);
        var_mapping.put("bank", ds.buyer.bank);
        var_mapping.put("bik", ds.buyer.bik);
        var_mapping.put("organupravl", ds.buyer.organ_upravl);
        var_mapping.put("vlitze", ds.buyer.v_litze);
        var_mapping.put("naosnovanii", ds.buyer.na_osnovanii);
        var_mapping.put("litzo", ds.buyer.litzo);
        var_mapping.put("naimenovaniya", ds.getServiceNameInRow());
        var_mapping.put("total", RussianMoney.totalToStrWithNds(ds.total));
        var_mapping.put("schet", ds.number.toString());
        var_mapping.put("fromDate", ds.fromDate == null?"":sdf.format(ds.fromDate.getTime()));
        var_mapping.put("mesto", ds.actionPlace);
        var_mapping.put("data", ds.actionDate == null?"":sdf.format(ds.actionDate.getTime()));
        var_mapping.put("starttime", ds.startTime);
        var_mapping.put("endtime", ds.endTime);
        var_mapping.put("equipmentlist", ds.getEquipment());
    }

    private static void replacePlaceholder(WordprocessingMLPackage template, String name, String placeholder) {
        List texts = getAllElementFromObject(template.getMainDocumentPart(), Text.class);
        ObjectFactory factory = Context.getWmlObjectFactory();
        Br br = factory.createBr();
        Tab tab = factory.createRTab();
        Iterator var8 = texts.iterator();

        label43:
        while(var8.hasNext()) {
            Object text = var8.next();
            Text textElement = (Text)text;
            if(textElement.getValue() != null && textElement.getValue().contains(placeholder)) {
                if(placeholder.equals("equipmentlist")) {
                    List rList = getAllElementFromObject(template.getMainDocumentPart(), R.class);
                    R eqRun = null;
                    Iterator var13 = rList.iterator();

                    label37:
                    while(true) {
                        if(var13.hasNext()) {
                            Object str = var13.next();
                            List ttt = getAllElementFromObject(str, Text.class);
                            Iterator tel = ttt.iterator();

                            Text tt;
                            do {
                                if(!tel.hasNext()) {
                                    continue label37;
                                }

                                Object t = tel.next();
                                tt = (Text)t;
                            } while(!"equipmentlist".equals(tt.getValue()));

                            eqRun = (R)str;
                        }

                        eqRun.getContent().remove(textElement.getParent());
                        String[] var21;
                        int var20 = (var21 = name.split("\n")).length;
                        int var19 = 0;

                        while(true) {
                            if(var19 >= var20) {
                                continue label43;
                            }

                            String var18 = var21[var19];
                            Text var22 = factory.createText();
                            var22.setValue(var18);
                            eqRun.getContent().add(tab);
                            eqRun.getContent().add(var22);
                            eqRun.getContent().add(br);
                            ++var19;
                        }
                    }
                } else {
                    textElement.setValue(textElement.getValue().replaceAll(placeholder, name));
                }
            }
        }

    }

    public static void saveDOCX(DocumentState ds, String path) {
        path = path + "/" + ds.number + " - " + ds.buyer.name;
        (new File(path)).mkdir();
        saveSchet(ds, path + "/" + ds.number + " от " + sdf.format(ds.getFromDate()) + "- Счет -" + ds.buyer.name + ".docx");
        saveAct(ds, path + "/" + ds.number + " от " + sdf.format(ds.getActionDate()) + "- Акт -" + ds.buyer.name + ".docx");
        saveDogovor(ds, path + "/" + ds.number + " от " +  sdf.format(ds.getFromDate()) + "- Договор -" + ds.buyer.name + ".docx");
        saveSchetFactura(ds, path + "/" + ds.number + " от " +  sdf.format(ds.getActionDate()) + "- Счет-фактура -" + ds.buyer.name + ".docx");
    }

    private static void saveSchetFactura(DocumentState ds, String path) {
        WordprocessingMLPackage wdp = work(ds, "resources/templates/schetfactura_template.docx");
        SaveToZipFile saver = new SaveToZipFile(wdp);

        try {
            saver.save(path);
        } catch (Docx4JException var5) {
            var5.printStackTrace();
        }

    }

    public static void printDOCX(DocumentState ds) {
        String path = "resources/temp";
        saveSchet(ds, path + "/Schet.docx");
        saveAct(ds, path + "/Act.docx");
        saveDogovor(ds, path + "/Dogovor.docx");
        saveSchetFactura(ds, path + "/SchetFactura.docx");

        try {
            Desktop.getDesktop().print(new File(path + "/Schet.docx"));
            Desktop.getDesktop().print(new File(path + "/Dogovor.docx"));
            Desktop.getDesktop().print(new File(path + "/Dogovor.docx"));
            Desktop.getDesktop().print(new File(path + "/SchetFactura.docx"));
            Desktop.getDesktop().print(new File(path + "/Act.docx"));
            Desktop.getDesktop().print(new File(path + "/Act.docx"));
        } catch (IOException var3) {
            var3.printStackTrace();
        }

    }

    private static void saveSchet(DocumentState ds, String path) {
        WordprocessingMLPackage wdp = work(ds, "resources/templates/schet_template.docx");
        SaveToZipFile saver = new SaveToZipFile(wdp);

        try {
            saver.save(path);
        } catch (Docx4JException var5) {
            var5.printStackTrace();
        }

    }

    private static void saveAct(DocumentState ds, String path) {
        WordprocessingMLPackage wdp = work(ds, "resources/templates/act_template.docx");
        SaveToZipFile saver = new SaveToZipFile(wdp);

        try {
            saver.save(path);
        } catch (Docx4JException var5) {
            var5.printStackTrace();
        }

    }

    private static void saveDogovor(DocumentState ds, String path) {
        WordprocessingMLPackage wdp = work(ds, "resources/templates/dogovor_template.docx");
        SaveToZipFile saver = new SaveToZipFile(wdp);

        try {
            saver.save(path);
        } catch (Docx4JException var5) {
            var5.printStackTrace();
        }

    }

    public static void printPdf(DocumentState documentState) {
        String tempPath = "resources/temp/Schet.pdf";
        Conversion c = new Conversion(work(documentState, "resources/templates/pdf_template.docx"));

        try {
            c.output(new FileOutputStream(tempPath), new PdfSettings());
        } catch (Docx4JException | FileNotFoundException var5) {
            var5.printStackTrace();
        }

        try {
            Desktop.getDesktop().print(new File(tempPath));
        } catch (IOException var4) {
            var4.printStackTrace();
        }

    }

    private static WordprocessingMLPackage work(DocumentState ds, String templ) {
        try {
            WordprocessingMLPackage e = WordprocessingMLPackage.load(new File(templ));

            try {
                VariablePrepare.prepare(e);
                prepareMapping(ds);
                MainDocumentPart e1 = e.getMainDocumentPart();
                List tables = getAllElementFromObject(e1, Tbl.class);
                Tbl tempTable = getTemplateTable(tables, "service_name");
                List rows = getAllElementFromObject(tempTable, Tr.class);
                if(rows.size() >= 3) {
                    Tr k = (Tr)rows.get(1);
                    Tr totalRow = (Tr)rows.get(2);
                    List servlst = ds.servicesTM;
                    BigDecimal totalSum = BigDecimal.ZERO;
                    BigDecimal totalNds = BigDecimal.ZERO;
                    BigDecimal totalSumbnds = BigDecimal.ZERO;
                    Iterator object = servlst.iterator();

                    while(object.hasNext()) {
                        document.ServiceTableModel.R textElements = (document.ServiceTableModel.R)object.next();
                        BigDecimal price = ds.nds?textElements.price.multiply(NDS):textElements.price;
                        BigDecimal text = ds.nds?textElements.count.multiply(textElements.price).multiply(NDS):textElements.count.multiply(textElements.price);
                        BigDecimal sumbnds = ds.nds?textElements.price.multiply(textElements.count):textElements
                                .price.multiply(textElements.count).divide(NDS, MathContext.DECIMAL64);
                        BigDecimal nds = text.subtract(sumbnds);
                        totalSum = totalSum.add(text);
                        totalNds = totalNds.add(nds);
                        totalSumbnds = totalSumbnds.add(sumbnds);
                        Tr workingRow = (Tr)XmlUtils.deepCopy(k);
                        List textElements1 = getAllElementFromObject(workingRow, Text.class);
                        Iterator var22 = textElements1.iterator();

                        while(var22.hasNext()) {
                            Object object1 = var22.next();
                            Text text1 = (Text)object1;
                            String var24;
                            switch((var24 = text1.getValue()).hashCode()) {
                                case -1857956186:
                                    if(var24.equals("sumbnds")) {
                                        text1.setValue(String.format("%1$.2f", new Object[]{sumbnds}));
                                    }
                                    break;
                                case -849875833:
                                    if(var24.equals("totalsum")) {
                                        text1.setValue(String.format("%1$.2f", new Object[]{totalSum}));
                                    }
                                    break;
                                case -576860385:
                                    if(var24.equals("totalbnds")) {
                                        text1.setValue(totalSumbnds.toString()/*String.format("%1$.2f", new Object[]{totalSumbnds})*/);
                                    }
                                    break;
                                case 108925:
                                    if(var24.equals("nds")) {
                                        text1.setValue(String.format("%1$.2f", new Object[]{nds}));
                                    }
                                    break;
                                case 114251:
                                    if(var24.equals("sum")) {
                                        text1.setValue(String.format("%1$.2f", new Object[]{text}));
                                    }
                                    break;
                                case 3385935:
                                    if(var24.equals("nnds")) {
                                        text1.setValue(String.format("%1$.2f", new Object[]{totalNds}));
                                    }
                                    break;
                                case 94851343:
                                    if(var24.equals("count")) {
                                        text1.setValue(textElements.count.toString());
                                    }
                                    break;
                                case 106934601:
                                    if(var24.equals("price")) {
                                        text1.setValue(String.format("%1$.2f", new Object[]{price}));
                                    }
                                    break;
                                case 359880149:
                                    if(var24.equals("service_name")) {
                                        text1.setValue(textElements.name.serviceName);
                                    }
                            }
                        }

                        tempTable.getContent().add(tempTable.getContent().size() - 2, workingRow);
                    }

                    List textElements2 = getAllElementFromObject(totalRow, Text.class);
                    Iterator price1 = textElements2.iterator();

                    while(price1.hasNext()) {
                        Object object2 = price1.next();
                        Text text2 = (Text)object2;
                        String sumbnds1;
                        switch((sumbnds1 = text2.getValue()).hashCode()) {
                            case -849875833:
                                if(sumbnds1.equals("totalsum")) {
                                    text2.setValue(String.format("%1$.2f", new Object[]{totalSum}));
                                }
                                break;
                            case -576860385:
                                if(sumbnds1.equals("totalbnds")) {
                                    text2.setValue(String.format("%1$.2f", new Object[]{totalSumbnds}));
                                }
                                break;
                            case 3385935:
                                if(sumbnds1.equals("nnds")) {
                                    text2.setValue(String.format("%1$.2f", new Object[]{totalNds}));
                                }
                        }
                    }

                    tempTable.getContent().remove(k);
                }

                Iterator totalRow1 = var_mapping.keySet().iterator();

                while(totalRow1.hasNext()) {
                    String k1 = (String)totalRow1.next();
                    replacePlaceholder(e, (String)var_mapping.get(k1), k1);
                }

                return e;
            } catch (Exception var25) {
                var25.printStackTrace();
            }
        } catch (Docx4JException var26) {
            var26.printStackTrace();
        }

        return null;
    }

    public static void savePdf(DocumentState documentState, String path) {
        path = path + "/" + documentState.number + " - " + documentState.buyer.name;
        (new File(path)).mkdir();
        Conversion c = new Conversion(work(documentState, "resources/templates/pdf_template.docx"));
        Conversion c1 = new Conversion(work(documentState, "resources/templates/pdf_schetfactura_template.docx"));

        try {
            c.output(new FileOutputStream(path + "/" + documentState.number + " от " + sdf.format(documentState.getFromDate()) + "- Счет -" + documentState.buyer.name + ".pdf"), new PdfSettings());
            c1.output(new FileOutputStream(path + "/" + documentState.number + " от " + sdf.format(documentState.getActionDate()) + "- Счет-Фактура -" + documentState.buyer.name + ".pdf"), new PdfSettings());
        } catch (Docx4JException | FileNotFoundException var4) {
            var4.printStackTrace();
        }

    }

    public static void savePdfDogovor(DocumentState documentState, String path) {
        path = path + "/" + documentState.number + " - " + documentState.buyer.name;
        (new File(path)).mkdir();
        Conversion c = new Conversion(work(documentState, "resources/templates/pdf_dogovor_template.docx"));
        Conversion c1 = new Conversion(work(documentState, "resources/templates/pdf_act_template.docx"));

        try {
            c.output(new FileOutputStream(path + "/" + documentState.number + " от " + sdf.format(documentState.getFromDate()) + "- Договор -" + documentState.buyer.name + ".pdf"), new PdfSettings());
            c1.output(new FileOutputStream(path + "/" + documentState.number + " от " + sdf.format(documentState.getActionDate()) + "- Акт -" + documentState.buyer.name + ".pdf"), new PdfSettings());
        } catch (Docx4JException | FileNotFoundException var4) {
            var4.printStackTrace();
        }

    }

    private static List<Object> getAllElementFromObject(Object obj, Class<?> toSearch) {
        ArrayList result = new ArrayList();
        if(obj instanceof JAXBElement) {
            obj = ((JAXBElement)obj).getValue();
        }

        if(obj != null && obj.getClass().equals(toSearch)) {
            result.add(obj);
        } else if(obj instanceof ContentAccessor) {
            List children = ((ContentAccessor)obj).getContent();
            Iterator var5 = children.iterator();

            while(var5.hasNext()) {
                Object child = var5.next();
                result.addAll(getAllElementFromObject(child, toSearch));
            }
        }

        return result;
    }

    private static Tbl getTemplateTable(List<Object> tables, String templateKey) throws Docx4JException, JAXBException {
        Iterator iterator = tables.iterator();

        while(iterator.hasNext()) {
            Object tbl = iterator.next();
            List textElements = getAllElementFromObject(tbl, Text.class);
            Iterator var6 = textElements.iterator();

            while(var6.hasNext()) {
                Object text = var6.next();
                Text textElement = (Text)text;
                if(textElement.getValue() != null && textElement.getValue().equals(templateKey)) {
                    return (Tbl)tbl;
                }
            }
        }

        return null;
    }
}

