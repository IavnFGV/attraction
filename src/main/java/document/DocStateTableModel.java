package document;

/**
 * @author Dmitry Tumash
 */

import javax.swing.table.AbstractTableModel;
import java.io.*;
import java.util.*;

public class DocStateTableModel extends AbstractTableModel implements Serializable {
  private List<DocumentState> list = new ArrayList();
  private static final String ARCHIVE_FILE = "resources/document/archive.dat";

  public List<DocumentState> getList() {
    return list;
  }

  public void setList(List<DocumentState> list) {
    this.list = list;
  }

  public Integer nextNum(Integer n) {
    int i;
    for (i = n.intValue() + 1; this.containsNumber(i); ++i) {
      ;
    }

    return Integer.valueOf(i);
  }

  private boolean containsNumber(int i) {
    Iterator var3 = this.list.iterator();

    while (var3.hasNext()) {
      DocumentState ds = (DocumentState) var3.next();
      if (ds.number.intValue() == i) {
        return true;
      }
    }

    return false;
  }

  public DocStateTableModel() {
    this.loadArchive();
  }

  public int getIndexForNum(int num) {
    for (int i = 0; i < this.list.size(); ++i) {
      if (((DocumentState) this.list.get(i)).number.intValue() == num) {
        return i;
      }
    }

    return -1;
  }

  public void replaceDocState(DocumentState ds) {
    int ind = this.getIndexForNum(ds.number.intValue());
    if (ind != -1) {
      this.list.set(ind, ds);
    }

    this.saveArchive();
    this.fireTableDataChanged();
  }

  public void addDocState(DocumentState ds) {
    this.list.add(ds);
    this.saveArchive();
    this.fireTableDataChanged();
  }

  public void remove(int row) {
    this.list.remove(row);
    this.fireTableDataChanged();
    this.saveArchive();
  }

  public DocumentState getDocState(int row) {
    return (DocumentState) this.list.get(row);
  }

  private void loadArchive() {
    try {
      ObjectInputStream e = new ObjectInputStream(new FileInputStream("resources/document/archive.dat"));
      this.list = (List) e.readObject();
      Collections.sort(list, new Comparator<DocumentState>() {
        @Override
        public int compare(DocumentState o1, DocumentState o2) {
          return o2.getFromDate().compareTo(o1.getFromDate());
        }
      });

      e.close();
    } catch (FileNotFoundException var2) {
      var2.printStackTrace();
    } catch (IOException var3) {
      var3.printStackTrace();
    } catch (ClassNotFoundException var4) {
      var4.printStackTrace();
    }

  }

  private void saveArchive() {
    try {
      ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("resources/document/archive.dat"));
      out.writeObject(this.list);
      out.flush();
      out.close();
    } catch (FileNotFoundException var3) {
      var3.printStackTrace();
    } catch (IOException var4) {
      var4.printStackTrace();
    }

  }

  public void setValueAt(Object o, int row, int col) {
    switch (col) {
      case 4:
        ((DocumentState) this.list.get(row)).payed = ((Boolean) o).booleanValue();
        this.saveArchive();
        break;
      case 5:
        ((DocumentState) this.list.get(row)).willNotBePayed = ((Boolean) o).booleanValue();
        this.saveArchive();
    }

  }

  public String getColumnName(int col) {
    switch (col) {
      case 0:
        return "№";
      case 1:
        return "Дата";
      case 2:
        return "Партнер";
      case 3:
        return "Сумма";
      case 4:
        return "Оплачен";
      case 5:
        return "Не будет оплачен";
      default:
        return null;
    }
  }

  public boolean isCellEditable(int row, int col) {
    return col == 4 || col == 5;
  }

  public int getRowCount() {
    return this.list.size();
  }

  public int getColumnCount() {
    return 6;
  }

  public Class<?> getColumnClass(int col) {
    switch (col) {
      case 0:
        return Integer.class;
      case 1:
        return String.class;
      case 2:
        return String.class;
      case 3:
        return String.class;
      case 4:
        return Boolean.class;
      case 5:
        return Boolean.class;
      default:
        return null;
    }
  }

  public Object getValueAt(int rowIndex, int columnIndex) {
    DocumentState row = (DocumentState) this.list.get(rowIndex);
    switch (columnIndex) {
      case 0:
        return row.number.toString();
      case 1:
        return Printing.sdf.format(row.fromDate.getTime());
      case 2:
        return row.buyer.type + " " + row.buyer.name;
      case 3:
        return row.total.toString();
      case 4:
        return Boolean.valueOf(row.payed);
      case 5:
        return Boolean.valueOf(row.willNotBePayed);
      default:
        return null;
    }
  }
}

