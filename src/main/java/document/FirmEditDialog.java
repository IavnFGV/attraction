package document;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class FirmEditDialog extends JDialog {
    private final JPanel contentPanel = new JPanel();
    private JTextField name;
    private JTextField type;
    private JTextField address;
    private JTextField inn;
    private JTextField kpp;
    private JTextField schet;
    private JTextField korr_schet;
    private JTextField bank;
    private JTextField bik;
    private JTextField organ;
    private JTextField na_osnov;
    private JTextField v_litze;
    private JTextField litzo;
    private MyComboboxModel mcb;
    private Firm result = new Firm();
    private boolean create = false;
    private boolean alreadyExists;
    private ActionListener al = new ActionListener() {
        public void actionPerformed(ActionEvent arg0) {
            if("OK".equals(arg0.getActionCommand())) {
                FirmEditDialog.this.result.name = FirmEditDialog.this.name.getText();
                FirmEditDialog.this.result.type = FirmEditDialog.this.type.getText();
                FirmEditDialog.this.result.address = FirmEditDialog.this.address.getText();
                FirmEditDialog.this.result.inn = FirmEditDialog.this.inn.getText().trim();
                FirmEditDialog.this.result.kpp = FirmEditDialog.this.kpp.getText();
                FirmEditDialog.this.result.rasch_schet = FirmEditDialog.this.schet.getText();
                FirmEditDialog.this.result.korr_schet = FirmEditDialog.this.korr_schet.getText();
                FirmEditDialog.this.result.bank = FirmEditDialog.this.bank.getText();
                FirmEditDialog.this.result.bik = FirmEditDialog.this.bik.getText();
                FirmEditDialog.this.result.organ_upravl = FirmEditDialog.this.organ.getText();
                FirmEditDialog.this.result.na_osnovanii = FirmEditDialog.this.na_osnov.getText();
                FirmEditDialog.this.result.v_litze = FirmEditDialog.this.v_litze.getText();
                FirmEditDialog.this.result.litzo = FirmEditDialog.this.litzo.getText();
                if(!FirmEditDialog.this.create && !FirmEditDialog.this.isAlreadyExists()) {
                    FirmEditDialog.this.result = null;
                }

                FirmEditDialog.this.setVisible(false);
            } else {
                FirmEditDialog.this.result = null;
                FirmEditDialog.this.setVisible(false);
            }

        }
    };


    public FirmEditDialog() {
        this.setModal(true);
        this.setTitle("Фирма");
        this.setBounds(100, 100, 360, 474);
        this.getContentPane().setLayout(new BorderLayout());
        this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.getContentPane().add(this.contentPanel, "Center");
        GridBagLayout gbl_contentPanel = new GridBagLayout();
        gbl_contentPanel.columnWidths = new int[3];
        gbl_contentPanel.rowHeights = new int[15];
        gbl_contentPanel.columnWeights = new double[]{0.0D, 1.0D, 4.9E-324D};
        gbl_contentPanel.rowWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 4.9E-324D};
        this.contentPanel.setLayout(gbl_contentPanel);
        JLabel buttonPane = new JLabel("Название");
        GridBagConstraints cancelButton = new GridBagConstraints();
        cancelButton.insets = new Insets(0, 0, 5, 5);
        cancelButton.anchor = 13;
        cancelButton.gridx = 0;
        cancelButton.gridy = 0;
        this.contentPanel.add(buttonPane, cancelButton);
        this.name = new JTextField();
        GridBagConstraints buttonPane1 = new GridBagConstraints();
        buttonPane1.insets = new Insets(0, 0, 5, 0);
        buttonPane1.fill = 2;
        buttonPane1.gridx = 1;
        buttonPane1.gridy = 0;
        this.contentPanel.add(this.name, buttonPane1);
        this.name.setColumns(10);
        buttonPane = new JLabel("Вид юр. лица");
        cancelButton = new GridBagConstraints();
        cancelButton.anchor = 13;
        cancelButton.insets = new Insets(0, 0, 5, 5);
        cancelButton.gridx = 0;
        cancelButton.gridy = 1;
        this.contentPanel.add(buttonPane, cancelButton);
        this.type = new JTextField();
        buttonPane1 = new GridBagConstraints();
        buttonPane1.insets = new Insets(0, 0, 5, 0);
        buttonPane1.fill = 2;
        buttonPane1.gridx = 1;
        buttonPane1.gridy = 1;
        this.contentPanel.add(this.type, buttonPane1);
        this.type.setColumns(10);
        buttonPane = new JLabel("Адресс и телефон");
        cancelButton = new GridBagConstraints();
        cancelButton.anchor = 13;
        cancelButton.insets = new Insets(0, 0, 5, 5);
        cancelButton.gridx = 0;
        cancelButton.gridy = 2;
        this.contentPanel.add(buttonPane, cancelButton);
        this.address = new JTextField();
        buttonPane1 = new GridBagConstraints();
        buttonPane1.insets = new Insets(0, 0, 5, 0);
        buttonPane1.fill = 2;
        buttonPane1.gridx = 1;
        buttonPane1.gridy = 2;
        this.contentPanel.add(this.address, buttonPane1);
        this.address.setColumns(10);
        buttonPane = new JLabel("ИНН");
        cancelButton = new GridBagConstraints();
        cancelButton.anchor = 13;
        cancelButton.insets = new Insets(0, 0, 5, 5);
        cancelButton.gridx = 0;
        cancelButton.gridy = 3;
        this.contentPanel.add(buttonPane, cancelButton);
        this.inn = new JTextField();
        buttonPane1 = new GridBagConstraints();
        buttonPane1.insets = new Insets(0, 0, 5, 0);
        buttonPane1.fill = 2;
        buttonPane1.gridx = 1;
        buttonPane1.gridy = 3;
        this.contentPanel.add(this.inn, buttonPane1);
        this.inn.setColumns(10);
        this.inn.addCaretListener(new CaretListener() {
            public void caretUpdate(CaretEvent e) {
                if(FirmEditDialog.this.create) {
                    String strInn = FirmEditDialog.this.inn.getText();
                    List lst = FirmEditDialog.this.mcb.getNumbers();
                    if(!"".equals(strInn) && lst.contains(strInn)) {
                        int ans = JOptionPane.showConfirmDialog(FirmEditDialog.this, "Фирма с таким ИНН уже есть, загрузить ее?", "Фирма уже существует", 0);
                        if(ans == 0) {
                            int index = lst.indexOf(strInn);
                            Firm f = FirmEditDialog.this.mcb.getElementAt(index);
                            FirmEditDialog.this.create = false;
                            FirmEditDialog.this.alreadyExists = true;
                            FirmEditDialog.this.editFirm(f, FirmEditDialog.this.mcb);
                        }
                    }
                }

            }
        });
        buttonPane = new JLabel("КПП");
        cancelButton = new GridBagConstraints();
        cancelButton.anchor = 13;
        cancelButton.insets = new Insets(0, 0, 5, 5);
        cancelButton.gridx = 0;
        cancelButton.gridy = 4;
        this.contentPanel.add(buttonPane, cancelButton);
        this.kpp = new JTextField();
        buttonPane1 = new GridBagConstraints();
        buttonPane1.insets = new Insets(0, 0, 5, 0);
        buttonPane1.fill = 2;
        buttonPane1.gridx = 1;
        buttonPane1.gridy = 4;
        this.contentPanel.add(this.kpp, buttonPane1);
        this.kpp.setColumns(10);
        buttonPane = new JLabel("Расчетный счет");
        cancelButton = new GridBagConstraints();
        cancelButton.anchor = 13;
        cancelButton.insets = new Insets(0, 0, 5, 5);
        cancelButton.gridx = 0;
        cancelButton.gridy = 5;
        this.contentPanel.add(buttonPane, cancelButton);
        this.schet = new JTextField();
        buttonPane1 = new GridBagConstraints();
        buttonPane1.insets = new Insets(0, 0, 5, 0);
        buttonPane1.fill = 2;
        buttonPane1.gridx = 1;
        buttonPane1.gridy = 5;
        this.contentPanel.add(this.schet, buttonPane1);
        this.schet.setColumns(10);
        buttonPane = new JLabel("Кор. счет");
        cancelButton = new GridBagConstraints();
        cancelButton.anchor = 13;
        cancelButton.insets = new Insets(0, 0, 5, 5);
        cancelButton.gridx = 0;
        cancelButton.gridy = 6;
        this.contentPanel.add(buttonPane, cancelButton);
        this.korr_schet = new JTextField();
        buttonPane1 = new GridBagConstraints();
        buttonPane1.insets = new Insets(0, 0, 5, 0);
        buttonPane1.fill = 2;
        buttonPane1.gridx = 1;
        buttonPane1.gridy = 6;
        this.contentPanel.add(this.korr_schet, buttonPane1);
        this.korr_schet.setColumns(10);
        buttonPane = new JLabel("Банк");
        cancelButton = new GridBagConstraints();
        cancelButton.anchor = 13;
        cancelButton.insets = new Insets(0, 0, 5, 5);
        cancelButton.gridx = 0;
        cancelButton.gridy = 7;
        this.contentPanel.add(buttonPane, cancelButton);
        this.bank = new JTextField();
        buttonPane1 = new GridBagConstraints();
        buttonPane1.insets = new Insets(0, 0, 5, 0);
        buttonPane1.fill = 2;
        buttonPane1.gridx = 1;
        buttonPane1.gridy = 7;
        this.contentPanel.add(this.bank, buttonPane1);
        this.bank.setColumns(10);
        buttonPane = new JLabel("БИК");
        cancelButton = new GridBagConstraints();
        cancelButton.anchor = 13;
        cancelButton.insets = new Insets(0, 0, 5, 5);
        cancelButton.gridx = 0;
        cancelButton.gridy = 8;
        this.contentPanel.add(buttonPane, cancelButton);
        this.bik = new JTextField();
        buttonPane1 = new GridBagConstraints();
        buttonPane1.insets = new Insets(0, 0, 5, 0);
        buttonPane1.fill = 2;
        buttonPane1.gridx = 1;
        buttonPane1.gridy = 8;
        this.contentPanel.add(this.bik, buttonPane1);
        this.bik.setColumns(10);
        buttonPane = new JLabel("Орган управления");
        cancelButton = new GridBagConstraints();
        cancelButton.anchor = 13;
        cancelButton.insets = new Insets(0, 0, 5, 5);
        cancelButton.gridx = 0;
        cancelButton.gridy = 9;
        this.contentPanel.add(buttonPane, cancelButton);
        this.organ = new JTextField();
        buttonPane1 = new GridBagConstraints();
        buttonPane1.insets = new Insets(0, 0, 5, 0);
        buttonPane1.fill = 2;
        buttonPane1.gridx = 1;
        buttonPane1.gridy = 9;
        this.contentPanel.add(this.organ, buttonPane1);
        this.organ.setColumns(10);
        buttonPane = new JLabel("В лице");
        cancelButton = new GridBagConstraints();
        cancelButton.anchor = 13;
        cancelButton.insets = new Insets(0, 0, 5, 5);
        cancelButton.gridx = 0;
        cancelButton.gridy = 10;
        this.contentPanel.add(buttonPane, cancelButton);
        this.v_litze = new JTextField();
        buttonPane1 = new GridBagConstraints();
        buttonPane1.insets = new Insets(0, 0, 5, 0);
        buttonPane1.fill = 2;
        buttonPane1.gridx = 1;
        buttonPane1.gridy = 10;
        this.contentPanel.add(this.v_litze, buttonPane1);
        this.v_litze.setColumns(10);
        buttonPane = new JLabel("Действ. на основании");
        cancelButton = new GridBagConstraints();
        cancelButton.anchor = 13;
        cancelButton.insets = new Insets(0, 0, 5, 5);
        cancelButton.gridx = 0;
        cancelButton.gridy = 11;
        this.contentPanel.add(buttonPane, cancelButton);
        this.na_osnov = new JTextField();
        buttonPane1 = new GridBagConstraints();
        buttonPane1.insets = new Insets(0, 0, 5, 0);
        buttonPane1.fill = 2;
        buttonPane1.gridx = 1;
        buttonPane1.gridy = 11;
        this.contentPanel.add(this.na_osnov, buttonPane1);
        this.na_osnov.setColumns(10);
        buttonPane = new JLabel("Лицо");
        cancelButton = new GridBagConstraints();
        cancelButton.anchor = 13;
        cancelButton.insets = new Insets(0, 0, 5, 5);
        cancelButton.gridx = 0;
        cancelButton.gridy = 12;
        this.contentPanel.add(buttonPane, cancelButton);
        this.litzo = new JTextField();
        buttonPane1 = new GridBagConstraints();
        buttonPane1.insets = new Insets(0, 0, 5, 0);
        buttonPane1.fill = 2;
        buttonPane1.gridx = 1;
        buttonPane1.gridy = 12;
        this.contentPanel.add(this.litzo, buttonPane1);
        this.litzo.setColumns(10);
        JPanel buttonPane2 = new JPanel();
        buttonPane2.setLayout(new FlowLayout(2));
        this.getContentPane().add(buttonPane2, "South");
        JButton cancelButton1 = new JButton("OK");
        cancelButton1.setActionCommand("OK");
        buttonPane2.add(cancelButton1);
        this.getRootPane().setDefaultButton(cancelButton1);
        cancelButton1.addActionListener(this.al);
        cancelButton1 = new JButton("Cancel");
        cancelButton1.setActionCommand("Cancel");
        buttonPane2.add(cancelButton1);
        cancelButton1.addActionListener(this.al);
    }

    public void editFirm(Firm f, MyComboboxModel mcb) {
        this.mcb = mcb;
        this.result = f;
        this.name.setText(f.name);
        this.type.setText(f.type);
        this.address.setText(f.address);
        if(!this.inn.getText().equals(f.inn)) {
            this.inn.setText(f.inn);
        }

        this.kpp.setText(f.kpp);
        this.schet.setText(f.rasch_schet);
        this.korr_schet.setText(f.korr_schet);
        this.bank.setText(f.bank);
        this.bik.setText(f.bik);
        this.organ.setText(f.organ_upravl);
        this.na_osnov.setText(f.na_osnovanii);
        this.v_litze.setText(f.v_litze);
        this.litzo.setText(f.litzo);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        try {
            FirmEditDialog e = new FirmEditDialog();
            e.setDefaultCloseOperation(2);
            e.setVisible(true);
        } catch (Exception var2) {
            var2.printStackTrace();
        }

    }

    public boolean isAlreadyExists() {
        return alreadyExists;
    }

    public Firm createFirm(MyComboboxModel mcb) {
        this.create = true;
        this.result = new Firm();
        this.editFirm(this.result, mcb);
        return this.result;
    }
}

