package document;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ServiceNameEditorDialog extends JDialog {
  private final JPanel contentPanel = new JPanel();
  private ServiceNameCBModel mcb;
  private JList list = new JList();
  private ServiceNameRodPadEditDialog snrped = new ServiceNameRodPadEditDialog();

  public static void main(String[] args) {
    try {
      ServiceNameEditorDialog e = new ServiceNameEditorDialog();
      e.setDefaultCloseOperation(2);
      e.setVisible(true);
    } catch (Exception var2) {
      var2.printStackTrace();
    }

  }

  public void showDialog(ServiceNameCBModel serviceNameCBModel) {
    this.mcb = serviceNameCBModel;
    this.list.setModel(serviceNameCBModel);
    this.setVisible(true);
  }

  public ServiceNameEditorDialog() {
//    this.setUndecorated(true);
    /*this.setBounds(300, 300, 400, 300);
    this.setModal(true);
    this.getContentPane().setLayout(new BorderLayout());
    this.contentPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
    this.contentPanel.setAlignmentY(0.0F);
    this.contentPanel.setAlignmentX(0.0F);
    this.getContentPane().add(this.contentPanel, "Center");
    GridBagLayout gbl_contentPanel = new GridBagLayout();
    gbl_contentPanel.columnWidths = new int[]{100, 1, 0};
    gbl_contentPanel.rowHeights = new int[]{1, 0, 0};
    gbl_contentPanel.columnWeights = new double[]{0.0D, 0.0D, 4.9E-324D};
    gbl_contentPanel.rowWeights = new double[]{1.0D, 1.0D, 4.9E-324D};
    this.contentPanel.setLayout(gbl_contentPanel);
    JScrollPane buttonPane = new JScrollPane();
    buttonPane.setPreferredSize(new Dimension(0, 0));
    GridBagConstraints fl_buttonPane = new GridBagConstraints();
    fl_buttonPane.gridheight = 2;
    fl_buttonPane.gridwidth = 2;
    fl_buttonPane.weighty = 1.0D;
    fl_buttonPane.weightx = 1.0D;
    fl_buttonPane.fill = 1;
    fl_buttonPane.gridx = 0;
    fl_buttonPane.gridy = 0;
    this.contentPanel.add(buttonPane, fl_buttonPane);
    buttonPane.setViewportView(this.list);
    this.list.setLayoutOrientation(2);
    this.list.setSelectionMode(0);
    JPanel buttonPane1 = new JPanel();
    buttonPane1.setBorder(new LineBorder(new Color(0, 0, 0)));
    FlowLayout fl_buttonPane1 = new FlowLayout(0);
    fl_buttonPane1.setVgap(0);
    fl_buttonPane1.setHgap(0);
    buttonPane1.setLayout(fl_buttonPane1);
    this.getContentPane().add(buttonPane1, "South");*/

    setBounds(300, 300, 500, 300);
    setModal(true);

    final JPanel mainPanel = new JPanel(new GridLayout(1, 1));
    final JPanel content = new JPanel(new BorderLayout());
    final JScrollPane listPanel = new JScrollPane(list);
    content.add(listPanel, BorderLayout.CENTER);


    JButton deleteBtn = new JButton("");
    deleteBtn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          Object e2 = ServiceNameEditorDialog.this.list.getSelectedValue();
          if (e2 != null) {
            ServiceNameEditorDialog.this.mcb.setSelectedItem(e2);
          }

          ServiceNameEditorDialog.this.setVisible(false);
        } catch (RuntimeException var3) {
          ServiceNameEditorDialog.this.setVisible(false);
          var3.printStackTrace();
        }

      }
    });
    deleteBtn.setIcon(new ImageIcon("resources/images/ok.png"));
    deleteBtn.setPreferredSize(new Dimension(20, 20));
    deleteBtn.setMinimumSize(new Dimension(20, 20));
    deleteBtn.setMaximumSize(new Dimension(20, 20));
    final JPanel buttonPane1 = new JPanel(new GridLayout(1, 4));
    buttonPane1.add(deleteBtn);
    final JButton deleteBtn1 = deleteBtn;
    deleteBtn = new JButton("");
    deleteBtn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Point loc = deleteBtn1.getLocationOnScreen();
        ServiceNameEditorDialog.this.snrped.setLocation(loc.x - ServiceNameEditorDialog.this.snrped.getWidth() / 2, loc.y - ServiceNameEditorDialog.this.snrped.getHeight() / 2);
        ServiceName ans = ServiceNameEditorDialog.this.snrped.createSN();
        if (ans != null) {
          ServiceNameEditorDialog.this.mcb.addServiceName(ans);
        }

      }
    });
    deleteBtn.setIcon(new ImageIcon("resources/images/add.png"));
    deleteBtn.setMaximumSize(new Dimension(20, 20));
    deleteBtn.setMinimumSize(new Dimension(20, 20));
    deleteBtn.setPreferredSize(new Dimension(20, 20));
    buttonPane1.add(deleteBtn);
    final JButton deleteBtn2 = deleteBtn;
    deleteBtn = new JButton("");
    deleteBtn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        int index = ServiceNameEditorDialog.this.list.getSelectedIndex();
        if (index != -1) {
          Point loc = deleteBtn2.getLocationOnScreen();
          ServiceNameEditorDialog.this.snrped.setLocation(loc.x - ServiceNameEditorDialog.this.snrped.getWidth() / 2, loc.y - ServiceNameEditorDialog.this.snrped.getHeight() / 2);
          ServiceName ans = ServiceNameEditorDialog.this.snrped.editSN((ServiceName) ServiceNameEditorDialog.this.list.getSelectedValue());
          if (ans != null) {
            ServiceNameEditorDialog.this.mcb.replace(index, ans);
          }
        }

      }
    });
    deleteBtn.setIcon(new ImageIcon("resources/images/edit.png"));
    deleteBtn.setPreferredSize(new Dimension(20, 20));
    deleteBtn.setMinimumSize(new Dimension(20, 20));
    deleteBtn.setMaximumSize(new Dimension(20, 20));
    buttonPane1.add(deleteBtn);
    deleteBtn = new JButton("");
    deleteBtn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        int i = ServiceNameEditorDialog.this.list.getSelectedIndex();
        if (i != -1) {
          ServiceNameEditorDialog.this.mcb.remove(i);
        }

      }
    });
    deleteBtn.setIcon(new ImageIcon("resources/images/del.png"));
    deleteBtn.setPreferredSize(new Dimension(20, 20));
    buttonPane1.add(deleteBtn);
    content.add(buttonPane1, BorderLayout.SOUTH);
    mainPanel.add(content);
    add(mainPanel);
  }
}
