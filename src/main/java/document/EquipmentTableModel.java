package document;

import javax.swing.table.AbstractTableModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EquipmentTableModel extends AbstractTableModel implements Serializable {
    private List<String> data = new ArrayList();

    public EquipmentTableModel() {
    }

    public List<String> getList() {
        return this.data;
    }

    public int getColumnCount() {
        return 1;
    }

    public int getRowCount() {
        return this.data.size();
    }

    public Object getValueAt(int arg0, int arg1) {
        return this.data.get(arg0);
    }

    public String getColumnName(int c) {
        return "Список оборудования";
    }

    public boolean isCellEditable(int r, int c) {
        return true;
    }

    public void setValueAt(Object val, int r, int c) {
        this.data.set(r, val.toString());
    }

    public void addEquipment(String str) {
        this.data.add(str);
        this.fireTableDataChanged();
    }

    public void removeEquipment(int r) {
        this.data.remove(r);
        this.fireTableDataChanged();
    }
}
