package document;


//import gui.calendar.JDateChooser;

import es.esy.iavnfgv.fx.component.datepicker.DatePickerWithWeek;
import gui.components.FilterComboBox;
import gui.special.DateLabelFormatter;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tools.RussianMoney;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellEditor;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Locale;


/**
 * @author Dmitry Tumash
 */
public class DocumentPanel extends JPanel {
    private static final Logger log = LoggerFactory.getLogger(DocumentPanel.class);
    private static final String SELLERS_FNAME = "resources/document/sellers.dat";
    private static final String BUYERS_FNAME = "resources/document/buyers.dat";
    private final ButtonGroup buttonGroup = new ButtonGroup();
    private JTextField number;
    private JScrollPane table_sp;
    private JComboBox<?> sellerCB;
    private JComboBox<?> buyerCB;
    private JButton btnNewSeller;
    private MyComboboxModel sellerComboboxModel;
    private FirmEditDialog firmDialog = new FirmEditDialog();
    private MyComboboxModel buyersComboboxModel;
    private JFXPanel fromDateCalendarPanel = new JFXPanel();
    private JFXPanel actionDateCalendarPanel = new JFXPanel();
    private DatePickerWithWeek fromDatePicker;
    private DatePickerWithWeek actionDatePicker;
    //  private JDateChooser actionDatePicker;
//  private JDatePicker actionDatePicker = new JDatePicker();
    private EquipmentTableModel equipmentTableModel;

//  private JDatePicker fromDatePicker = new JDatePicker();
    private ServiceTableModel serviceTableModel = new ServiceTableModel();
    private JTable serviceTable;
    private JTextArea equipmentTA;
    private JTextField startTimeTF;
    private JTextField endTimeTF;
    private JTextField actionPlaceTF;
    private JRadioButton ndsRB;
    private BigDecimal total;
    private JLabel lbl;
    private FirmMiniDialog firmMiniDialog;
    private Font taFont;
    private ArchiveDialog archiveDialog;
    private DocStateTableModel docStateTM;
    private String saveDir;
    private BigDecimal total_bez_nds;
    public DocumentPanel() {
        this.total = BigDecimal.ZERO;
        this.lbl = new JLabel();
        this.firmMiniDialog = new FirmMiniDialog();
        this.taFont = new Font("SansSerif", 0, 12);
        this.archiveDialog = new ArchiveDialog();
        this.docStateTM = new DocStateTableModel();
        this.saveDir = System.getProperty("user.dir", ".");
        SpringLayout springLayout = new SpringLayout();
        this.setLayout(springLayout);

        Locale.setDefault(new Locale("ru", "RU"));
        Calendar cal = Calendar.getInstance(new Locale("ru", "RU"));
       // UtilCalendarModel ucm = new UtilCalendarModel(cal);



        /*this.fromDatePicker = new JDatePickerImpl(new JDatePanelImpl(ucm), new DateLabelFormatter());
        this.fromDatePicker.setMinimumSize(new Dimension(48, 26));
        this.fromDatePicker.setPreferredSize(new Dimension(202, 26));
        this.fromDatePicker.getJFormattedTextField().setMargin(new Insets(0, 0, 0, 0));
        this.fromDatePicker.getJFormattedTextField().setMinimumSize(new Dimension(6, 23));
        this.fromDatePicker.getJFormattedTextField().setBackground(Color.white);*/
        final Locale aLocale = new Locale("ru", "RU");
        //   final JDateChooser jDateChooser = new JDateChooser(gui.special.DateLabelFormatter.datePattern, false);
        //   jDateChooser.setLocale(aLocale);
        //   fromDatePicker = jDateChooser;


      //  UtilCalendarModel ucm2 = new UtilCalendarModel();
        Locale.setDefault(new Locale("ru", "RU"));
        //final JDateChooser jDateChooser1 = new JDateChooser(gui.special.DateLabelFormatter.datePattern, true);
        // jDateChooser1.setLocale(aLocale);
        // actionDatePicker = jDateChooser1;
//    springLayout.putConstraint("East", this.fromDatePicker, 0, "East", this.actionDatePicker);
        springLayout.putConstraint("East", this.fromDateCalendarPanel, 0, "East", this.actionDateCalendarPanel);


        JLabel label = new JLabel("Продавец");
        springLayout.putConstraint("North", label, 20, "North", this);
        springLayout.putConstraint("West", label, 10, "West", this);
        this.add(label);
        this.sellerComboboxModel = new MyComboboxModel("resources/document/sellers.dat", false);
        this.sellerCB = new JComboBox(this.sellerComboboxModel);
        if (this.sellerComboboxModel.getSize() != 0) {
            this.sellerCB.setSelectedIndex(0);
        }

        springLayout.putConstraint("North", this.sellerCB, -3, "North", label);
        springLayout.putConstraint("West", this.sellerCB, 6, "East", label);
        springLayout.putConstraint("East", this.sellerCB, 209, "East", label);
        this.add(this.sellerCB);
        this.btnNewSeller = new JButton("");
        this.btnNewSeller.setIconTextGap(0);
        this.btnNewSeller.setIcon(new ImageIcon(DocumentPanel.class.getResource("/org/apache/fop/render/awt/viewer/images/nextpg.gif")));
        this.btnNewSeller.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                Point p = DocumentPanel.this.btnNewSeller.getLocationOnScreen();
                int w = DocumentPanel.this.btnNewSeller.getWidth();
                DocumentPanel.this.firmMiniDialog.setLocation(p.x + w, p.y);
                DocumentPanel.this.firmMiniDialog.showDialog(DocumentPanel.this.sellerComboboxModel);
            }
        });
        springLayout.putConstraint("North", this.btnNewSeller, 0, "North", this.sellerCB);
        springLayout.putConstraint("West", this.btnNewSeller, 0, "East", this.sellerCB);
        springLayout.putConstraint("South", this.btnNewSeller, 0, "South", this.sellerCB);
        this.btnNewSeller.setAlignmentX(0.5F);
        this.btnNewSeller.setMinimumSize(new Dimension(21, 21));
        this.btnNewSeller.setMaximumSize(new Dimension(21, 21));
        this.btnNewSeller.setPreferredSize(new Dimension(21, 21));
        this.btnNewSeller.setMargin(new Insets(0, 0, 0, 0));
        this.add(this.btnNewSeller);
        JLabel lblNewLabel = new JLabel("Покупатель");
        springLayout.putConstraint("North", lblNewLabel, 0, "North", label);
        springLayout.putConstraint("East", lblNewLabel, -284, "East", this);
        this.add(lblNewLabel);
        this.buyersComboboxModel = new MyComboboxModel("resources/document/buyers.dat", true);
        this.buyerCB = new FilterComboBox(this.buyersComboboxModel);
        springLayout.putConstraint("North", this.buyerCB, -3, "North", label);
        springLayout.putConstraint("West", this.buyerCB, 6, "East", lblNewLabel);
        this.add(this.buyerCB);
        final JButton btnNewBuyer = new JButton("");
        btnNewBuyer.setIcon(new ImageIcon(DocumentPanel.class.getResource("/org/apache/fop/render/awt/viewer/images/nextpg.gif")));
        springLayout.putConstraint("North", btnNewBuyer, 0, "North", this.buyerCB);
        springLayout.putConstraint("West", btnNewBuyer, 0, "East", this.buyerCB);
        springLayout.putConstraint("South", btnNewBuyer, 0, "South", this.buyerCB);
        btnNewBuyer.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                Point p = btnNewBuyer.getLocationOnScreen();
                int w = btnNewBuyer.getWidth();
                DocumentPanel.this.firmMiniDialog.setLocation(p.x - DocumentPanel.this.firmMiniDialog.getWidth(), p.y);
                DocumentPanel.this.buyersComboboxModel.setFilterString(null);
                DocumentPanel.this.firmMiniDialog.showDialog(DocumentPanel.this.buyersComboboxModel);
            }
        });
        btnNewBuyer.setPreferredSize(new Dimension(21, 21));
        btnNewBuyer.setMinimumSize(new Dimension(21, 21));
        btnNewBuyer.setMaximumSize(new Dimension(21, 21));
        btnNewBuyer.setMargin(new Insets(0, 0, 0, 0));
        btnNewBuyer.setAlignmentX(0.5F);
        this.add(btnNewBuyer);
        JLabel label_1 = new JLabel("Счет");
        springLayout.putConstraint("North", label_1, 15, "South", label);
        springLayout.putConstraint("West", label_1, -8, "West", label);
        this.add(label_1);
        this.number = new JTextField();
        springLayout.putConstraint("North", this.fromDateCalendarPanel, 0, "North", this.number);
        springLayout.putConstraint("South", this.fromDateCalendarPanel,0, "South", this.number);
        springLayout.putConstraint("North", this.number, -4, "North", label_1);
        springLayout.putConstraint("West", this.number, 6, "East", label_1);
        springLayout.putConstraint("East", this.number, 50, "East", label_1);
        this.number.setPreferredSize(new Dimension(6, 26));
        this.number.setMinimumSize(new Dimension(6, 26));
        this.add(this.number);
        this.number.setColumns(10);
        JButton btnN = new JButton(">");
        springLayout.putConstraint("North", btnN, -4, "North", label_1);
        springLayout.putConstraint("West", btnN, 1, "East", this.number);
        btnN.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    Integer nfre = Integer.valueOf(Integer.parseInt(DocumentPanel.this.number.getText()));
                    DocumentPanel.this.number.setText(DocumentPanel.this.docStateTM.nextNum(nfre).toString());
                    setDocumentState(DocumentPanel.this.getDocumentState());
                } catch (NumberFormatException var3) {
                    DocumentPanel.this.number.setText(DocumentPanel.this.docStateTM.nextNum(Integer.valueOf(0)).toString());
                }

            }
        });
        btnN.setPreferredSize(new Dimension(21, 25));
        btnN.setMinimumSize(new Dimension(21, 21));
        btnN.setMaximumSize(new Dimension(21, 21));
        btnN.setMargin(new Insets(0, 0, 0, 0));
        btnN.setAlignmentX(0.5F);
        this.add(btnN);
        JLabel label_2 = new JLabel("от");
        springLayout.putConstraint("West", this.fromDateCalendarPanel, 0, "East", label_2);
        springLayout.putConstraint("North", label_2, 0, "North", label_1);
        springLayout.putConstraint("West", label_2, 0, "East", btnN);
        this.add(label_2);

        //actionDatePicker.setTextEditable(true);
        this.add(this.fromDateCalendarPanel);
        //if someday DocumentPanel will be created first
        Platform.runLater(() -> fromDatePicker = new DatePickerWithWeek(false));
        Platform.runLater(() -> initFX(fromDateCalendarPanel, fromDatePicker));
        this.ndsRB = new JRadioButton("Начислить НДС");
        this.ndsRB.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                DocumentPanel.this.updateTotal();
            }
        });
        this.buttonGroup.add(this.ndsRB);
        this.add(this.ndsRB);
        JRadioButton radioButton_1 = new JRadioButton("Выделить НДС");
        radioButton_1.setSelected(true);
        springLayout.putConstraint("North", radioButton_1, 0, "North", this.ndsRB);
        springLayout.putConstraint("East", radioButton_1, -10, "West", this.ndsRB);
        this.buttonGroup.add(radioButton_1);
        this.add(radioButton_1);
        JLabel label_4 = new JLabel("Дата мероприятия");
        springLayout.putConstraint("North", label_4, 25, "South", this.number);
        springLayout.putConstraint("North", this.actionDateCalendarPanel, -2, "North", label_4);
        springLayout.putConstraint("West", label_4, 0, "West", label);
        this.add(label_4);
        springLayout.putConstraint("West", this.actionDateCalendarPanel, 6, "East", label_4);
        springLayout.putConstraint("East", this.actionDateCalendarPanel, 0, "East", this.btnNewSeller);

//    ((JTextField)actionDatePicker.getJFormattedTextField()).setEditable(true);
//    ((JTextField)actionDatePicker.getJFormattedTextField()).setBackground(Color.white);

        this.add(this.actionDateCalendarPanel);
        //if someday DocumentPanel will be created first
        Platform.runLater(() -> actionDatePicker = new DatePickerWithWeek(false));
        Platform.runLater(() -> initFX(actionDateCalendarPanel, actionDatePicker));
        JLabel label_5 = new JLabel("Время начала");
        springLayout.putConstraint("North", label_5, 6, "South", label_4);
        springLayout.putConstraint("East", label_5, 0, "East", label_4);
        this.add(label_5);
        this.startTimeTF = new JTextField();
        springLayout.putConstraint("North", this.startTimeTF, 0, "South", this.actionDateCalendarPanel);
        springLayout.putConstraint("West", this.startTimeTF, 0, "West", this.actionDateCalendarPanel);
        springLayout.putConstraint("East", this.startTimeTF, 0, "East", this.btnNewSeller);
        this.add(this.startTimeTF);
        JLabel label_6 = new JLabel("Время окончания");
        springLayout.putConstraint("North", label_6, 6, "South", label_5);
        springLayout.putConstraint("East", label_6, 0, "East", label_5);
        this.add(label_6);
        this.endTimeTF = new JTextField();
        springLayout.putConstraint("North", this.endTimeTF, 0, "South", this.startTimeTF);
        springLayout.putConstraint("West", this.endTimeTF, 0, "West", this.actionDateCalendarPanel);
        springLayout.putConstraint("East", this.endTimeTF, 0, "East", this.btnNewSeller);
        this.add(this.endTimeTF);
        JLabel label_7 = new JLabel("Место проведения");
        springLayout.putConstraint("North", label_7, 6, "South", label_6);
        springLayout.putConstraint("East", label_7, 0, "East", label_6);
        this.add(label_7);
        this.actionPlaceTF = new JTextField();
        springLayout.putConstraint("North", this.actionPlaceTF, 0, "South", this.endTimeTF);
        springLayout.putConstraint("West", this.actionPlaceTF, 6, "East", label_7);
        springLayout.putConstraint("East", this.actionPlaceTF, 0, "East", this.btnNewSeller);
        this.add(this.actionPlaceTF);
        JButton addButton = new JButton("Добавить");
        springLayout.putConstraint("West", addButton, 311, "West", this);
        springLayout.putConstraint("South", addButton, 78, "South", this);
        this.add(addButton);
        JButton button_2 = new JButton("Удалить");
        springLayout.putConstraint("West", button_2, 6, "East", addButton);
        springLayout.putConstraint("South", button_2, 78, "South", this);
        this.add(button_2);
        this.equipmentTableModel = new EquipmentTableModel();
        JScrollPane equipmentTablesp = new JScrollPane();
        equipmentTablesp.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Список оборудования", 2, 2, (Font) null, new Color(0, 0, 0)));
        springLayout.putConstraint("North", equipmentTablesp, 5, "South", this.buyerCB);
        springLayout.putConstraint("East", this.buyerCB, 0, "East", equipmentTablesp);
        springLayout.putConstraint("West", equipmentTablesp, 20, "East", this.actionDateCalendarPanel);
        springLayout.putConstraint("South", equipmentTablesp, 0, "South", this.actionPlaceTF);
        springLayout.putConstraint("East", equipmentTablesp, -57, "East", this);
        this.add(equipmentTablesp);
        this.table_sp = new JScrollPane();
        springLayout.putConstraint("North", this.table_sp, 20, "South", this.actionPlaceTF);
        springLayout.putConstraint("South", this.table_sp, -140, "South", this);
        springLayout.putConstraint("North", this.ndsRB, 6, "South", this.table_sp);
        springLayout.putConstraint("East", this.ndsRB, 0, "East", this.table_sp);
        springLayout.putConstraint("West", this.table_sp, 10, "West", this);
        springLayout.putConstraint("East", this.table_sp, 0, "East", equipmentTablesp);
        this.equipmentTA = new JTextArea();
        this.equipmentTA.setFont(this.taFont);
        equipmentTablesp.setViewportView(this.equipmentTA);
        this.add(this.table_sp);
        JButton btnAddService = new JButton("Добавить");
        springLayout.putConstraint("North", btnAddService, 6, "South", this.table_sp);
        springLayout.putConstraint("West", btnAddService, 0, "West", label);
        btnAddService.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                serviceTableModel.addR();
            }
        });
        final ServiceName moreAttractiveAttraction = new ServiceName();
        moreAttractiveAttraction.serviceName = "Комплекс услуг по организации развлекательной зоны";
        moreAttractiveAttraction.serviceNameRodPad = "комплекса услуг по организации развлекательной зоны";

        serviceTableModel.addR(moreAttractiveAttraction);
        this.add(btnAddService);
        JButton btnNewButton_4 = new JButton("Удалить");
        springLayout.putConstraint("North", btnNewButton_4, 0, "North", btnAddService);
        springLayout.putConstraint("West", btnNewButton_4, 8, "East", btnAddService);
        btnNewButton_4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int i = DocumentPanel.this.serviceTable.getSelectedRow();
                i = DocumentPanel.this.serviceTable.convertRowIndexToModel(i);
                if (i != -1) {
                    TableCellEditor tce = DocumentPanel.this.serviceTable.getCellEditor();
                    if (tce != null) {
                        tce.cancelCellEditing();
                    }

                    DocumentPanel.this.serviceTableModel.remove(i);
                }

            }
        });
        this.add(btnNewButton_4);
        JLabel label_9 = new JLabel("Итого");
        springLayout.putConstraint("North", label_9, 6, "South", btnAddService);
        springLayout.putConstraint("West", label_9, 0, "West", btnAddService);
        this.serviceTable = new JTable(this.serviceTableModel) {
            @Override
            public void changeSelection(int rowIndex, int columnIndex, boolean toggle, boolean extend) {
                super.changeSelection(rowIndex, columnIndex, toggle, extend);
                if (editCellAt(rowIndex, columnIndex)) {
                    final Component editor = getEditorComponent();
                    if (editor == null || !(editor instanceof JTextComponent)) {
                        return;
                    }
                    String text = ((JTextComponent) editor).getText();
                    double curPrice;
                    try {
                        curPrice = Double.parseDouble(text);
                    } catch (NumberFormatException ex) {
                        log.error(ex.getMessage(), ex);
                        return;
                    }
                    if (curPrice == 0) {
                        editor.requestFocusInWindow();
                        ((JTextComponent) editor).selectAll();
                    }
                }
            }
        };

        this.serviceTable.setAutoResizeMode(4);
        this.serviceTable.getColumnModel().getColumn(0).setPreferredWidth(400);
        this.serviceTable.getColumnModel().getColumn(0).setCellEditor(new ServiceNameCellEditor());
        this.serviceTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        this.serviceTable.getModel().addTableModelListener(new TableModelListener() {
            public void tableChanged(TableModelEvent e) {
                DocumentPanel.this.updateTotal();
            }
        });
        this.table_sp.setViewportView(this.serviceTable);
        this.add(label_9);
        JButton button_3 = new JButton("Сохранить");
        springLayout.putConstraint("North", button_3, 8, "South", label_9);
        button_3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DocumentPanel.this.saveDocState();
                Printing.saveDOCX(DocumentPanel.this.getDocumentState(), DocumentPanel.this.saveDir);
                Printing.savePdf(DocumentPanel.this.getDocumentState(), DocumentPanel.this.saveDir);
                Printing.savePdfDogovor(DocumentPanel.this.getDocumentState(), DocumentPanel.this.saveDir);
            }
        });
        springLayout.putConstraint("West", button_3, 0, "West", label);
        this.add(button_3);
        JButton btnNewButton_5 = new JButton("Печать");
        springLayout.putConstraint("North", btnNewButton_5, 8, "South", button_3);
        springLayout.putConstraint("West", btnNewButton_5, 0, "West", button_3);
        springLayout.putConstraint("East", btnNewButton_5, 0, "East", button_3);
        btnNewButton_5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DocumentPanel.this.saveDocState();
                Printing.printDOCX(DocumentPanel.this.getDocumentState());
            }
        });
        this.add(btnNewButton_5);
        JButton button_6 = new JButton("Очистить");
        springLayout.putConstraint("West", button_6, 5, "East", button_3);
        button_6.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DocumentPanel.this.clearFields();
            }
        });
        springLayout.putConstraint("North", button_6, 0, "North", button_3);
        this.add(button_6);
        JButton button_7 = new JButton("Открыть счет из архива");
        springLayout.putConstraint("North", button_7, 0, "North", btnNewButton_5);
        springLayout.putConstraint("West", button_7, 5, "East", btnNewButton_5);
        button_7.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DocumentState ds = DocumentPanel.this.archiveDialog.getDocState();
                if (ds != null) {
                    DocumentPanel.this.setDocumentState(ds);
                }

            }
        });
        this.add(button_7);
        JButton btnn = new JButton("Выбрать папку \r\n");
        springLayout.putConstraint("West", btnn, 481, "West", this);
        btnn.setIcon(new ImageIcon(DocumentPanel.class.getResource("/com/sun/java/swing/plaf/windows/icons/Directory.gif")));
        btnn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser fChoser = new JFileChooser(DocumentPanel.this.saveDir);
                fChoser.setFileSelectionMode(1);
                int ans = fChoser.showOpenDialog(DocumentPanel.this);
                if (ans == 0) {
                    DocumentPanel.this.saveDir = fChoser.getSelectedFile().toString();

                    try {
                        FileWriter e1 = new FileWriter("resources/dirPath");
                        e1.write(DocumentPanel.this.saveDir);
                        e1.close();
                    } catch (IOException var5) {
                        var5.printStackTrace();
                    }
                }

            }
        });
        springLayout.putConstraint("North", btnn, 0, "North", button_3);
        btnn.setActionCommand("Выбрать папку \r\n");
        this.add(btnn);
        JScrollPane scrollPane = new JScrollPane();
        this.add(scrollPane);
        this.lbl = new JLabel("");
        springLayout.putConstraint("North", this.lbl, 0, "North", label_9);
        springLayout.putConstraint("West", this.lbl, 6, "East", label_9);
        this.add(this.lbl);
        JLabel lblNewLabel_1 = new JLabel("Данные о мероприятии");
        springLayout.putConstraint("North", lblNewLabel_1, 8, "South", label_1);
        lblNewLabel_1.setFont(new Font("Segoe UI", 0, 13));
        springLayout.putConstraint("West", lblNewLabel_1, 17, "West", this.sellerCB);
        springLayout.putConstraint("East", lblNewLabel_1, 0, "East", this.sellerCB);
        lblNewLabel_1.setAlignmentX(0.5F);
        this.add(lblNewLabel_1);
        JButton btnNewButton = new JButton("Открыть папку");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (Desktop.isDesktopSupported()) {
                    try {
                        Desktop.getDesktop().open(new File(DocumentPanel.this.saveDir));
                    } catch (IOException var3) {
                        var3.printStackTrace();
                    }
                }

            }
        });
        btnNewButton.setIcon(new ImageIcon(DocumentPanel.class.getResource("/com/sun/java/swing/plaf/windows/icons/UpFolder.gif")));
        springLayout.putConstraint("North", btnNewButton, 8, "South", btnn);
        springLayout.putConstraint("West", btnNewButton, 0, "West", btnn);
        springLayout.putConstraint("East", btnNewButton, 0, "East", btnn);
        this.add(btnNewButton);
        this.initDocPanel();
        this.loadDirPath();
        this.archiveDialog.setModel(this.docStateTM);
    }

    private DocumentState getDocumentState() {
        DocumentState ds = new DocumentState();
        final Calendar instance = Calendar.getInstance();
//    instance.setTime((Date) this.actionDatePicker.getModel().getValue());
//        instance.setTime(((Calendar) this.actionDatePicker.getModel().getValue()).getTime());
        instance.setTime(actionDatePicker.getDatePicker().getCalendar().getTime());
        ds.actionDate = instance;
        ds.actionPlace = this.actionPlaceTF.getText();
        if (this.buyerCB.getSelectedItem() != null) {
            ds.buyer = (Firm) this.buyerCB.getSelectedItem();
        } else {
            ds.buyer = new Firm();
        }

        ds.endTime = this.endTimeTF.getText();
        ds.equipmentString = this.equipmentTA.getText();
        final Calendar instance1 = Calendar.getInstance();

//    instance1.setTime((Date) this.fromDatePicker.getModel().getValue());
//        instance1.setTime(((Calendar) this.fromDatePicker.getModel().getValue()).getTime());
        instance1.setTime(fromDatePicker.getDatePicker().getCalendar().getTime());
        ds.fromDate = instance1;
        ds.nds = this.ndsRB.isSelected();

        try {
            ds.number = Integer.valueOf(Integer.parseInt(this.number.getText()));
        } catch (NumberFormatException var3) {
            ds.number = Integer.valueOf(0);
        }

        if (this.sellerCB.getSelectedItem() != null) {
            ds.seller = (Firm) this.sellerCB.getSelectedItem();
        } else {
            ds.seller = new Firm();
        }

        ds.servicesTM = this.serviceTableModel.getList();
        ds.startTime = this.startTimeTF.getText();
        this.updateTotal();
        ds.total = this.total;
        return ds;
    }

    private void initFX(JFXPanel fxPanel, DatePickerWithWeek datePickerCheap) {
        // This method is invoked on the JavaFX thread
        Scene scene = createScene(datePickerCheap);
//        fxPanel.setLayout(new FlowLayout());
        fxPanel.setScene(scene);
    }

    private void updateTotal() {
        BigDecimal t = this.serviceTableModel.getTotal();
        if (this.ndsRB.isSelected()) {
            this.total = t.multiply(new BigDecimal("1.18"));
            this.total_bez_nds = t;
            this.lbl.setText(RussianMoney.totalToStrWithNds(this.total));
        } else {
            this.total = t;
            this.total_bez_nds = t.setScale(2).divide(new BigDecimal("1.18"), RoundingMode.HALF_DOWN);
            this.lbl.setText(RussianMoney.totalToStrWithNds(this.total));
        }

    }

    private void saveDocState() {
        DocumentState ds = this.getDocumentState();
        int index = this.docStateTM.getIndexForNum(ds.number.intValue());
        if (index != -1) {
            DocumentState dsSaved = this.docStateTM.getDocState(index);
            int ans = JOptionPane.showConfirmDialog(this, "Документ с номером " + dsSaved.number + " уже существует, заменить?\n" + "Покупатель: " + dsSaved.buyer.name + "\nДата составления: " + (dsSaved.fromDate == null ? "" : Printing.sdf.format(dsSaved.fromDate.getTime())) + "\nДата мероприятия: " + (dsSaved.actionDate == null ? "" : Printing.sdf.format(dsSaved.actionDate.getTime())) + "\nМесто проведения: " + dsSaved.actionPlace + "\nНименование: " + dsSaved.getServiceNameInRow() + "\nСумма: " + dsSaved.total, "Внимание!!!", 0, 3);
            if (ans == 0) {
                this.docStateTM.replaceDocState(ds);
            }
        } else {
            this.docStateTM.addDocState(ds);
        }

    }

    private void clearFields() {
//    this.actionDatePicker.setDate(new Date());
//    ((JTextField)this.actionDatePicker.getJFormattedTextField()).setText("");
        this.actionDatePicker.getDatePicker().setCalendar(null);
        this.actionPlaceTF.setText("");
        this.buyerCB.setSelectedItem((Object) null);
        this.startTimeTF.setText("");
        this.endTimeTF.setText("");
        this.equipmentTableModel = new EquipmentTableModel();
        this.equipmentTA.setText("");
//    this.fromDatePicker.setDate(new Date());
//    ((JTextField)this.fromDatePicker.getJFormattedTextField()).setText("");
        this.fromDatePicker.getDatePicker().setCalendar(null);
        this.number.setText("");
        this.serviceTableModel.clearList();
        this.startTimeTF.setText("");
        this.total = BigDecimal.ZERO;
        this.lbl.setText(this.total.toString());
    }

    private void initDocPanel() {
    }

    private void loadDirPath() {
        try {
            FileReader e = new FileReader("resources/dirPath");
            this.saveDir = (new BufferedReader(e)).readLine();
            if (!(new File(this.saveDir)).exists()) {
                this.saveDir = ".";
            }
        } catch (IOException var2) {
            var2.printStackTrace();
        }

    }

    private Scene createScene(DatePickerWithWeek datePickerCheap) {
        Group root = new Group();
        Scene scene = new Scene(root, 50, 25);
//        root.
        datePickerCheap.setDatePattern(DateLabelFormatter.datePattern);
        root.getChildren().add(datePickerCheap);
        datePickerCheap.setPrefSize(179, 25);
        return (scene);
    }

    private void setDocumentState(DocumentState aDs) {
        DocumentState ds = new DocumentState(aDs);
        if (ds.actionDate != null) {
//      this.actionDatePicker.setDate(ds.actionDate.getTime());
            //Calendar selectedCalendar = (Calendar) this.actionDatePicker.getModel().getValue();
            // selectedCalendar.setTime(ds.actionDate.getTime());
//      Date date = ds.actionDate.getTime();
            actionDatePicker.getDatePicker().setCalendar(ds.actionDate);
//      this.actionDatePicker.getJDateInstantPanel().getModel().setValue(ds.actionDate);

        }

        this.actionPlaceTF.setText(ds.actionPlace);
        this.buyersComboboxModel.setSelectedItem(ds.buyer);
        this.endTimeTF.setText(ds.endTime);
        this.equipmentTA.setText(ds.equipmentString);
        if (ds.fromDate != null) {
//      this.fromDatePicker.setDate(ds.fromDate.getTime());
//      Calendar selectedCalendar = (Calendar) this.fromDatePicker.getModel().getValue();
//      selectedCalendar.setTime(ds.actionDate.getTime());
            fromDatePicker.getDatePicker().setCalendar(ds.fromDate);
        }

        this.number.setText(ds.number.toString());
        this.sellerComboboxModel.setSelectedItem(ds.seller);
        this.serviceTableModel.setList(ds.servicesTM);
        this.startTimeTF.setText(ds.startTime);
        this.total = ds.total;
        this.lbl.setText(this.total.toString());
    }

    public static void main(String[] args) {
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(3);
        f.getContentPane().add(new DocumentPanel());
        f.setVisible(true);
        f.setSize(700, 600);
        f.setResizable(false);
    }

    public DocStateTableModel getDocStateTM() {
        return this.docStateTM;
    }
}
