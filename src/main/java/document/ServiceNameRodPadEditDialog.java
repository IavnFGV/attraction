package document;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ServiceNameRodPadEditDialog extends JDialog {
    private final JPanel contentPanel = new JPanel();
    private JTextField textField;
    private JTextField textField_1;
    private ServiceName sn;

    public static void main(String[] args) {
        try {
            ServiceNameRodPadEditDialog e = new ServiceNameRodPadEditDialog();
            e.setDefaultCloseOperation(2);
            e.setVisible(true);
        } catch (Exception var2) {
            var2.printStackTrace();
        }

    }

    public ServiceName editSN(ServiceName sn) {
        this.sn = sn;
        this.textField.setText(sn.serviceName);
        this.textField_1.setText(sn.serviceNameRodPad);
        this.setVisible(true);
        return sn;
    }

    public ServiceName createSN() {
        this.sn = new ServiceName();
        this.textField.setText("");
        this.textField_1.setText("");
        this.setVisible(true);
        return this.sn;
    }

    public ServiceNameRodPadEditDialog() {
        this.setBounds(100, 100, 450, 128);
        this.setModal(true);
        this.getContentPane().setLayout(new BorderLayout());
        this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.getContentPane().add(this.contentPanel, "Center");
        GridBagLayout gbl_contentPanel = new GridBagLayout();
        gbl_contentPanel.columnWidths = new int[3];
        gbl_contentPanel.rowHeights = new int[3];
        gbl_contentPanel.columnWeights = new double[]{0.0D, 1.0D, 4.9E-324D};
        gbl_contentPanel.rowWeights = new double[]{0.0D, 0.0D, 4.9E-324D};
        this.contentPanel.setLayout(gbl_contentPanel);
        JLabel buttonPane = new JLabel("Наименование");
        GridBagConstraints cancelButton = new GridBagConstraints();
        cancelButton.anchor = 13;
        cancelButton.insets = new Insets(0, 0, 5, 5);
        cancelButton.gridx = 0;
        cancelButton.gridy = 0;
        this.contentPanel.add(buttonPane, cancelButton);
        this.textField = new JTextField();
        GridBagConstraints buttonPane1 = new GridBagConstraints();
        buttonPane1.insets = new Insets(0, 0, 5, 0);
        buttonPane1.fill = 2;
        buttonPane1.gridx = 1;
        buttonPane1.gridy = 0;
        this.contentPanel.add(this.textField, buttonPane1);
        this.textField.setColumns(10);
        buttonPane = new JLabel("Наименование для договора");
        cancelButton = new GridBagConstraints();
        cancelButton.anchor = 13;
        cancelButton.insets = new Insets(0, 0, 0, 5);
        cancelButton.gridx = 0;
        cancelButton.gridy = 1;
        this.contentPanel.add(buttonPane, cancelButton);
        this.textField_1 = new JTextField();
        buttonPane1 = new GridBagConstraints();
        buttonPane1.fill = 2;
        buttonPane1.gridx = 1;
        buttonPane1.gridy = 1;
        this.contentPanel.add(this.textField_1, buttonPane1);
        this.textField_1.setColumns(10);
        JPanel buttonPane2 = new JPanel();
        buttonPane2.setLayout(new FlowLayout(2));
        this.getContentPane().add(buttonPane2, "South");
        JButton cancelButton1 = new JButton("OK");
        cancelButton1.setActionCommand("OK");
        buttonPane2.add(cancelButton1);
        this.getRootPane().setDefaultButton(cancelButton1);
        cancelButton1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ServiceNameRodPadEditDialog.this.sn.serviceName = ServiceNameRodPadEditDialog.this.textField.getText();
                ServiceNameRodPadEditDialog.this.sn.serviceNameRodPad = ServiceNameRodPadEditDialog.this.textField_1.getText();
                ServiceNameRodPadEditDialog.this.setVisible(false);
            }
        });
        cancelButton1 = new JButton("Cancel");
        cancelButton1.setActionCommand("Cancel");
        buttonPane2.add(cancelButton1);
        cancelButton1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ServiceNameRodPadEditDialog.this.sn = null;
                ServiceNameRodPadEditDialog.this.setVisible(false);
            }
        });
    }
}