package document;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ServiceNameCBModel extends AbstractListModel implements ComboBoxModel {
    private String file = "resources/document/serviceName.dat";
    private List<ServiceName> list = new ArrayList();
    private int sellectedIndex = 0;
    Object sellected = null;
    private boolean valueEdited = false;

    public void setEdited() {
        this.valueEdited = true;
    }

    public ServiceNameCBModel() {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(this.file));
            Object e = in.readObject();
            if(e instanceof List) {
                this.list = (List)e;
            }

            in.close();
        } catch (Exception var3) {
            var3.printStackTrace();
        }

    }

    private void save() {
        try {
            ObjectOutputStream e = new ObjectOutputStream(new FileOutputStream(this.file));
            e.writeObject(this.list);
            e.flush();
            e.close();
        } catch (FileNotFoundException var2) {
            var2.printStackTrace();
        } catch (IOException var3) {
            var3.printStackTrace();
        }

    }

    public Object getElementAt(int arg0) {
        return this.list.get(arg0);
    }

    public int getSize() {
        return this.list.size();
    }

    public Object getSelectedItem() {
        return this.sellected;
    }

    public void setSelectedItem(Object arg0) {
        this.sellected = arg0;
        this.fireContentsChanged(this, 0, this.list.size());
    }

    public void addServiceName(ServiceName name) {
        if(!"".equals(name.toString())) {
            this.list.add(name);
            this.setSelectedItem(name);
            this.fireIntervalAdded(this, 0, this.list.size() - 1);
            this.save();
        }

    }

    public void remove(int i) {
        this.list.remove(i);
        this.setSelectedItem(this.list.get(0));
        this.fireIntervalRemoved(this, i, i);
    }

    public void replace(int i, ServiceName val) {
        this.list.set(i, val);
        this.setSelectedItem(val);
        this.fireContentsChanged(this, i, i);
        this.save();
    }
}