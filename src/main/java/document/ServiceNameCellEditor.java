package document;

import tools.FrameUtil;
import tools.GBC;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ServiceNameCellEditor extends AbstractCellEditor implements TableCellEditor {
  private ServiceNameCBModel serviceNameCBModel;
  private JComboBox editor = new JComboBox();
  private JPanel editorPanel = new JPanel();
  private JButton editorButton = new JButton();
  private ServiceNameEditorDialog dialog = new ServiceNameEditorDialog();
  private boolean edited;
  private int sellectedInd = 0;

  public ServiceNameCellEditor() {
    this.editor.setEditable(true);
    this.serviceNameCBModel = new ServiceNameCBModel();
    this.editor.setModel(this.serviceNameCBModel);
    JTextComponent tc = (JTextComponent) this.editor.getEditor().getEditorComponent();
    this.editorButton.setIcon(new ImageIcon(DocumentPanel.class.getResource("/org/apache/fop/render/awt/viewer/images/nextpg.gif")));
    Dimension d = new Dimension(23, 23);
    this.editorButton.setMinimumSize(d);
    this.editorButton.setMaximumSize(d);
    this.editorButton.setPreferredSize(d);
    this.editorPanel.setLayout(new GridBagLayout());
    this.editorPanel.add(this.editor, (new GBC(0, 0)).setFill(1).setWeight(1.0D, 1.0D));
    this.editorPanel.add(this.editorButton, (new GBC(1, 0)).setFill(1));
    this.editorButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Point p = ServiceNameCellEditor.this.editorButton.getLocationOnScreen();
//                ServiceNameCellEditor.this.dialog.setLocation(p.x - ServiceNameCellEditor.this.dialog.getWidth(), p.y);
        dialog.setLocationRelativeTo(FrameUtil.getCurrentFrame());
        ServiceNameCellEditor.this.dialog.showDialog(ServiceNameCellEditor.this.serviceNameCBModel);
      }
    });
  }

  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
    this.editor.getEditor().setItem(value);
    this.editor.setSelectedItem(value);
    this.serviceNameCBModel.setSelectedItem(value);
    return this.editorPanel;
  }

  public Object getCellEditorValue() {
    return this.editor.getEditor().getItem();
  }
}

