package document;


import document.ServiceTableModel.R;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Dmitry Tumash
 */
public class DocumentState implements Serializable {
    private static final long serialVersionUID = 52463489482356521L;
    Firm seller = new Firm();
    Firm buyer = new Firm();
    Integer number;
    Calendar fromDate;
    String notes = "";
    boolean nds;
    Calendar actionDate;
    String startTime = "";
    String endTime = "";
    String actionPlace = "";
    String equipmentString = "";
    List<R> servicesTM;
    BigDecimal total;
    BigDecimal total_bez_nds;
    boolean payed;
    boolean willNotBePayed;

    public DocumentState(DocumentState otherDs) {

        this.seller = otherDs.seller;
        this.buyer = otherDs.buyer;
        if (otherDs.number != null) {
            this.number = otherDs.number;
        }
        if (otherDs.fromDate != null) {
            this.fromDate = new GregorianCalendar() {{
                setTime(otherDs.fromDate.getTime());
            }};
        }
        if (otherDs.notes != null) {
            this.notes = otherDs.notes;
        }
        this.nds = otherDs.nds;
        if (otherDs.actionDate != null) {
            this.actionDate = new GregorianCalendar() {{
                setTime(otherDs.actionDate.getTime());
            }};
        }
        if (otherDs.startTime != null) {
            this.startTime = otherDs.startTime;
        }
        if (otherDs.endTime != null) {
            this.endTime = otherDs.endTime;
        }
        if (otherDs.actionPlace != null) {
            this.actionPlace = otherDs.actionPlace;
        }
        if (otherDs.equipmentString != null) {
            this.equipmentString = otherDs.equipmentString;
        }
        if (otherDs.servicesTM != null) {
            this.servicesTM = otherDs.servicesTM.stream().map(R::new).collect(Collectors.toList());
//            this.servicesTM = new ArrayList<>(otherDs.servicesTM);
        }
        if (otherDs.total != null) {
            this.total = otherDs.total;
        }
        if (otherDs.total_bez_nds != null) {
            this.total_bez_nds = otherDs.total_bez_nds;
        }
        this.payed = otherDs.payed;
        this.willNotBePayed = otherDs.willNotBePayed;
    }

    public DocumentState() {
    }

    public Date getActionDate() {
        return actionDate == null ? null : actionDate.getTime();
    }

    public String getEquipment() {
        return this.equipmentString;
    }

    public Date getFromDate() {
        return fromDate == null ? null : fromDate.getTime();
    }

    public String getBuyerName() {
        return this.buyer.type + " " + this.buyer.name;
    }

    public Integer getSchet() {
        return this.number;
    }

    public String getServiceNameInRow() {
        StringBuilder sb = new StringBuilder();
        Iterator var3 = this.servicesTM.iterator();

        while (var3.hasNext()) {
            R r = (R) var3.next();
            sb.append(r.name.serviceNameRodPad + ", ");
        }

        return sb.toString();
    }

    public BigDecimal getTotal() {
        return this.total;
    }
}
