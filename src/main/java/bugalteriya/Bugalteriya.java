package bugalteriya;

/**
 * @author Dmitry Tumash
 */

import document.DocStateTableModel;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import java.math.BigDecimal;
import java.util.List;

public class Bugalteriya extends JPanel {
    private static final String BUGFILENAME = "resources/bugalteriya";
    private JTextField saldoTF;
    private JTextField ostatokTF;
    private JTable table;
    private BugTableModel btm = new BugTableModel();
    private BugSchetCellRendererEditor bscr;
    private BugDateCellRendererEditor bdse = new BugDateCellRendererEditor();

    public void save() {
        try {
            ObjectOutputStream e = new ObjectOutputStream(new FileOutputStream("resources/bugalteriya"));
            e.writeObject(this.btm.list);
            e.writeObject(this.saldoTF.getText());
            e.flush();
            e.close();
        } catch (IOException var2) {
            var2.printStackTrace();
        }

    }

    public void load() {
        try {
            ObjectInputStream e = new ObjectInputStream(new FileInputStream("resources/bugalteriya"));
            this.btm.list = (List)e.readObject();
            this.saldoTF.setText((String)e.readObject());
            e.close();
            this.updateSaldo();
            this.btm.newRow = this.btm.list.size() - 1;
        } catch (IOException var2) {
            var2.printStackTrace();
        } catch (ClassNotFoundException var3) {
            var3.printStackTrace();
        }

    }

    private void updateSaldo() {
        BigDecimal start = BigDecimal.ZERO;

        try {
            start = new BigDecimal(this.saldoTF.getText());
        } catch (NumberFormatException var3) {
            ;
        }

        BigDecimal saldo = start.add(this.btm.getDiff());
        this.ostatokTF.setText(saldo.toPlainString());
    }

    public Bugalteriya(DocStateTableModel dstm) {
        this.bscr = new BugSchetCellRendererEditor(this.btm, dstm);
        this.setLayout(new BorderLayout(0, 0));
        JPanel panel = new JPanel();
        this.add(panel, "North");
        JLabel label = new JLabel("Сальдо начальное");
        panel.add(label);
        this.saldoTF = new JTextField();
        panel.add(this.saldoTF);
        this.saldoTF.setColumns(10);
        this.saldoTF.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
                Bugalteriya.this.updateSaldo();
            }

            public void keyPressed(KeyEvent e) {
            }
        });
        Component horizontalStrut = Box.createHorizontalStrut(20);
        panel.add(horizontalStrut);
        JLabel lblNewLabel = new JLabel("Текущий остаток");
        panel.add(lblNewLabel);
        this.ostatokTF = new JTextField();
        panel.add(this.ostatokTF);
        this.ostatokTF.setColumns(10);
        JButton btnNewButton = new JButton("Вставить строку");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                int sellectedRow = Bugalteriya.this.table.getSelectedRow();
                sellectedRow = Bugalteriya.this.table.convertRowIndexToModel(sellectedRow);
                Bugalteriya.this.btm.insertRow(sellectedRow + 1);
            }
        });
        btnNewButton.setAlignmentX(1.0F);
        panel.add(btnNewButton);
        JButton btnNewButton_1 = new JButton("Удалить строку");
        btnNewButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int sellectedRow = Bugalteriya.this.table.getSelectedRow();
                sellectedRow = Bugalteriya.this.table.convertRowIndexToModel(sellectedRow);
                Bugalteriya.this.btm.removeRow(sellectedRow);
            }
        });
        panel.add(btnNewButton_1);
        JScrollPane scrollPane = new JScrollPane();
        this.add(scrollPane, "Center");
        this.table = new JTable();
        this.table.setModel(this.btm);
        this.table.setFillsViewportHeight(true);
        DefaultCellEditor dce = (DefaultCellEditor)this.table.getDefaultEditor(this.btm.getColumnClass(1));
        dce.setClickCountToStart(1);
        this.table.getColumnModel().getColumn(1).setCellEditor(dce);
        dce = (DefaultCellEditor)this.table.getDefaultEditor(this.btm.getColumnClass(3));
        dce.setClickCountToStart(1);
        this.table.getColumnModel().getColumn(3).setCellEditor(dce);
        dce = (DefaultCellEditor)this.table.getDefaultEditor(this.btm.getColumnClass(4));
        dce.setClickCountToStart(1);
        this.table.getColumnModel().getColumn(4).setCellEditor(dce);
        dce = (DefaultCellEditor)this.table.getDefaultEditor(this.btm.getColumnClass(5));
        dce.setClickCountToStart(1);
        this.table.getColumnModel().getColumn(5).setCellEditor(dce);
        this.table.getColumnModel().getColumn(0).setCellEditor(this.bdse);
        this.table.getColumnModel().getColumn(0).setCellRenderer(this.bdse);
        this.table.getColumnModel().getColumn(6).setCellEditor(new BugDateCellRendererEditor());
        this.table.getColumnModel().getColumn(6).setCellRenderer(new BugDateCellRendererEditor());
        this.table.getColumnModel().getColumn(2).setCellRenderer(this.bscr);
        this.table.getColumnModel().getColumn(2).setCellEditor(this.bscr);
        this.table.setCellSelectionEnabled(true);
        this.table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        this.table.setRowHeight(25);
        this.table.getModel().addTableModelListener(new TableModelListener() {
            public void tableChanged(TableModelEvent e) {
                Bugalteriya.this.updateSaldo();
            }
        });
        scrollPane.setViewportView(this.table);
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                Bugalteriya.this.load();
            }
        });
        this.table.scrollRectToVisible(this.table.getCellRect(this.table.getRowCount() - 1, 0, true));
    }
}

