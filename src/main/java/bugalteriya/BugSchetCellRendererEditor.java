package bugalteriya;


import document.ArchiveDialog;
import document.DocStateTableModel;
import document.DocumentState;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;

/**
 * @author Dmitry Tumash
 */
public class BugSchetCellRendererEditor extends AbstractCellEditor implements TableCellEditor, TableCellRenderer {
    private JTextField edit = new JTextField(20);
    private JPanel editP = new JPanel();
    private JLabel l = new JLabel();
    private JButton btn = new JButton(new ImageIcon("resources/images/edit.png"));
    private BugTableModel btm;
    private BugSchetCellRendererEditor.BtnActionListener bal = new BugSchetCellRendererEditor.BtnActionListener(0, 0);
    private DocStateTableModel dstm;
    private ArchiveDialog ad = new ArchiveDialog();

    public boolean isCellEditable(EventObject e) {
        return true;
    }

    public BugSchetCellRendererEditor(BugTableModel btm, DocStateTableModel dstm) {
        this.btn.setMaximumSize(new Dimension(23, 23));
        this.btn.setPreferredSize(new Dimension(23, 23));
        this.btn.addActionListener(this.bal);
        this.editP.setLayout(new BorderLayout());
        this.editP.add(this.edit, "Center");
        this.editP.add(this.btn, "East");
        this.editP.setForeground(Color.green);
        this.btm = btm;
        this.dstm = dstm;
        this.ad.setModel(dstm);
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        this.l.setText(value == null?"":value.toString());
        return this.l;
    }

    public Object getCellEditorValue() {
        return this.edit.getText();
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        this.bal.r = row;
        this.bal.c = column;
        this.edit.setText((String)value);
        return this.editP;
    }

    private class BtnActionListener implements ActionListener {
        private int r;
        private int c;

        public BtnActionListener(int r, int c) {
            this.r = r;
            this.c = c;
        }

        public void actionPerformed(ActionEvent e) {
            DocumentState ds = BugSchetCellRendererEditor.this.ad.getDocState();
            if(ds != null) {
                BugSchetCellRendererEditor.this.btm.setBySchetNum(ds, this.r);
                BugSchetCellRendererEditor.this.edit.setText(ds.getSchet().toString());
            }

        }
    }
}

