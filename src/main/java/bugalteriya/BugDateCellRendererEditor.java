package bugalteriya;


import gui.special.DateLabelFormatter;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilCalendarModel;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author Dmitry Tumash
 */
public class BugDateCellRendererEditor extends AbstractCellEditor implements TableCellEditor, TableCellRenderer {
    private JLabel lbl = new JLabel();
    private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
    private JDatePickerImpl datePicker = new JDatePickerImpl(new JDatePanelImpl(new UtilCalendarModel()), new DateLabelFormatter());

    public BugDateCellRendererEditor() {
    }

    public Object getCellEditorValue() {
        return this.datePicker.getModel().getValue();
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        Calendar c = (Calendar)value;
        if(c != null) {
            UtilCalendarModel ucm = (UtilCalendarModel)this.datePicker.getModel();
            ucm.setValue(c);
            return this.datePicker;
        } else {
            this.datePicker.getModel().setValue(/*todo dmt(Object)*/null);
            return this.datePicker;
        }
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Calendar c = (Calendar)value;
        if(c != null) {
            this.lbl.setText(this.sdf.format(c.getTime()));
        } else {
            this.lbl.setText("");
        }

        return this.lbl;
    }
}

