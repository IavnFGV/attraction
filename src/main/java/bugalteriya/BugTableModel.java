package bugalteriya;

import document.DocumentState;

import javax.swing.table.AbstractTableModel;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Dmitry Tumash
 */
public class BugTableModel extends AbstractTableModel {

    private Comparator<BugTableModel.R> compar = new Comparator<BugTableModel.R>() {

        public int compare(BugTableModel.R o1, BugTableModel.R o2) {
            return o1.d == null && o2.d == null?0:(o1.d == null && o2.d != null?1:(o1.d != null && o2.d == null?-1:o1.d.compareTo(o2.d)));
        }
    };

    List<BugTableModel.R> list = new ArrayList();
    int newRow;

    public BugTableModel() {
        this.list.add(new BugTableModel.R());
        this.newRow = 0;
    }

    public void insertRow(int row) {
        this.list.add(row, new BugTableModel.R());
        ++this.newRow;
        this.fireTableDataChanged();
    }

    private void sortList() {
    }

    public String getColumnName(int column) {
        switch(column) {
            case 0:
                return "Дата";
            case 1:
                return "От кого";
            case 2:
                return "По счету";
            case 3:
                return "Приход";
            case 4:
                return "Расход";
            case 5:
                return "За что";
            case 6:
                return "Дата";
            default:
                return null;
        }
    }

    public BigDecimal getDiff() {
        BigDecimal sum = BigDecimal.ZERO;
        Iterator var3 = this.list.iterator();

        while(var3.hasNext()) {
            BugTableModel.R r = (BugTableModel.R)var3.next();
            if(r.in != null) {
                sum = sum.add(r.in);
            }

            if(r.out != null) {
                sum = sum.subtract(r.out);
            }
        }

        return sum;
    }

    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0:
                return String.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return BigDecimal.class;
            case 4:
                return BigDecimal.class;
            case 5:
                return String.class;
            case 6:
                return String.class;
            default:
                return null;
        }
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        BugTableModel.R r = (BugTableModel.R)this.list.get(rowIndex);
        boolean firstNotEmpty = false;
        boolean secondNotEmpty = false;

        for(int i = 0; i < 7; ++i) {
            switch(i) {
                case 0:
                    if(r.d != null) {
                        firstNotEmpty = true;
                    }
                    break;
                case 1:
                    if(r.from == null || !"".equals(r.from.trim())) {
                        firstNotEmpty = true;
                    }
                    break;
                case 2:
                    if(r.schet == null || !"".equals(r.schet.trim())) {
                        firstNotEmpty = true;
                    }
                    break;
                case 3:
                    if(r.in != null) {
                        firstNotEmpty = true;
                    }
                    break;
                case 4:
                    if(r.out != null) {
                        secondNotEmpty = true;
                    }
                    break;
                case 5:
                    if(r.forWhat == null || !"".equals(r.forWhat.trim())) {
                        secondNotEmpty = true;
                    }
                    break;
                case 6:
                    if(r.d2 != null) {
                        secondNotEmpty = true;
                    }
            }
        }

        if(firstNotEmpty && secondNotEmpty) {
            return true;
        } else if(!firstNotEmpty && !secondNotEmpty) {
            return true;
        } else if(columnIndex < 4 && secondNotEmpty) {
            return false;
        } else if(columnIndex > 3 && firstNotEmpty) {
            return false;
        } else {
            return true;
        }
    }

    public void setValueAt(Object val, int row, int columnIndex) {
        BugTableModel.R r = (BugTableModel.R)this.list.get(row);
        if(row == this.newRow && val != null && !"".equals(val.toString())) {
            this.list.add(new BugTableModel.R());
            ++this.newRow;
            this.fireTableRowsInserted(row, this.newRow);
        }

        switch(columnIndex) {
            case 0:
                r.d = (Calendar)val;
                this.sortList();
                this.fireTableDataChanged();
                break;
            case 1:
                r.from = (String)val;
                break;
            case 2:
                r.schet = (String)val;
                break;
            case 3:
                r.in = (BigDecimal)val;
                this.fireTableDataChanged();
                break;
            case 4:
                r.out = (BigDecimal)val;
                this.fireTableDataChanged();
                break;
            case 5:
                r.forWhat = (String)val;
                break;
            case 6:
                r.d2 = (Calendar)val;
        }

    }

    public int getRowCount() {
        return this.list.size();
    }

    public int getColumnCount() {
        return 7;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        BugTableModel.R r = (BugTableModel.R)this.list.get(rowIndex);
        switch(columnIndex) {
            case 0:
                return r.d;
            case 1:
                return r.from;
            case 2:
                return r.schet;
            case 3:
                return r.in;
            case 4:
                return r.out;
            case 5:
                return r.forWhat;
            case 6:
                return r.d2;
            default:
                return null;
        }
    }

    public void setBySchetNum(DocumentState ds, int r) {
        ((BugTableModel.R)this.list.get(r)).from = ds.getBuyerName();
        ((BugTableModel.R)this.list.get(r)).schet = ds.getSchet().toString();
        ((BugTableModel.R)this.list.get(r)).in = ds.getTotal();
        if(r == this.newRow) {
            this.list.add(new BugTableModel.R());
            ++this.newRow;
        }

        this.fireTableDataChanged();
    }

    public void removeRow(int sellectedRow) {
        if(sellectedRow >= 0 && sellectedRow < this.list.size()) {
            this.list.remove(sellectedRow);
            --this.newRow;
        }

        this.fireTableDataChanged();
    }

    public static class R implements Serializable {
        private static final long serialVersionUID = 1L;
        Calendar d;
        String from = "";
        String schet = "";
        BigDecimal in;
        BigDecimal out;
        String forWhat = "";
        Calendar d2;

        public R() {
        }
    }
}
